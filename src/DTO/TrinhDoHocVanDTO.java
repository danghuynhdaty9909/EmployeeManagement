package DTO;

public class TrinhDoHocVanDTO {
	//	FIELDS
	private String maTDHV;
	private String tenTDHV;
	private String chuyenNganh;
	
	//	CONSTRUCTORS
	public TrinhDoHocVanDTO(String maTDHV, String tenTDHV, String chuyenNganh) {
		super();
		this.maTDHV = maTDHV;
		this.tenTDHV = tenTDHV;
		this.chuyenNganh = chuyenNganh;
	}
	public TrinhDoHocVanDTO() {
		super();
	}
	
	//	GETTERS SETTERS
	public String getMaTDHV() {
		return maTDHV;
	}
	public void setMaTDHV(String maTDHV) {
		this.maTDHV = maTDHV;
	}
	public String getTenTDHV() {
		return tenTDHV;
	}
	public void setTenTDHV(String tenTDHV) {
		this.tenTDHV = tenTDHV;
	}
	public String getChuyenNganh() {
		return chuyenNganh;
	}
	public void setChuyenNganh(String chuyenNganh) {
		this.chuyenNganh = chuyenNganh;
	}
	
}
