package DTO;

import java.sql.Date;

public class HopDongLaoDongDTO {
	//	FIELDS
	private String mahdld;
	private String loaiHopDong;
	private Date ngayKy;
	private Date ngayBatDau;
	private Date ngayKetThuc;
	
	//	CONSTRUCTORS
	public HopDongLaoDongDTO() {
		super();
	}
	public HopDongLaoDongDTO(String mahdld, String loaiHopDong, Date ngayKy, Date ngayBatDau, Date ngayKetThuc) {
		super();
		this.mahdld = mahdld;
		this.loaiHopDong = loaiHopDong;
		this.ngayKy = ngayKy;
		this.ngayBatDau = ngayBatDau;
		this.ngayKetThuc = ngayKetThuc;
	}
	
	//	GETTERS SETTERS
	
	public String getMahdld() {
		return mahdld;
	}
	public void setMahdld(String mahdld) {
		this.mahdld = mahdld;
	}
	public String getLoaiHopDong() {
		return loaiHopDong;
	}
	public void setLoaiHopDong(String loaiHopDong) {
		this.loaiHopDong = loaiHopDong;
	}
	public Date getNgayKy() {
		return ngayKy;
	}
	public void setNgayKy(Date ngayKy) {
		this.ngayKy = ngayKy;
	}
	public Date getNgayBatDau() {
		return ngayBatDau;
	}
	public void setNgayBatDau(Date ngayBatDau) {
		this.ngayBatDau = ngayBatDau;
	}
	public Date getNgayKetThuc() {
		return ngayKetThuc;
	}
	public void setNgayKetThuc(Date ngayKetThuc) {
		this.ngayKetThuc = ngayKetThuc;
	}
	
	// METHODS
	
	
}
