package DTO;

public class ChamCongDTO {
	
	//	FIELDS
	private String maChamCong;
	private String[] ngay = new String[31];
	public String getMaChamCong() {
		return maChamCong;
	}
	public void setMaChamCong(String maChamCong) {
		this.maChamCong = maChamCong;
	}
	public String[] getNgay() {
		return ngay;
	}
	public void setNgay(String[] ngay) {
		this.ngay = ngay;
	}
	public ChamCongDTO(String maChamCong, String[] ngay) {
		super();
		this.maChamCong = maChamCong;
		this.ngay = ngay;
	}
	public ChamCongDTO() {
		super();
	}
	
	
	
	
	
	
}
