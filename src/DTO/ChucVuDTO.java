package DTO;

public class ChucVuDTO {

	// FIELDS
	private String maChucVu;
	private String tenChucVu;

	// CONSTRUCTORS
	public ChucVuDTO() {
		super();
	}

	public ChucVuDTO(String maChucVu, String tenChucVu) {
		super();
		this.maChucVu = maChucVu;
		this.tenChucVu = tenChucVu;
	}

	// GETTERS SETTERS
	public String getMaChucVu() {
		return maChucVu;
	}

	public void setMaChucVu(String maChucVu) {
		this.maChucVu = maChucVu;
	}

	public String getTenChucVu() {
		return tenChucVu;
	}

	public void setTenChucVu(String tenChucVu) {
		this.tenChucVu = tenChucVu;
	}

	// METHODS
}
