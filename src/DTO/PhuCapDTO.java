package DTO;

public class PhuCapDTO {
	
	// FIELDS
	private String maPhuCap;
	private	double heSoPhuCap;
	
	//	CONSTRUCTORS
	public PhuCapDTO(String maPhuCap, double heSoPhuCap) {
		super();
		this.maPhuCap = maPhuCap;
		this.heSoPhuCap = heSoPhuCap;
	}
	public PhuCapDTO() {
		super();
	}
	
	//	GETTERS SETTERS
	public String getMaPhuCap() {
		return maPhuCap;
	}
	public void setMaPhuCap(String maPhuCap) {
		this.maPhuCap = maPhuCap;
	}
	public double getHeSoPhuCap() {
		return heSoPhuCap;
	}
	public void setHeSoPhuCap(double heSoPhuCap) {
		this.heSoPhuCap = heSoPhuCap;
	}
	
	//	METHODS
	
}
