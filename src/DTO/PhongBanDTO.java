package DTO;

public class PhongBanDTO {

	// FIELDS
	private String maPhongBan;
	private String tenPhong;
	private String truongPhong;
	private String diaChi;

	// CONSTRUCTORS
	public PhongBanDTO(String maPhongBan, String tenPhong, String truongPhong, String diaChi) {
		super();
		this.maPhongBan = maPhongBan;
		this.tenPhong = tenPhong;
		this.truongPhong = truongPhong;
		this.diaChi = diaChi;
	}

	public PhongBanDTO() {
		super();
	}

	// GETTERS SETTERS
	public String getMaPhongBan() {
		return maPhongBan;
	}

	public void setMaPhongBan(String maPhongBan) {
		this.maPhongBan = maPhongBan;
	}

	public String getTenPhong() {
		return tenPhong;
	}

	public void setTenPhong(String tenPhong) {
		this.tenPhong = tenPhong;
	}

	public String getTruongPhong() {
		return truongPhong;
	}

	public void setTruongPhong(String truongPhong) {
		this.truongPhong = truongPhong;
	}

	public String getDiaChi() {
		return diaChi;
	}

	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	
	
	// METHODS
}
