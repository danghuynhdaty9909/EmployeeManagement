package DTO;

public class LuongDTO {
	
	//	FIELDS
	private String maBacLuong;
	private int luongCoBan;
	
	
	//	CONSTRUCTORS
	
	public LuongDTO(String maBacLuong, int luongCoBan) {
		super();
		this.maBacLuong = maBacLuong;
		this.luongCoBan = luongCoBan;
	}
	public LuongDTO() {
		super();
	}
	
	// 	GETTERS SETTERS
	public String getMaBacLuong() {
		return maBacLuong;
	}
	public void setMaBacLuong(String maBacLuong) {
		this.maBacLuong = maBacLuong;
	}
	public int getLuongCoBan() {
		return luongCoBan;
	}
	public void setLuongCoBan(int luongCoBan) {
		this.luongCoBan = luongCoBan;
	}
	
	
	//	METHODS
	
}
