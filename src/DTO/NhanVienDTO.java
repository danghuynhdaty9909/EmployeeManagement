package DTO;

import java.sql.Date;

public class NhanVienDTO {

	// FIELDS
	private String maNV;
	private String ho;
	private String ten;
	private String gioiTinh;
	private Date ngaySinh;
	private String diaChi;
	private String danToc;
	private String soCMND;
	private String soDT;
	private String email;
	private String soTaiKhoan;
	private String hinhAnh;
	private String mahdld;
	private String macv;
	private String maPhongBan;
	private String maTDHV;
	private String tonGiao;
	private String maBacLuong;
	private String maPhuCap;
	public NhanVienDTO(String maNV, String ho, String ten, String gioiTinh, Date ngaySinh, String diaChi, String danToc,
			String soCMND, String soDT, String email, String soTaiKhoan, String hinhAnh, String mahdld, String macv,
			String maPhongBan, String maTDHV, String tonGiao,String maBacLuong,String maPhuCap) {
		super();
		this.maNV = maNV;
		this.ho = ho;
		this.ten = ten;
		this.gioiTinh = gioiTinh;
		this.ngaySinh = ngaySinh;
		this.diaChi = diaChi;
		this.danToc = danToc;
		this.soCMND = soCMND;
		this.soDT = soDT;
		this.email = email;
		this.soTaiKhoan = soTaiKhoan;
		this.hinhAnh = hinhAnh;
		this.mahdld = mahdld;
		this.macv = macv;
		this.maPhongBan = maPhongBan;
		this.maTDHV = maTDHV;
		this.tonGiao = tonGiao;
		this.maBacLuong=maBacLuong;
		this.maPhuCap=maPhuCap;
	}
	public NhanVienDTO() {
		super();
	}
	public String getMaNV() {
		return maNV;
	}
	public void setMaNV(String maNV) {
		this.maNV = maNV;
	}
	public String getHo() {
		return ho;
	}
	public void setHo(String ho) {
		this.ho = ho;
	}
	public String getTen() {
		return ten;
	}
	public void setTen(String ten) {
		this.ten = ten;
	}
	public String getGioiTinh() {
		return gioiTinh;
	}
	public void setGioiTinh(String gioiTinh) {
		this.gioiTinh = gioiTinh;
	}
	public Date getNgaySinh() {
		return ngaySinh;
	}
	public void setNgaySinh(Date ngaySinh) {
		this.ngaySinh = ngaySinh;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public String getDanToc() {
		return danToc;
	}
	public void setDanToc(String danToc) {
		this.danToc = danToc;
	}
	public String getSoCMND() {
		return soCMND;
	}
	public void setSoCMND(String soCMND) {
		this.soCMND = soCMND;
	}
	public String getSoDT() {
		return soDT;
	}
	public void setSoDT(String soDT) {
		this.soDT = soDT;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSoTaiKhoan() {
		return soTaiKhoan;
	}
	public void setSoTaiKhoan(String soTaiKhoan) {
		this.soTaiKhoan = soTaiKhoan;
	}
	public String getHinhAnh() {
		return hinhAnh;
	}
	public void setHinhAnh(String hinhAnh) {
		this.hinhAnh = hinhAnh;
	}
	public String getMahdld() {
		return mahdld;
	}
	public void setMahdld(String mahdld) {
		this.mahdld = mahdld;
	}
	public String getMacv() {
		return macv;
	}
	public void setMacv(String macv) {
		this.macv = macv;
	}
	public String getMaPhongBan() {
		return maPhongBan;
	}
	public void setMaPhongBan(String maPhongBan) {
		this.maPhongBan = maPhongBan;
	}
	public String getMaTDHV() {
		return maTDHV;
	}
	public void setMaTDHV(String maTDHV) {
		this.maTDHV = maTDHV;
	}
	public String getTonGiao() {
		return tonGiao;
	}
	public void setTonGiao(String tonGiao) {
		this.tonGiao = tonGiao;
	}
	public String getMaBacLuong() {
		return maBacLuong;
	}
	public void setMaBacLuong(String maBacLuong) {
		this.maBacLuong = maBacLuong;
	}
	public String getMaPhuCap() {
		return maPhuCap;
	}
	public void setMaPhuCap(String maPhuCap) {
		this.maPhuCap = maPhuCap;
	}
	
	// METHODS
	
}
