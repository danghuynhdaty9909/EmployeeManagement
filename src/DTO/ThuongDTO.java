package DTO;

public class ThuongDTO {
	
	//	FIELDS
	private String maThuong;
	private String tenLoaiThuong;
	private int mucThuong;
	
	//	CONSTRUCTORS
	public ThuongDTO(String maThuong, String tenLoaiThuong, int mucThuong) {
		super();
		this.maThuong = maThuong;
		this.tenLoaiThuong = tenLoaiThuong;
		this.mucThuong = mucThuong;
	}
	public ThuongDTO() {
		super();
	}
	
	//	GETTERS SETTERS
	public String getMaThuong() {
		return maThuong;
	}
	public void setMaThuong(String maThuong) {
		this.maThuong = maThuong;
	}
	public String getTenLoaiThuong() {
		return tenLoaiThuong;
	}
	public void setTenLoaiThuong(String tenLoaiThuong) {
		this.tenLoaiThuong = tenLoaiThuong;
	}
	public int getMucThuong() {
		return mucThuong;
	}
	public void setMucThuong(int mucThuong) {
		this.mucThuong = mucThuong;
	}
	
	
	
}
