package DTO;

public class LuongChiTietDTO {
	
	
	// FIELDS
	
	private String maLuongChiTiet;
	private int thucLanh;
	private String maThuong;
	private String maNhanVien;
	private String maChamCong;
	private int thang;
	public LuongChiTietDTO(String maLuongChiTiet, int thucLanh,String maThuong,
			String maNhanVien, String maChamCong, int thang) {
		super();
		this.maLuongChiTiet = maLuongChiTiet;
		this.thucLanh = thucLanh;
		this.maThuong = maThuong;
		this.maNhanVien = maNhanVien;
		this.maChamCong = maChamCong;
		this.thang = thang;
	}
	public LuongChiTietDTO() {
		super();
	}
	//	GETTERS SETTERS
	public String getMaLuongChiTiet() {
		return maLuongChiTiet;
	}
	public void setMaLuongChiTiet(String maLuongChiTiet) {
		this.maLuongChiTiet = maLuongChiTiet;
	}
	public int getThucLanh() {
		return thucLanh;
	}
	public void setThucLanh(int thucLanh) {
		this.thucLanh = thucLanh;
	}
	public String getMaThuong() {
		return maThuong;
	}
	public void setMaThuong(String maThuong) {
		this.maThuong = maThuong;
	}
	public String getMaNhanVien() {
		return maNhanVien;
	}
	public void setMaNhanVien(String maNhanVien) {
		this.maNhanVien = maNhanVien;
	}
	public String getMaChamCong() {
		return maChamCong;
	}
	public void setMaChamCong(String maChamCong) {
		this.maChamCong = maChamCong;
	}
	public int getThang() {
		return thang;
	}
	public void setThang(int thang) {
		this.thang = thang;
	}
	
	// 	METHODS
	
	

	
	
	
	
}
