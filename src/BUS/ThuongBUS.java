package BUS;

import java.util.ArrayList;

import DAO.ThuongDAO;
import DTO.ThuongDTO;

public class ThuongBUS {
	ThuongDAO thuongDao=new ThuongDAO();
	
	//static list thuong
	public static ArrayList<ThuongDTO> sttListThuongDTO = new ArrayList<ThuongDTO>();
	
	
	public static ArrayList<ThuongDTO> getSttListThuongDTO() {
		return sttListThuongDTO;
	}


	public static void setSttListThuongDTO(ArrayList<ThuongDTO> sttListThuongDTO) {
		ThuongBUS.sttListThuongDTO = sttListThuongDTO;
	}


	public ArrayList<String> getOneCol(String col){
		return thuongDao.getOneCol(col);
	}
	
	public ArrayList<ThuongDTO> getAllThuong()
	{
		return thuongDao.getAllThuong();
	}
}
