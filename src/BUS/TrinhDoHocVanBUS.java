package BUS;

import java.util.ArrayList;

import DAO.TrinhDoHocVanDAO;
import DTO.TrinhDoHocVanDTO;

public class TrinhDoHocVanBUS {
	TrinhDoHocVanDAO trinhdoDao=new TrinhDoHocVanDAO();
	public TrinhDoHocVanDTO getTrinhDo(String id){
		return trinhdoDao.getTrinhDo(id);
	}
	public ArrayList<String> getListName(String col){
		return trinhdoDao.getListName(col);
	}
	public boolean check(String name, String valuename, String chuyennganh, String valuechuyennganh) {
		return trinhdoDao.check(name, valuename, chuyennganh, valuechuyennganh);
	}
	public String createNewId() {
		return trinhdoDao.createNewId();
	}
	public boolean insert(TrinhDoHocVanDTO trinhdo) {
		return trinhdoDao.insert(trinhdo);
	}
	public String getIdByCondition(String valueName, String specializeName){
		return trinhdoDao.getIdByCondition(valueName, specializeName);
	}
}
