package BUS;

import java.util.ArrayList;

import DAO.HopDongLaoDongDAO;
import DTO.HopDongLaoDongDTO;

public class HopDongLaoDongBUS {
	HopDongLaoDongDAO hopdongDao=new HopDongLaoDongDAO();
	public HopDongLaoDongDTO getHopDongLaoDong(String id) {
		return hopdongDao.getHopDongLaoDong(id);
	}
	public boolean delete(String id) {
		return hopdongDao.delete(id);
	}
	public ArrayList<String> getList(String col){
		return hopdongDao.getList(col);
	}
	public String createNewId() {
		return hopdongDao.createNewId();
	}
	public boolean insert(HopDongLaoDongDTO hopdongDto){
		return hopdongDao.insert(hopdongDto);
	}
	public boolean update(HopDongLaoDongDTO hopdong) {
		return hopdongDao.update(hopdong);
	}
}
