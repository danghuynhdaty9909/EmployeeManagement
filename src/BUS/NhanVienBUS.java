package BUS;

import java.util.ArrayList;
import DAO.NhanVienDAO;
import DTO.NhanVienDTO;

public class NhanVienBUS {

	// static list luu tat ca danh sach nhan vien
	public static ArrayList<NhanVienDTO> sttListNhanVienDTO = new ArrayList<NhanVienDTO>();

	// static nhanvien dto
	public static NhanVienDTO sttNhanVienDTO = new NhanVienDTO();

	// getters setters
	public static ArrayList<NhanVienDTO> getSttListNhanVienDTO() {
		return sttListNhanVienDTO;
	}

	public static void setSttListNhanVienDTO(ArrayList<NhanVienDTO> sttListNhanVienDTO) {
		NhanVienBUS.sttListNhanVienDTO = sttListNhanVienDTO;
	}

	public static NhanVienDTO getSttNhanVienDTO() {
		return sttNhanVienDTO;
	}

	public static void setSttNhanVienDTO(NhanVienDTO sttNhanVienDTO) {
		NhanVienBUS.sttNhanVienDTO = sttNhanVienDTO;
	}

	// methods
	NhanVienDAO nhanvienDao = new NhanVienDAO();

	public ArrayList<NhanVienDTO> loadDataByCondition(String cond, String value) {
		return nhanvienDao.loadDataByCondition(cond, value);
	}

	public NhanVienDTO getNhanVienByID(String manv) {
		return nhanvienDao.getNhanVienByID(manv);
	}

	public boolean deleteEmployee(String id) {
		return nhanvienDao.deleteEmployee(id);
	}

	public ArrayList<String> getListOneCol(String col) {
		return nhanvienDao.getListOneCol(col);
	}

	public String createNewId() {
		return nhanvienDao.createNewId();
	}

	public boolean insert(NhanVienDTO staff) {
		return nhanvienDao.insert(staff);
	}

	public boolean updateOneCol(String col, String newValue, String valueId) {
		return nhanvienDao.updateOneCol(col, newValue, valueId);
	}

	public boolean update(NhanVienDTO nhanvienDto) {
		return nhanvienDao.update(nhanvienDto);
	}

	public int countEmployeeByCondition(String condition, String value) {
		return nhanvienDao.countEmployeeByCondition(condition, value);
	}
	public ArrayList<NhanVienDTO> getAllNhanVienDTO(){
		return nhanvienDao.getAllNhanVienDTO();
	}
}
