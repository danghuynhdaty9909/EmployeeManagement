package BUS;

import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import DAO.LuongChiTietDAO;
import DTO.LuongChiTietDTO;

public class LuongChiTietBUS {
	LuongChiTietDAO luongchitietDao=new LuongChiTietDAO();
	public LuongChiTietDTO getLuongChiTiet(String idNhanVien) {
		return luongchitietDao.getLuongChiTiet(idNhanVien);
	}
	public boolean delete(String manv){
		return luongchitietDao.delete(manv);
	}
	
	
	//	METHODS
	public void TinhLuong()
	{
		luongchitietDao.TinhLuong();
	}
	public DefaultTableModel setModelTableNhanVien(String phong, int thang)
	{
		return luongchitietDao.setModelTableNhanVien( phong, thang);
	}
	
	public DefaultTableModel TimKiemNhanVien(int thang, String manv) 
	{
		return luongchitietDao.TimKiemNhanVien(thang,manv);
	}
	
	public boolean CheckNhanVien(String manv, String thang) {
		return luongchitietDao.CheckNhanVien(manv, thang);
	}
	public boolean TinhLuongThang(int thang) {
		return luongchitietDao.TinhLuongThang(thang);
	}
	public ArrayList<LuongChiTietDTO> getAllLuongChiTietDTO() 
	{
		return luongchitietDao.getAllLuongChiTietDTO();
	}
	public boolean ThemLuongChiTiet(LuongChiTietDTO luong)
	{
		return luongchitietDao.ThemLuongChiTiet(luong);
	}

}
