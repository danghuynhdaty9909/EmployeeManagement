package BUS;

import java.util.ArrayList;

import DAO.PhongBanDAO;
import DTO.PhongBanDTO;

public class PhongBanBUS {

	// FIELDS

	// Biến DAO
	PhongBanDAO phongbanDao = new PhongBanDAO();

	// biến tĩnh lưu phòng ban hiện tại
	private static PhongBanDTO sttPhongBanDTO = new PhongBanDTO();

	// danh sách tất cả phòng ban
	private static ArrayList<PhongBanDTO> sttArrayPhongBanDTO = new ArrayList<PhongBanDTO>();

	// GETTERS SETTERS

	public static PhongBanDTO getSttPhongBanDTO() {
		return sttPhongBanDTO;
	}

	public static void setSttPhongBanDTO(PhongBanDTO sttPhongBanDTO) {
		PhongBanBUS.sttPhongBanDTO = sttPhongBanDTO;
	}

	public static ArrayList<PhongBanDTO> getSttArrayPhongBanDTO() {
		return sttArrayPhongBanDTO;
	}

	public static void setSttArrayPhongBanDTO(ArrayList<PhongBanDTO> sttArrayPhongBanDTO) {
		PhongBanBUS.sttArrayPhongBanDTO = sttArrayPhongBanDTO;
	}

	// METHODS

	public PhongBanDTO getPhongBan(String id) {
		return phongbanDao.getPhongBan(id);
	}

	public ArrayList<String> getListOneCol(String col, String cond, String value) {
		return phongbanDao.getListOneCol(col, cond, value);
	}

	public ArrayList<PhongBanDTO> getAllPhongBan() {
		return phongbanDao.getAllPhongBan();
	}
	public String getIdDepartment(String name) {
		return phongbanDao.getIdDepartment(name);
	}
	public String getValue(String id, String col){
		return phongbanDao.getValue(id, col);
	}
	public boolean update(PhongBanDTO phongban) {
		return phongbanDao.update(phongban);
	}
	public boolean check(String tenphong,String diachi){
		return phongbanDao.check(tenphong, diachi);
	}
	public boolean insert(PhongBanDTO phongban) {
		return phongbanDao.insert(phongban);
	}
}
