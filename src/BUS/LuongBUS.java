package BUS;

import java.util.ArrayList;

import DAO.LuongDAO;
import DTO.LuongDTO;

public class LuongBUS {

	
	//biến LuongDAO
		private LuongDAO luongDAO = new LuongDAO();
		
		// biến static luongdto
		private static LuongDTO luongDTO = new LuongDTO();
		
		//danh sách static luongdto
		
		private static ArrayList<LuongDTO> sttListLuongDTO = new ArrayList<LuongDTO>();

		
		
		//	GETTERS SETTERS
		public static LuongDTO getLuongDTO() {
			return luongDTO;
		}

		public static void setLuongDTO(LuongDTO luongDTO) {
			LuongBUS.luongDTO = luongDTO;
		}

		public static ArrayList<LuongDTO> getSttListLuongDTO() {
			return sttListLuongDTO;
		}

		public static void setSttListLuongDTO(ArrayList<LuongDTO> sttListLuongDTO) {
			LuongBUS.sttListLuongDTO = sttListLuongDTO;
		}
		
		
		//	METHODS 
		
		//hàm lấy tất cả luong trong db
		public ArrayList<LuongDTO> getAllLuong(){
			return luongDAO.getAllLuong();
		}
		public ArrayList<String> getOneCol(String col){
			return luongDAO.getOneCol(col);
		}
		public String getOneCol(int value){
			return luongDAO.getOneCol(value);
		}
		public String getLuongCoBan(String id){
			return luongDAO.getLuongCoBan(id);
		}
}
