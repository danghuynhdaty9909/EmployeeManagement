package BUS;

import java.util.ArrayList;

import DAO.ChucVuDAO;
import DTO.ChucVuDTO;

public class ChucVuBUS {
	ChucVuDAO chucvuDao=new ChucVuDAO();
	public ChucVuDTO getChucVu(String id){
		return chucvuDao.getChucVu(id);
	}
	public ArrayList<String> getListNameByCond(String col,String cond, String value){
		return chucvuDao.getListNameByCond(col, cond, value);
	}
	public String getIdPositionByName(String name){
		return chucvuDao.getIdPositionByName(name);
	}
}
