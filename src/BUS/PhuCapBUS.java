package BUS;

import java.util.ArrayList;

import DAO.PhuCapDAO;
import DTO.PhuCapDTO;

public class PhuCapBUS {
	PhuCapDAO phucapDao=new PhuCapDAO();
	
	//stattic list phu cap
	private static  ArrayList<PhuCapDTO>  sttListPhuCap = new  ArrayList<PhuCapDTO>() ;
	
	public static ArrayList<PhuCapDTO> getSttListPhuCap() {
		return sttListPhuCap;
	}
	public static void setSttListPhuCap(ArrayList<PhuCapDTO> sttListPhuCap) {
		PhuCapBUS.sttListPhuCap = sttListPhuCap;
	}
	public ArrayList<String> getOneCol(String col){
		return phucapDao.getOneCol(col);
	}
	public String getId(double heso){
		return phucapDao.getId(heso);
	}
	public String getHeSo(String id){
		return phucapDao.getHeSo(id);
	}
	public ArrayList<PhuCapDTO> getAllPhuCap()
	{
		return phucapDao.getAllPhuCap();
	}
}
