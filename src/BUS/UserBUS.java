package BUS;

import java.util.ArrayList;

import DAO.UserDAO;
import DTO.UserDTO;

public class UserBUS {
	
	// biến UserDAO
	private UserDAO userDAO = new UserDAO();
	
	// biến static lưu user tĩnh 
	private static UserDTO sttUserDTO = new UserDTO();
	
	// biến static lưu danh sách user
	private static ArrayList<UserDTO> sttListUserDTO = new ArrayList<UserDTO>();
	
	//	GETTERS SETTERS
	

	public static UserDTO getSttUserDTO() {
		return sttUserDTO;
	}

	public static void setSttUserDTO(UserDTO sttUserDTO) {
		UserBUS.sttUserDTO = sttUserDTO;
	}

	public static ArrayList<UserDTO> getSttListUserDTO() {
		return sttListUserDTO;
	}

	public static void setSttListUserDTO(ArrayList<UserDTO> sttListUserDTO) {
		UserBUS.sttListUserDTO = sttListUserDTO;
	}
	
	//	METHODS
	
	public boolean Login(String username, String password){
		return userDAO.Login(username, password);
	}
	
}
