package GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import DTO.PhongBanDTO;

import java.awt.Color;
import javax.swing.ImageIcon;

public class fTKPhongBan extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTable table;
	private JButton btClose;
	private DefaultTableModel model;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fTKPhongBan frame = new fTKPhongBan();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public fTKPhongBan() {
		setTitle("Thống Kê Phòng Ban");
		// nimbus look and feel
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLocationRelativeTo(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lbTitle = new JLabel("Thống Kê Phòng Ban");
		lbTitle.setForeground(Color.RED);
		lbTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lbTitle.setFont(new Font("Times New Roman", Font.BOLD, 25));
		lbTitle.setBounds(0, 37, 1350, 80);
		contentPane.add(lbTitle);

		table = new JTable();
		table.setFont(new Font("Tahoma", Font.PLAIN, 15));
		table.setModel(model = new DefaultTableModel(new Object[][] {}, new String[] { "T\u00EAn Ph\u00F2ng",
				"Tr\u01B0\u1EDFng Ph\u00F2ng", "\u0110\u1ECBa Ch\u1EC9 Ph\u00F2ng", "S\u1ED1 Nh\u00E2n Vi\u00EAn" }));
		table.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 17));
		table.setRowHeight(25);

		JScrollPane sc = new JScrollPane(table);
		sc.setBounds(96, 146, 1139, 442);
		contentPane.add(sc);

		btClose = new JButton("Đóng");
		btClose.setIcon(new ImageIcon("img\\cancel2.png"));
		btClose.setBackground(Color.WHITE);
		btClose.setFont(new Font("Tahoma", Font.BOLD, 15));
		btClose.setBounds(620, 619, 118, 42);
		btClose.addActionListener(this);
		loadData();
		contentPane.add(btClose);
	}

	public void loadData() {

		PhongBanBUS departmentBus = new PhongBanBUS();
		NhanVienBUS staffBus = new NhanVienBUS();
		ArrayList<PhongBanDTO> result = departmentBus.getAllPhongBan();
		if (result != null) {
			Vector<String> row;
			for (PhongBanDTO department : result) {
				row = new Vector<>();
				row.add(department.getTenPhong());
				row.add(staffBus.getNhanVienByID(department.getTruongPhong()).getHo()+" "+staffBus.getNhanVienByID(department.getTruongPhong()).getTen());
				row.add(department.getDiaChi());
				row.add(String.valueOf(staffBus.countEmployeeByCondition("maphongban", department.getMaPhongBan())));
				model.addRow(row);

			}
			table.setModel(model);
		}
	}

	public void actionPerformed(ActionEvent e) {
		this.setVisible(false);
		new fThongKeNhanVien().setVisible(true);
	}

}