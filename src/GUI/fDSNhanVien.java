package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import DTO.NhanVienDTO;
import DTO.PhongBanDTO;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Color;

@SuppressWarnings("serial")
public class fDSNhanVien extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private DefaultTableModel model;
	private JButton btOk;
	private JButton btClose;
	private static String newIdBoss;
	private static NhanVienDTO truongphong = new NhanVienDTO();
	private NhanVienBUS nhanvienBus = new NhanVienBUS();
	private static PhongBanDTO dto = new PhongBanDTO();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fDSNhanVien frame = new fDSNhanVien();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public fDSNhanVien() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 93, 1330, 387);
		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		table.setFont(new Font("SansSerif", Font.BOLD, 12));
		table.setModel(model = new DefaultTableModel(new Object[][] {},
				new String[] { "M\u00E3 Nh\u00E2n Vi\u00EAn", "H\u1ECD", "T\u00EAn", "Gi\u1EDBi T\u00EDnh", "Email",
						"S\u1ED1 \u0110i\u1EC7n Tho\u1EA1i", "S\u1ED1 CMND", "Qu\u00EA Qu\u00E1n",
						"M\u00E3 ph\u00F2ng ban", "M\u00E3 ch\u1EE9c v\u1EE5", "M\u00E3 h\u1EE3p \u0111\u1ED3ng",
						"M\u00E3 TDHV" }));
		scrollPane.setViewportView(table);

		JLabel lblNewLabel = new JLabel("Danh Sách Nhân Viên");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 30));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(89, 11, 1163, 71);
		contentPane.add(lblNewLabel);

		btOk = new JButton("Đồng Ý");
		btOk.setBackground(Color.WHITE);
		btOk.setIcon(new ImageIcon("img\\accept.png"));
		btOk.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btOk.setBounds(342, 529, 183, 71);
		contentPane.add(btOk);

		btClose = new JButton("Đóng");
		btClose.setBackground(Color.WHITE);
		btClose.setIcon(new ImageIcon("img\\cancel.png"));
		btClose.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btClose.setBounds(835, 529, 183, 71);
		contentPane.add(btClose);
		getIdBoss();
		clickCancel();
		loadData();
		clickOK();
	}

	public void loadData() {
		NhanVienBUS staffBus = new NhanVienBUS();
		ArrayList<NhanVienDTO> list = staffBus.loadDataByCondition("", "");
		Vector<String> row;

		for (NhanVienDTO staff : list) {
			row = new Vector<>();
			row.add(staff.getMaNV());
			row.add(staff.getHo());
			row.add(staff.getTen());
			row.add(staff.getGioiTinh());
			row.add(staff.getEmail());
			row.add(staff.getSoDT());
			row.add(staff.getSoCMND());
			row.add(staff.getDiaChi());
			row.add(staff.getMaPhongBan());
			row.add(staff.getMacv());
			row.add(staff.getMahdld());
			row.add(staff.getMaTDHV());
			model.addRow(row);
		}
		table.setModel(model);
	}

	public void clickOK() {
		btOk.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int k = table.getSelectedRow();
				if (k < 0) {
					JOptionPane.showMessageDialog(null, "Chọn nhân viên làm trưởng phòng");
				} else {
					truongphong = nhanvienBus.getNhanVienByID(newIdBoss);
					dto.setMaPhongBan(fThemPhongBan.getPhongbanDto().getMaPhongBan());
					dto.setDiaChi(fThemPhongBan.getPhongbanDto().getDiaChi());
					dto.setTruongPhong(newIdBoss);
					dto.setTenPhong(fThemPhongBan.getPhongbanDto().getTenPhong());
					// set dữ lieu len lai form them
					fThemPhongBan themPhong = new fThemPhongBan();
					themPhong.getTxtAddress().setText(dto.getDiaChi());
					themPhong.getTxtBoss().setText(truongphong.getHo() + " " + truongphong.getTen());
					themPhong.getTxtName().setText(dto.getTenPhong());
					themPhong.setVisible(true);
					setVisible(false);
				}

			}
		});
	}

	// lấy mã trưởng phòng mới
	public void getIdBoss() {
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				int k = table.getSelectedRow();
				if (((String) table.getValueAt(k, 9)).equals("TP") || ((String) table.getValueAt(k, 9)).equals("CEO")) {
					JOptionPane.showMessageDialog(null,
							"Nhân viên này đã là trưởng phòng của phòng khác! Vui lòng chọn lại!");
				} else {
					newIdBoss = (String) table.getValueAt(k, 0);
				}
			}
		});
	}

	public void clickCancel() {
		btClose.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
				new fThemPhongBan().setVisible(true);
				;
			}
		});
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public DefaultTableModel getModel() {
		return model;
	}

	public void setModel(DefaultTableModel model) {
		this.model = model;
	}

	public static String getNewIdBoss() {
		return newIdBoss;
	}

	public static void setNewIdBoss(String newIdBoss) {
		fDSNhanVien.newIdBoss = newIdBoss;
	}

	public static PhongBanDTO getDto() {
		return dto;
	}

	public static void setDto(PhongBanDTO dto) {
		fDSNhanVien.dto = dto;
	}

}
