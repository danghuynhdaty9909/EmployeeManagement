package GUI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import BUS.ChucVuBUS;
import BUS.HopDongLaoDongBUS;
import BUS.LuongBUS;
import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import BUS.PhuCapBUS;
import BUS.ThuongBUS;
import BUS.TrinhDoHocVanBUS;
import DAO.PhuCapDAO;
import DAO.TrinhDoHocVanDAO;
import DTO.HopDongLaoDongDTO;
import DTO.TrinhDoHocVanDTO;
import jdk.nashorn.internal.runtime.linker.JavaAdapterFactory;
import DTO.NhanVienDTO;

import javax.swing.BoxLayout;
import java.awt.FlowLayout;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.DefaultComboBoxModel;
import javax.swing.border.BevelBorder;
import java.awt.event.MouseAdapter;

@SuppressWarnings("serial")
public class fThemNhanVien extends JFrame {

	private JPanel contentPane;
	private JPanel panel;
	private JTextField txtFirstName;
	private JTextField txtLastName;
	private JTextField txtEmail;
	private JTextField txtId;
	private JTextField txtPhoneNumber;
	private JTextField txtIdCard;
	private JTable table;
	private JLabel lbAdd;
	private JLabel lbCancel;
	private JLabel lbReturn;
	private JTextField txtNation;
	private JTextField txtAddress;
	private JButton btAddImage;
	private String linkFile;
	private JLabel lbImg;
	private JRadioButton rbtFemale;
	private JRadioButton rbtMale;
	private JTextField txtAccount;
	private JComboBox<String> cbDepartmentName;
	private JComboBox<String> cbMonth;
	private JComboBox<String> cbDay;
	private JComboBox<String> cbYear;
	private JComboBox<String> cbPositionName;
	@SuppressWarnings("rawtypes")
	private JComboBox cbLiteracyName;
	@SuppressWarnings("rawtypes")
	private JComboBox<String> cbNgayKT;
	private JComboBox<String> cbThangKT;
	private JComboBox<String> cbNamKT;
	private JComboBox cbContract;
	private JTextField txtSpecialized;
	private TrinhDoHocVanBUS trinhdoBus = new TrinhDoHocVanBUS();
	private ChucVuBUS chucvuBus = new ChucVuBUS();
	private PhongBanBUS phongbanBus = new PhongBanBUS();
	private HopDongLaoDongBUS hopdongBus = new HopDongLaoDongBUS();
	private NhanVienBUS nhanvienBus = new NhanVienBUS();
	private ThuongBUS thuongBus = new ThuongBUS();
	private LuongBUS luongBus = new LuongBUS();
	private PhuCapBUS phucapBus = new PhuCapBUS();
	private JLabel lbTonGiao;
	private JTextField txtTonGiao;
	private JLabel lbLuongCoBan;
	private JComboBox<String> cbLuongCoBan;
	private JLabel lbHeSo;
	private JComboBox<String> cbHeSo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fThemNhanVien frame = new fThemNhanVien();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public fThemNhanVien() {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, you can set the GUI to another look
			// and feel.
		}
		setExtendedState(JFrame.MAXIMIZED_BOTH);// set full màn hình
		setTitle("THÊM NHÂN SỰ");
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\add1.png"));
		setBounds(100, 100, 1366, 768);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setForeground(Color.BLACK);
		panel.setBounds(150, -18, 1091, 736);
		panel.setLayout(null);

		JLabel lbFirstName = new JLabel("Họ:");
		lbFirstName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbFirstName.setBounds(310, 160, 30, 20);
		panel.add(lbFirstName);

		txtFirstName = new JTextField();
		txtFirstName.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtFirstName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtFirstName.setBounds(360, 160, 97, 20);
		panel.add(txtFirstName);
		txtFirstName.setColumns(10);

		JLabel lbLastName = new JLabel("Tên:");
		lbLastName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbLastName.setBounds(493, 160, 46, 20);
		panel.add(lbLastName);

		txtLastName = new JTextField();
		txtLastName.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtLastName.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		txtLastName.setBounds(549, 160, 121, 20);
		panel.add(txtLastName);
		txtLastName.setColumns(10);

		ButtonGroup bg = new ButtonGroup();

		rbtMale = new JRadioButton("Nam");
		bg.add(rbtMale);
		rbtMale.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rbtMale.setBounds(831, 160, 52, 23);
		panel.add(rbtMale);

		rbtFemale = new JRadioButton("Nữ");
		bg.add(rbtFemale);
		rbtFemale.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rbtFemale.setBounds(902, 160, 41, 23);
		panel.add(rbtFemale);

		JLabel lbId = new JLabel("Mã Nhân Viên:");
		lbId.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbId.setBounds(310, 260, 112, 20);
		panel.add(lbId);

		txtEmail = new JTextField();
		txtEmail.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtEmail.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtEmail.setBounds(455, 210, 215, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);

		JLabel lbNation = new JLabel("Dân tộc:");
		lbNation.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbNation.setBounds(740, 210, 61, 20);
		panel.add(lbNation);

		JLabel lbEmail = new JLabel("Email:");
		lbEmail.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbEmail.setBounds(310, 210, 46, 20);
		panel.add(lbEmail);

		JLabel lbPhoneNumber = new JLabel("Số Điện Thoại:");
		lbPhoneNumber.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbPhoneNumber.setBounds(310, 310, 105, 20);
		panel.add(lbPhoneNumber);

		JLabel lbIdCard = new JLabel("Số CMND:");
		lbIdCard.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbIdCard.setBounds(310, 360, 78, 20);
		panel.add(lbIdCard);

		JLabel lbDepartmentName = new JLabel("Tên Phòng Ban:");
		lbDepartmentName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbDepartmentName.setBounds(310, 460, 121, 20);
		panel.add(lbDepartmentName);

		JLabel lbPositionName = new JLabel("Tên Chức Vụ: ");
		lbPositionName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbPositionName.setBounds(310, 511, 105, 20);
		panel.add(lbPositionName);

		JLabel lbContractName = new JLabel("Loại HDLD:");
		lbContractName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbContractName.setBounds(740, 310, 97, 20);
		panel.add(lbContractName);

		JLabel lbLiteracyName = new JLabel("Tên TDHV:");
		lbLiteracyName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbLiteracyName.setBounds(740, 410, 97, 20);
		panel.add(lbLiteracyName);

		txtId = new JTextField();
		txtId.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtId.setBounds(455, 260, 215, 20);
		panel.add(txtId);
		txtId.setEnabled(false);
		txtId.setColumns(10);

		txtPhoneNumber = new JTextField();
		txtPhoneNumber.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPhoneNumber.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtPhoneNumber.setBounds(455, 310, 215, 20);
		panel.add(txtPhoneNumber);
		txtPhoneNumber.setColumns(10);

		txtIdCard = new JTextField();
		txtIdCard.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtIdCard.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtIdCard.setBounds(455, 360, 215, 20);
		panel.add(txtIdCard);
		txtIdCard.setColumns(10);

		JLabel lbAddress = new JLabel("Địa Chỉ:");
		lbAddress.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbAddress.setBounds(740, 260, 78, 20);
		panel.add(lbAddress);

		ArrayList<String> listPosition = chucvuBus.getListNameByCond("tencv", "", "");
		Vector<String> list = new Vector<>();
		if (listPosition != null) {
			for (String st : listPosition) {
				list.add(st);
			}
		}
		cbPositionName = new JComboBox(list);
		cbPositionName.setFont(new Font("Tahoma", Font.BOLD, 13));
		cbPositionName.setSelectedIndex(4);
		cbPositionName.setBounds(455, 511, 215, 21);
		panel.add(cbPositionName);

		ArrayList<String> listDepart = phongbanBus.getListOneCol("tenphong", "", "");// lấy
																						// danh
																						// sacch1
																						// tên
																						// phòng
		Vector<String> listNameDepart = new Vector<>();
		if (listDepart != null) {
			for (String st : listDepart) {
				listNameDepart.add(st);
			}
		}
		cbDepartmentName = new JComboBox(listNameDepart);
		cbDepartmentName.setFont(new Font("Tahoma", Font.BOLD, 13));
		cbDepartmentName.setBounds(455, 460, 215, 21);
		panel.add(cbDepartmentName);

		JLabel lbSex = new JLabel("Giới Tính:");
		lbSex.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbSex.setBounds(740, 160, 82, 20);
		panel.add(lbSex);

		table = new JTable();
		table.setBounds(1340, 699, -1043, -138);
		panel.add(table);

		getContentPane().add(panel);

		JLabel label = new JLabel("");
		label.setBounds(85, 170, 46, 14);
		panel.add(label);

		lbImg = new JLabel("");
		lbImg.setIcon(new ImageIcon("img\\addimage.png"));
		lbImg.setBounds(115, 76, 129, 156);
		panel.add(lbImg);

		btAddImage = new JButton("Thêm Ảnh");
		btAddImage.setFont(new Font("Tahoma", Font.BOLD, 11));
		btAddImage.setBounds(129, 240, 97, 23);
		panel.add(btAddImage);

		lbAdd = new JLabel("Thêm Nhân Viên");
		lbAdd.setFont(new Font("Tahoma", Font.BOLD, 11));
		lbAdd.setIcon(new ImageIcon("img\\add.png"));
		lbAdd.setBounds(455, 639, 147, 48);
		panel.add(lbAdd);

		lbCancel = new JLabel("Hủy");
		lbCancel.setIcon(new ImageIcon("img\\cancel3.png"));
		lbCancel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lbCancel.setBounds(740, 639, 97, 48);
		panel.add(lbCancel);

		JLabel lbAddTitle = new JLabel("THÊM NHÂN VIÊN");
		lbAddTitle.setForeground(Color.BLACK);
		lbAddTitle.setFont(new Font("Tahoma", Font.BOLD, 25));
		lbAddTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lbAddTitle.setBounds(323, 40, 495, 48);
		panel.add(lbAddTitle);

		lbReturn = new JLabel("");
		lbReturn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				dispose();
				fQuanLyNhanSu f = new fQuanLyNhanSu();
				f.setVisible(true);
			}
		});
		lbReturn.setIcon(new ImageIcon("img\\return.png"));
		lbReturn.setBounds(1000, 23, 91, 65);
		panel.add(lbReturn);

		txtNation = new JTextField();
		txtNation.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtNation.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtNation.setBounds(850, 210, 168, 20);
		panel.add(txtNation);
		txtNation.setColumns(10);

		txtAddress = new JTextField();
		txtAddress.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAddress.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtAddress.setBounds(850, 260, 168, 20);
		panel.add(txtAddress);
		txtAddress.setColumns(10);

		JLabel lbBirthday = new JLabel("Ngày Sinh:");
		lbBirthday.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbBirthday.setBounds(310, 410, 112, 20);
		panel.add(lbBirthday);

		cbDay = new JComboBox<String>();
		cbDay.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cbDay.setModel(new DefaultComboBoxModel(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09",
				"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26",
				"27", "28", "29", "30", "31" }));
		cbDay.setBounds(455, 410, 46, 20);
		panel.add(cbDay);

		JLabel lb1 = new JLabel("/");
		lb1.setVerticalAlignment(SwingConstants.TOP);
		lb1.setHorizontalAlignment(SwingConstants.CENTER);
		lb1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lb1.setBounds(493, 410, 30, 37);
		panel.add(lb1);

		cbMonth = new JComboBox<String>();
		cbMonth.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cbMonth.setModel(new DefaultComboBoxModel(
				new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));
		cbMonth.setBounds(518, 410, 46, 20);
		panel.add(cbMonth);

		JLabel lb2 = new JLabel("/");
		lb2.setVerticalAlignment(SwingConstants.TOP);
		lb2.setHorizontalAlignment(SwingConstants.CENTER);
		lb2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lb2.setBounds(560, 410, 30, 20);
		panel.add(lb2);

		cbYear = new JComboBox<String>();
		cbYear.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cbYear.setModel(new DefaultComboBoxModel(new String[] { "1980", "1981", "1982", "1983", "1984", "1985", "1986",
				"1987", "1988", "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998" }));
		cbYear.setBounds(600, 410, 70, 20);
		panel.add(cbYear);

		txtAccount = new JTextField();
		txtAccount.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAccount.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtAccount.setBounds(850, 511, 168, 20);
		panel.add(txtAccount);
		txtAccount.setColumns(10);

		JLabel lbAccount = new JLabel("Số tài khoản:");
		lbAccount.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbAccount.setBounds(740, 511, 97, 20);
		panel.add(lbAccount);

		ArrayList<String> listContract = new ArrayList<>();
		listContract = hopdongBus.getList("loaihopdong");
		Vector<String> listContractName = new Vector<>();
		if (listContract != null) {
			for (String st : listContract) {
				listContractName.add(st);
			}
		}
		cbContract = new JComboBox(listContractName);
		cbContract.setFont(new Font("Tahoma", Font.BOLD, 13));
		cbContract.setBounds(850, 310, 168, 20);
		panel.add(cbContract);

		ArrayList<String> listNameLiteracy = new ArrayList<>();
		listNameLiteracy = trinhdoBus.getListName("tentdhv");
		Vector<String> vtLiteracy = new Vector<>();
		if (listNameLiteracy != null) {
			for (String st : listNameLiteracy) {
				vtLiteracy.add(st);
			}
		}
		cbLiteracyName = new JComboBox(vtLiteracy);
		cbLiteracyName.setFont(new Font("Tahoma", Font.BOLD, 13));
		cbLiteracyName.setBounds(850, 409, 168, 20);
		panel.add(cbLiteracyName);

		txtSpecialized = new JTextField();
		txtSpecialized.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtSpecialized.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtSpecialized.setBounds(850, 462, 168, 20);
		panel.add(txtSpecialized);
		txtSpecialized.setColumns(10);

		JLabel lbSpecialized = new JLabel("Chuyên ngành:");
		lbSpecialized.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbSpecialized.setBounds(740, 460, 112, 20);
		panel.add(lbSpecialized);

		JLabel lbNgayKT = new JLabel("Ngày KT:");
		lbNgayKT.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbNgayKT.setBounds(740, 360, 75, 20);
		panel.add(lbNgayKT);

		cbNgayKT = new JComboBox<String>();
		cbNgayKT.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cbNgayKT.setModel(new DefaultComboBoxModel(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09",
				"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26",
				"27", "28", "29", "30", "31" }));
		cbNgayKT.setBounds(850, 360, 41, 20);
		panel.add(cbNgayKT);

		cbThangKT = new JComboBox<String>();
		cbThangKT.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cbThangKT.setModel(new DefaultComboBoxModel(
				new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));
		cbThangKT.setBounds(913, 360, 41, 20);
		panel.add(cbThangKT);

		cbNamKT = new JComboBox<String>();
		cbNamKT.setFont(new Font("Tahoma", Font.PLAIN, 13));
		cbNamKT.setModel(new DefaultComboBoxModel(new String[] { "2016", "2107", "2018", "2019", "2020", "2021", "2022",
				"2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030" }));
		cbNamKT.setBounds(964, 360, 54, 20);
		panel.add(cbNamKT);

		JLabel label_1 = new JLabel("/");
		label_1.setVerticalAlignment(SwingConstants.TOP);
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_1.setBounds(887, 360, 30, 20);
		panel.add(label_1);

		JLabel label_2 = new JLabel("/");
		label_2.setVerticalAlignment(SwingConstants.TOP);
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_2.setBounds(951, 360, 20, 20);
		panel.add(label_2);

		lbTonGiao = new JLabel("Tôn giáo:");
		lbTonGiao.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbTonGiao.setBounds(310, 554, 105, 20);
		panel.add(lbTonGiao);

		txtTonGiao = new JTextField();
		txtTonGiao.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtTonGiao.setColumns(10);
		txtTonGiao.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtTonGiao.setBounds(455, 554, 215, 20);
		panel.add(txtTonGiao);

		lbLuongCoBan = new JLabel("Lương cơ bản:");
		lbLuongCoBan.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbLuongCoBan.setBounds(740, 554, 112, 20);
		panel.add(lbLuongCoBan);

		ArrayList<String> listLuong = new ArrayList<>();
		listLuong = luongBus.getOneCol("luongcoban");
		Vector<String> vtLuong = new Vector<>();
		if (listLuong != null) {
			for (String st : listLuong) {
				vtLuong.add(st);
			}
		}
		cbLuongCoBan = new JComboBox<String>(vtLuong);
		cbLuongCoBan.setFont(new Font("Tahoma", Font.BOLD, 14));
		cbLuongCoBan.setBackground(Color.WHITE);
		cbLuongCoBan.setBounds(850, 554, 168, 20);
		panel.add(cbLuongCoBan);

		lbHeSo = new JLabel("Hệ số PC:");
		lbHeSo.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbHeSo.setBounds(310, 600, 112, 20);
		panel.add(lbHeSo);

		ArrayList<String> listPhuCap = new ArrayList<>();
		listPhuCap = phucapBus.getOneCol("hesophucap");
		Vector<String> vtPhucap = new Vector<>();
		if (listPhuCap != null) {
			for (String st : listPhuCap) {
				vtPhucap.add(st);
			}
		}
		cbHeSo = new JComboBox<String>(vtPhucap);
		cbHeSo.setFont(new Font("Tahoma", Font.BOLD, 14));
		cbHeSo.setBackground(Color.WHITE);
		cbHeSo.setBounds(455, 600, 121, 20);
		panel.add(cbHeSo);

		clickThem();
		clickHuy();
		// clickReturn();
		clickThemAnh();
	}

	public void clickThem() {
		lbAdd.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {
				lbAdd.setForeground(null);

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				lbAdd.setForeground(Color.green);

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				int respone = JOptionPane.showConfirmDialog(null, "Bạn có chắc chắn thêm nhân viên này?", "Confirm",
						JOptionPane.YES_NO_OPTION);

				if (respone == JOptionPane.YES_OPTION) {
					String sex = "";
					if (rbtMale.isSelected()) {
						sex = "Nam";
					} else {
						sex = "Nữ";
					}
					StringBuilder birthday = new StringBuilder();
					birthday.append(cbYear.getSelectedItem()).append("-").append(cbMonth.getSelectedItem()).append("-")
							.append(cbDay.getSelectedItem());

					String ngaySinh = birthday.toString();
				
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					Date date = null;
					date = Date.valueOf(ngaySinh);
					System.out.println(date);
					String maHD = hopdongBus.createNewId();// lấy id mới của
															// hopdaonglaodong;
					// chua add hdld
					java.util.Date now = new java.util.Date();
					HopDongLaoDongDTO hopdongDto = new HopDongLaoDongDTO();// tao
																			// doi
																			// tuong
					// hop dong lao
					// dong
					hopdongDto.setNgayBatDau(Date.valueOf(df.format(now)));
					hopdongDto.setNgayKy(Date.valueOf(df.format(now)));
					hopdongDto.setMahdld(maHD);
					hopdongDto.setLoaiHopDong((String) cbContract.getSelectedItem());
					// tao ngay ket thuc hop dong
					StringBuilder ngayKt = new StringBuilder();
					ngayKt.append(cbNamKT.getSelectedItem()).append("-").append(cbThangKT.getSelectedItem()).append("-")
							.append(cbNgayKT.getSelectedItem());

					Date ngayKT;
					ngayKT = Date.valueOf(ngayKt.toString());
					hopdongDto.setNgayKetThuc(ngayKT);
					// insert hopdonglaodong;
					boolean flag_1 = hopdongBus.insert(hopdongDto);
					if (!flag_1) {
						JOptionPane.showMessageDialog(null, "Thêm thất bại,hopdong!");
						return;
					}
					String maTrinhdo = "";// id trình độ học vấn
					boolean flag = trinhdoBus.check("tentdhv", (String) cbLiteracyName.getSelectedItem(), "chuyennganh",
							txtSpecialized.getText());
					System.out.println(flag);
					// boolean flag_2 = contractBus.insert(contractDto);
					if (flag) {// chưa tồn tại row này
						maTrinhdo = trinhdoBus.createNewId();// tao cai id moi
						// thêm trình đô hoc vấn
						@SuppressWarnings("unused")
						TrinhDoHocVanDTO trinhdoDto = new TrinhDoHocVanDTO(maTrinhdo,
								(String) cbLiteracyName.getSelectedItem(), txtSpecialized.getText());

						boolean flag_3 = trinhdoBus.insert(trinhdoDto);
						if (!flag_3) {
							JOptionPane.showMessageDialog(null, "Thêm thất bại,hopdong!");
							return;
						}

					} else {
						maTrinhdo = trinhdoBus.getIdByCondition((String) cbLiteracyName.getSelectedItem(),
								txtSpecialized.getText());
					}
					String id = nhanvienBus.createNewId();// id nhân viên
					NhanVienDTO nhanvienDto = new NhanVienDTO(id, txtFirstName.getText(), txtLastName.getText(), sex,
							date, txtAddress.getText(), txtNation.getText(), txtIdCard.getText(),
							txtPhoneNumber.getText(), txtEmail.getText(), txtAccount.getText(), linkFile,
							hopdongDto.getMahdld(),
							chucvuBus.getIdPositionByName((String) cbPositionName.getSelectedItem()),
							phongbanBus.getIdDepartment((String) cbDepartmentName.getSelectedItem()), maTrinhdo,
							txtTonGiao.getText(),
							luongBus.getOneCol(Integer.parseInt((String) cbLuongCoBan.getSelectedItem())),
							phucapBus.getId(Double.parseDouble((String) cbHeSo.getSelectedItem())));
					if (nhanvienBus.insert(nhanvienDto)) {
						JOptionPane.showMessageDialog(null, "Thêm thành công!");
						refresh();
					} else {
						JOptionPane.showMessageDialog(null, "Thêm thất bại!");
					}
				}
			}
		});
	}

	public void clickHuy() {
		lbCancel.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {
				lbCancel.setForeground(null);

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				lbCancel.setForeground(Color.RED);

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new fQuanLyNhanSu().setVisible(true);

			}
		});
	}

	public void clickReturn() {
		lbReturn.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new fQuanLyNhanSu().setVisible(true);

			}
		});
	}

	public void clickThemAnh() {
		btAddImage.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> linkList = nhanvienBus.getListOneCol("hinhanh");
				boolean flag;
				do {
					flag = true;
					JFileChooser fileChooser = new JFileChooser();
					int returnValue = fileChooser.showOpenDialog(contentPane);
					if (returnValue == JFileChooser.APPROVE_OPTION) {
						File selectedFile = fileChooser.getSelectedFile();
						linkFile = selectedFile.getName().toString();
						for (String st : linkList) {
							if (st.equals(linkFile)) {
								JOptionPane.showMessageDialog(null,
										"Ảnh này đã bị trùng với nhân viên khác!! Vui lòng chọn lại ảnh.");
								flag = false;
								break;
							}
						}
					}
				} while (!flag);
				lbImg.setIcon(new ImageIcon("img\\imgnhansu\\" + linkFile));

			}
		});
	}
	public void refresh() {
		txtAccount.setText("");
		txtAddress.setText("");
		txtEmail.setText("");
		txtFirstName.setText("");
		txtLastName.setText("");
		txtId.setText("");
		txtIdCard.setText("");
		txtNation.setText("");
		txtPhoneNumber.setText("");
		txtSpecialized.setText("");
		lbImg.setIcon(new ImageIcon("img\\addimage.png"));
		cbContract.setSelectedIndex(0);
		cbDay.setSelectedIndex(0);
		cbMonth.setSelectedIndex(0);
		cbYear.setSelectedIndex(0);
		cbDepartmentName.setSelectedIndex(0);
		cbPositionName.setSelectedIndex(0);
		cbLiteracyName.setSelectedIndex(0);
		rbtFemale.setSelected(false);
		rbtMale.setSelected(false);
		txtTonGiao.setText("");
	}

}
