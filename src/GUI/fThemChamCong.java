package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.TitledBorder;

import BUS.ChamCongBUS;
import BUS.LuongBUS;
import BUS.LuongChiTietBUS;
import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import BUS.ThuongBUS;
import DTO.ChamCongDTO;
import DTO.LuongChiTietDTO;
import DTO.LuongDTO;
import DTO.NhanVienDTO;
import DTO.PhongBanDTO;
import DTO.ThuongDTO;
import jdk.nashorn.internal.scripts.JO;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Calendar;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import java.awt.GridLayout;
import javax.swing.border.LineBorder;

public class fThemChamCong extends JFrame {

	private JPanel contentPane;
	private JTextField txfMaNV;
	private JTextField txfTen;
	private JTextField txfPhong;
	private JTextField txfThang;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fThemChamCong frame = new fThemChamCong();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public fThemChamCong() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent arg0) {
				NhanVienDTO nv = NhanVienBUS.getSttNhanVienDTO();
				txfMaNV.setText(nv.getMaNV());
				txfTen.setText(nv.getHo() + " " + nv.getTen());
				for (PhongBanDTO item : PhongBanBUS.getSttArrayPhongBanDTO()) {
					if (item.getMaPhongBan().equals(nv.getMaPhongBan())) {
						txfPhong.setText(item.getTenPhong());
						break;
					}
				}
			}
		});
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, you can set the GUI to another look
			// and feel.
		}
		setTitle("Thêm chấm công nhân viên");
		setExtendedState(JFrame.MAXIMIZED_BOTH);// set full màn hình
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		contentPane = new JPanel();
		setLocationRelativeTo(null);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel header = new JPanel();
		header.setBounds(10, 11, 1342, 40);
		contentPane.add(header);
		header.setLayout(null);

		JLabel lblHeader = new JLabel("Thêm Chấm Công");
		lblHeader.setHorizontalAlignment(SwingConstants.CENTER);
		lblHeader.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblHeader.setBounds(10, 0, 1322, 40);
		header.add(lblHeader);

		JPanel body = new JPanel();
		body.setBounds(20, 52, 1342, 642);
		contentPane.add(body);
		body.setLayout(null);

		JPanel chonnhanvien = new JPanel();
		chonnhanvien.setBorder(
				new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Th\u00F4ng tin nh\u00E2n vi\u00EAn",
						TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		chonnhanvien.setBounds(10, 11, 1322, 65);
		body.add(chonnhanvien);
		chonnhanvien.setLayout(null);

		JLabel lblMNhnVin = new JLabel("Mã nhân Viên");
		lblMNhnVin.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMNhnVin.setBounds(50, 23, 98, 25);
		chonnhanvien.add(lblMNhnVin);

		txfMaNV = new JTextField();
		txfMaNV.setHorizontalAlignment(SwingConstants.CENTER);
		txfMaNV.setEditable(false);
		txfMaNV.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txfMaNV.setBounds(170, 23, 70, 25);
		chonnhanvien.add(txfMaNV);
		txfMaNV.setColumns(10);

		JButton btnDSNV = new JButton("...");
		btnDSNV.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				fDanhSachNhanVien f = new fDanhSachNhanVien();
				f.setVisible(true);

			}
		});
		btnDSNV.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnDSNV.setBounds(270, 23, 40, 25);
		chonnhanvien.add(btnDSNV);

		JLabel lblTnNhnVin = new JLabel("Tên nhân Viên");
		lblTnNhnVin.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblTnNhnVin.setBounds(360, 23, 110, 25);
		chonnhanvien.add(lblTnNhnVin);

		txfTen = new JTextField();
		txfTen.setHorizontalAlignment(SwingConstants.CENTER);
		txfTen.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txfTen.setEditable(false);
		txfTen.setColumns(10);
		txfTen.setBounds(500, 23, 170, 25);
		chonnhanvien.add(txfTen);

		JLabel lblPhng = new JLabel("Phòng ");
		lblPhng.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPhng.setBounds(730, 23, 50, 25);
		chonnhanvien.add(lblPhng);

		txfPhong = new JTextField();
		txfPhong.setHorizontalAlignment(SwingConstants.CENTER);
		txfPhong.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txfPhong.setEditable(false);
		txfPhong.setColumns(10);
		txfPhong.setBounds(815, 23, 200, 25);
		chonnhanvien.add(txfPhong);

		JLabel lblThng = new JLabel("Tháng");
		lblThng.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblThng.setBounds(1060, 23, 50, 25);
		chonnhanvien.add(lblThng);
		java.util.Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int month = cal.get(Calendar.MONTH);
		txfThang = new JTextField();
		txfThang.setText(String.valueOf(month + 1));
		txfThang.setHorizontalAlignment(SwingConstants.CENTER);
		txfThang.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txfThang.setEditable(false);
		txfThang.setColumns(10);
		txfThang.setBounds(1120, 23, 130, 25);
		chonnhanvien.add(txfThang);

		JPanel body_1 = new JPanel();
		body_1.setBorder(new TitledBorder(null, "Ch\u1EA5m c\u00F4ng nh\u00E2n vi\u00EAn", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		body_1.setBounds(10, 76, 1322, 490);
		body.add(body_1);
		body_1.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 645, 468);
		body_1.add(panel);
		panel.setLayout(new GridLayout(16, 1, 0, 0));

		JPanel panel_3 = new JPanel();
		panel_3.setLayout(null);
		panel_3.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_3);

		JCheckBox ckb1 = new JCheckBox("Ngày 1");
		ckb1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb1.setBounds(80, 4, 97, 20);
		panel_3.add(ckb1);

		JLabel label = new JLabel("Làm tăng ca");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(205, 4, 100, 20);
		panel_3.add(label);

		JComboBox cbb1 = new JComboBox();
		cbb1.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb1.setBounds(315, 4, 100, 20);
		panel_3.add(cbb1);

		JLabel label_1 = new JLabel("Giờ");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_1.setBounds(445, 4, 70, 20);
		panel_3.add(label_1);

		JPanel panel_4 = new JPanel();
		panel_4.setLayout(null);
		panel_4.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_4);

		JCheckBox ckb2 = new JCheckBox("Ngày 2");
		ckb2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb2.setBounds(80, 4, 97, 20);
		panel_4.add(ckb2);

		JLabel label_2 = new JLabel("Làm tăng ca");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_2.setBounds(205, 4, 100, 20);
		panel_4.add(label_2);

		JComboBox cbb2 = new JComboBox();
		cbb2.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb2.setBounds(315, 4, 100, 20);
		panel_4.add(cbb2);

		JLabel label_3 = new JLabel("Giờ");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_3.setBounds(445, 4, 70, 20);
		panel_4.add(label_3);

		JPanel panel_5 = new JPanel();
		panel_5.setLayout(null);
		panel_5.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_5);

		JCheckBox ckb3 = new JCheckBox("Ngày 3");
		ckb3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb3.setBounds(80, 4, 97, 20);
		panel_5.add(ckb3);

		JLabel label_4 = new JLabel("Làm tăng ca");
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_4.setBounds(205, 4, 100, 20);
		panel_5.add(label_4);

		JComboBox cbb3 = new JComboBox();
		cbb3.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb3.setBounds(315, 4, 100, 20);
		panel_5.add(cbb3);

		JLabel label_5 = new JLabel("Giờ");
		label_5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_5.setBounds(445, 4, 70, 20);
		panel_5.add(label_5);

		JPanel panel_6 = new JPanel();
		panel_6.setLayout(null);
		panel_6.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_6);

		JCheckBox ckb4 = new JCheckBox("Ngày 4");
		ckb4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb4.setBounds(80, 4, 97, 20);
		panel_6.add(ckb4);

		JLabel label_6 = new JLabel("Làm tăng ca");
		label_6.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_6.setBounds(205, 4, 100, 20);
		panel_6.add(label_6);

		JComboBox cbb4 = new JComboBox();
		cbb4.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb4.setBounds(315, 4, 100, 20);
		panel_6.add(cbb4);

		JLabel label_7 = new JLabel("Giờ");
		label_7.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_7.setBounds(445, 4, 70, 20);
		panel_6.add(label_7);

		JPanel panel_7 = new JPanel();
		panel_7.setLayout(null);
		panel_7.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_7);

		JCheckBox ckb5 = new JCheckBox("Ngày 5");
		ckb5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb5.setBounds(80, 4, 97, 20);
		panel_7.add(ckb5);

		JLabel label_8 = new JLabel("Làm tăng ca");
		label_8.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_8.setBounds(205, 4, 100, 20);
		panel_7.add(label_8);

		JComboBox cbb5 = new JComboBox();
		cbb5.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb5.setBounds(315, 4, 100, 20);
		panel_7.add(cbb5);

		JLabel label_9 = new JLabel("Giờ");
		label_9.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_9.setBounds(445, 4, 70, 20);
		panel_7.add(label_9);

		JPanel panel_8 = new JPanel();
		panel_8.setLayout(null);
		panel_8.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_8);

		JCheckBox ckb6 = new JCheckBox("Ngày 6");
		ckb6.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb6.setBounds(80, 4, 97, 20);
		panel_8.add(ckb6);

		JLabel label_10 = new JLabel("Làm tăng ca");
		label_10.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_10.setBounds(205, 4, 100, 20);
		panel_8.add(label_10);

		JComboBox cbb6 = new JComboBox();
		cbb6.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb6.setBounds(315, 4, 100, 20);
		panel_8.add(cbb6);

		JLabel label_11 = new JLabel("Giờ");
		label_11.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_11.setBounds(445, 4, 70, 20);
		panel_8.add(label_11);

		JPanel panel_9 = new JPanel();
		panel_9.setLayout(null);
		panel_9.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_9);

		JCheckBox ckb7 = new JCheckBox("Ngày 7");
		ckb7.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb7.setBounds(80, 4, 97, 20);
		panel_9.add(ckb7);

		JLabel label_12 = new JLabel("Làm tăng ca");
		label_12.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_12.setBounds(205, 4, 100, 20);
		panel_9.add(label_12);

		JComboBox cbb7 = new JComboBox();
		cbb7.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb7.setBounds(315, 4, 100, 20);
		panel_9.add(cbb7);

		JLabel label_13 = new JLabel("Giờ");
		label_13.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_13.setBounds(445, 4, 70, 20);
		panel_9.add(label_13);

		JPanel panel_10 = new JPanel();
		panel_10.setLayout(null);
		panel_10.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_10);

		JCheckBox ckb8 = new JCheckBox("Ngày 8");
		ckb8.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb8.setBounds(80, 4, 97, 20);
		panel_10.add(ckb8);

		JLabel label_14 = new JLabel("Làm tăng ca");
		label_14.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_14.setBounds(205, 4, 100, 20);
		panel_10.add(label_14);

		JComboBox cbb8 = new JComboBox();
		cbb8.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb8.setBounds(315, 4, 100, 20);
		panel_10.add(cbb8);

		JLabel label_15 = new JLabel("Giờ");
		label_15.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_15.setBounds(445, 4, 70, 20);
		panel_10.add(label_15);

		JPanel panel_11 = new JPanel();
		panel_11.setLayout(null);
		panel_11.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_11);

		JCheckBox ckb9 = new JCheckBox("Ngày 9");
		ckb9.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb9.setBounds(80, 4, 97, 20);
		panel_11.add(ckb9);

		JLabel label_16 = new JLabel("Làm tăng ca");
		label_16.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_16.setBounds(205, 4, 100, 20);
		panel_11.add(label_16);

		JComboBox cbb9 = new JComboBox();
		cbb9.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb9.setBounds(315, 4, 100, 20);
		panel_11.add(cbb9);

		JLabel label_17 = new JLabel("Giờ");
		label_17.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_17.setBounds(445, 4, 70, 20);
		panel_11.add(label_17);

		JPanel panel_12 = new JPanel();
		panel_12.setLayout(null);
		panel_12.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_12);

		JCheckBox ckb10 = new JCheckBox("Ngày 10");
		ckb10.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb10.setBounds(80, 4, 97, 20);
		panel_12.add(ckb10);

		JLabel label_18 = new JLabel("Làm tăng ca");
		label_18.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_18.setBounds(205, 4, 100, 20);
		panel_12.add(label_18);

		JComboBox cbb10 = new JComboBox();
		cbb10.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb10.setBounds(315, 4, 100, 20);
		panel_12.add(cbb10);

		JLabel label_19 = new JLabel("Giờ");
		label_19.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_19.setBounds(445, 4, 70, 20);
		panel_12.add(label_19);

		JPanel panel_13 = new JPanel();
		panel_13.setLayout(null);
		panel_13.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_13);

		JCheckBox ckb11 = new JCheckBox("Ngày 11");
		ckb11.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb11.setBounds(80, 4, 97, 20);
		panel_13.add(ckb11);

		JLabel label_20 = new JLabel("Làm tăng ca");
		label_20.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_20.setBounds(205, 4, 100, 20);
		panel_13.add(label_20);

		JComboBox cbb11 = new JComboBox();
		cbb11.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb11.setBounds(315, 4, 100, 20);
		panel_13.add(cbb11);

		JLabel label_21 = new JLabel("Giờ");
		label_21.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_21.setBounds(445, 4, 70, 20);
		panel_13.add(label_21);

		JPanel panel_14 = new JPanel();
		panel_14.setLayout(null);
		panel_14.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_14);

		JCheckBox ckb12 = new JCheckBox("Ngày 12");
		ckb12.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb12.setBounds(80, 4, 97, 20);
		panel_14.add(ckb12);

		JLabel label_22 = new JLabel("Làm tăng ca");
		label_22.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_22.setBounds(205, 4, 100, 20);
		panel_14.add(label_22);

		JComboBox cbb12 = new JComboBox();
		cbb12.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb12.setBounds(315, 4, 100, 20);
		panel_14.add(cbb12);

		JLabel label_23 = new JLabel("Giờ");
		label_23.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_23.setBounds(445, 4, 70, 20);
		panel_14.add(label_23);

		JPanel panel_15 = new JPanel();
		panel_15.setLayout(null);
		panel_15.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_15);

		JCheckBox ckb13 = new JCheckBox("Ngày 13");
		ckb13.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb13.setBounds(80, 4, 97, 20);
		panel_15.add(ckb13);

		JLabel label_24 = new JLabel("Làm tăng ca");
		label_24.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_24.setBounds(205, 4, 100, 20);
		panel_15.add(label_24);

		JComboBox cbb13 = new JComboBox();
		cbb13.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb13.setBounds(315, 4, 100, 20);
		panel_15.add(cbb13);

		JLabel label_25 = new JLabel("Giờ");
		label_25.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_25.setBounds(445, 4, 70, 20);
		panel_15.add(label_25);

		JPanel panel_16 = new JPanel();
		panel_16.setLayout(null);
		panel_16.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_16);

		JCheckBox ckb14 = new JCheckBox("Ngày 14");
		ckb14.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb14.setBounds(80, 4, 97, 20);
		panel_16.add(ckb14);

		JLabel label_26 = new JLabel("Làm tăng ca");
		label_26.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_26.setBounds(205, 4, 100, 20);
		panel_16.add(label_26);

		JComboBox cbb14 = new JComboBox();
		cbb14.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb14.setBounds(315, 4, 100, 20);
		panel_16.add(cbb14);

		JLabel label_27 = new JLabel("Giờ");
		label_27.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_27.setBounds(445, 4, 70, 20);
		panel_16.add(label_27);

		JPanel panel_17 = new JPanel();
		panel_17.setLayout(null);
		panel_17.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_17);

		JCheckBox ckb15 = new JCheckBox("Ngày 15");
		ckb15.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb15.setBounds(80, 4, 97, 20);
		panel_17.add(ckb15);

		JLabel label_28 = new JLabel("Làm tăng ca");
		label_28.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_28.setBounds(205, 4, 100, 20);
		panel_17.add(label_28);

		JComboBox cbb15 = new JComboBox();
		cbb15.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb15.setBounds(315, 4, 100, 20);
		panel_17.add(cbb15);

		JLabel label_29 = new JLabel("Giờ");
		label_29.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_29.setBounds(445, 4, 70, 20);
		panel_17.add(label_29);

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_2);
		panel_2.setLayout(null);

		JCheckBox ckb16 = new JCheckBox("Ngày 16");
		ckb16.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb16.setBounds(80, 4, 97, 20);
		panel_2.add(ckb16);

		JLabel lblNewLabel = new JLabel("Làm tăng ca");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(205, 4, 100, 20);
		panel_2.add(lblNewLabel);

		JComboBox cbb16 = new JComboBox();
		cbb16.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb16.setBounds(315, 4, 100, 20);
		panel_2.add(cbb16);

		JLabel lblGi = new JLabel("Giờ");
		lblGi.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblGi.setBounds(445, 4, 70, 20);
		panel_2.add(lblGi);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(655, 11, 645, 468);
		body_1.add(panel_1);
		panel_1.setLayout(new GridLayout(16, 1, 0, 0));

		JPanel panel_19 = new JPanel();
		panel_19.setLayout(null);
		panel_19.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_19);

		JCheckBox ckb17 = new JCheckBox("Ngày 17");
		ckb17.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb17.setBounds(80, 4, 97, 20);
		panel_19.add(ckb17);

		JLabel label_32 = new JLabel("Làm tăng ca");
		label_32.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_32.setBounds(205, 4, 100, 20);
		panel_19.add(label_32);

		JComboBox cbb17 = new JComboBox();
		cbb17.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb17.setBounds(315, 4, 100, 20);
		panel_19.add(cbb17);

		JLabel label_33 = new JLabel("Giờ");
		label_33.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_33.setBounds(445, 4, 70, 20);
		panel_19.add(label_33);

		JPanel panel_20 = new JPanel();
		panel_20.setLayout(null);
		panel_20.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_20);

		JCheckBox ckb18 = new JCheckBox("Ngày 18");
		ckb18.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb18.setBounds(80, 4, 97, 20);
		panel_20.add(ckb18);

		JLabel label_34 = new JLabel("Làm tăng ca");
		label_34.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_34.setBounds(205, 4, 100, 20);
		panel_20.add(label_34);

		JComboBox cbb18 = new JComboBox();
		cbb18.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb18.setBounds(315, 4, 100, 20);
		panel_20.add(cbb18);

		JLabel label_35 = new JLabel("Giờ");
		label_35.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_35.setBounds(445, 4, 70, 20);
		panel_20.add(label_35);

		JPanel panel_21 = new JPanel();
		panel_21.setLayout(null);
		panel_21.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_21);

		JCheckBox ckb19 = new JCheckBox("Ngày 19");
		ckb19.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb19.setBounds(80, 4, 97, 20);
		panel_21.add(ckb19);

		JLabel label_36 = new JLabel("Làm tăng ca");
		label_36.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_36.setBounds(205, 4, 100, 20);
		panel_21.add(label_36);

		JComboBox cbb19 = new JComboBox();
		cbb19.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb19.setBounds(315, 4, 100, 20);
		panel_21.add(cbb19);

		JLabel label_37 = new JLabel("Giờ");
		label_37.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_37.setBounds(445, 4, 70, 20);
		panel_21.add(label_37);

		JPanel panel_22 = new JPanel();
		panel_22.setLayout(null);
		panel_22.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_22);

		JCheckBox ckb20 = new JCheckBox("Ngày 20");
		ckb20.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb20.setBounds(80, 4, 97, 20);
		panel_22.add(ckb20);

		JLabel label_38 = new JLabel("Làm tăng ca");
		label_38.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_38.setBounds(205, 4, 100, 20);
		panel_22.add(label_38);

		JComboBox cbb20 = new JComboBox();
		cbb20.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb20.setBounds(315, 4, 100, 20);
		panel_22.add(cbb20);

		JLabel label_39 = new JLabel("Giờ");
		label_39.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_39.setBounds(445, 4, 70, 20);
		panel_22.add(label_39);

		JPanel panel_23 = new JPanel();
		panel_23.setLayout(null);
		panel_23.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_23);

		JCheckBox ckb21 = new JCheckBox("Ngày 21");
		ckb21.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb21.setBounds(80, 4, 97, 20);
		panel_23.add(ckb21);

		JLabel label_40 = new JLabel("Làm tăng ca");
		label_40.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_40.setBounds(205, 4, 100, 20);
		panel_23.add(label_40);

		JComboBox cbb21 = new JComboBox();
		cbb21.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb21.setBounds(315, 4, 100, 20);
		panel_23.add(cbb21);

		JLabel label_41 = new JLabel("Giờ");
		label_41.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_41.setBounds(445, 4, 70, 20);
		panel_23.add(label_41);

		JPanel panel_24 = new JPanel();
		panel_24.setLayout(null);
		panel_24.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_24);

		JCheckBox ckb22 = new JCheckBox("Ngày 22");
		ckb22.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb22.setBounds(80, 4, 97, 20);
		panel_24.add(ckb22);

		JLabel label_42 = new JLabel("Làm tăng ca");
		label_42.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_42.setBounds(205, 4, 100, 20);
		panel_24.add(label_42);

		JComboBox cbb22 = new JComboBox();
		cbb22.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb22.setBounds(315, 4, 100, 20);
		panel_24.add(cbb22);

		JLabel label_43 = new JLabel("Giờ");
		label_43.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_43.setBounds(445, 4, 70, 20);
		panel_24.add(label_43);

		JPanel panel_25 = new JPanel();
		panel_25.setLayout(null);
		panel_25.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_25);

		JCheckBox ckb23 = new JCheckBox("Ngày 23");
		ckb23.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb23.setBounds(80, 4, 97, 20);
		panel_25.add(ckb23);

		JLabel label_44 = new JLabel("Làm tăng ca");
		label_44.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_44.setBounds(205, 4, 100, 20);
		panel_25.add(label_44);

		JComboBox cbb23 = new JComboBox();
		cbb23.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb23.setBounds(315, 4, 100, 20);
		panel_25.add(cbb23);

		JLabel label_45 = new JLabel("Giờ");
		label_45.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_45.setBounds(445, 4, 70, 20);
		panel_25.add(label_45);

		JPanel panel_26 = new JPanel();
		panel_26.setLayout(null);
		panel_26.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_26);

		JCheckBox ckb24 = new JCheckBox("Ngày 24");
		ckb24.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb24.setBounds(80, 4, 97, 20);
		panel_26.add(ckb24);

		JLabel label_46 = new JLabel("Làm tăng ca");
		label_46.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_46.setBounds(205, 4, 100, 20);
		panel_26.add(label_46);

		JComboBox cbb24 = new JComboBox();
		cbb24.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb24.setBounds(315, 4, 100, 20);
		panel_26.add(cbb24);

		JLabel label_47 = new JLabel("Giờ");
		label_47.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_47.setBounds(445, 4, 70, 20);
		panel_26.add(label_47);

		JPanel panel_27 = new JPanel();
		panel_27.setLayout(null);
		panel_27.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_27);

		JCheckBox ckb25 = new JCheckBox("Ngày 25");
		ckb25.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb25.setBounds(80, 4, 97, 20);
		panel_27.add(ckb25);

		JLabel label_48 = new JLabel("Làm tăng ca");
		label_48.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_48.setBounds(205, 4, 100, 20);
		panel_27.add(label_48);

		JComboBox cbb25 = new JComboBox();
		cbb25.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb25.setBounds(315, 4, 100, 20);
		panel_27.add(cbb25);

		JLabel label_49 = new JLabel("Giờ");
		label_49.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_49.setBounds(445, 4, 70, 20);
		panel_27.add(label_49);

		JPanel panel_28 = new JPanel();
		panel_28.setLayout(null);
		panel_28.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_28);

		JCheckBox ckb26 = new JCheckBox("Ngày 26");
		ckb26.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb26.setBounds(80, 4, 97, 20);
		panel_28.add(ckb26);

		JLabel label_50 = new JLabel("Làm tăng ca");
		label_50.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_50.setBounds(205, 4, 100, 20);
		panel_28.add(label_50);

		JComboBox cbb26 = new JComboBox();
		cbb26.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb26.setBounds(315, 4, 100, 20);
		panel_28.add(cbb26);

		JLabel label_51 = new JLabel("Giờ");
		label_51.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_51.setBounds(445, 4, 70, 20);
		panel_28.add(label_51);

		JPanel panel_29 = new JPanel();
		panel_29.setLayout(null);
		panel_29.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_29);

		JCheckBox ckb27 = new JCheckBox("Ngày 27");
		ckb27.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb27.setBounds(80, 4, 97, 20);
		panel_29.add(ckb27);

		JLabel label_52 = new JLabel("Làm tăng ca");
		label_52.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_52.setBounds(205, 4, 100, 20);
		panel_29.add(label_52);

		JComboBox cbb27 = new JComboBox();
		cbb27.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb27.setBounds(315, 4, 100, 20);
		panel_29.add(cbb27);

		JLabel label_53 = new JLabel("Giờ");
		label_53.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_53.setBounds(445, 4, 70, 20);
		panel_29.add(label_53);

		JPanel panel_30 = new JPanel();
		panel_30.setLayout(null);
		panel_30.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_30);

		JCheckBox ckb28 = new JCheckBox("Ngày 28");
		ckb28.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb28.setBounds(80, 4, 97, 20);
		panel_30.add(ckb28);

		JLabel label_54 = new JLabel("Làm tăng ca");
		label_54.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_54.setBounds(205, 4, 100, 20);
		panel_30.add(label_54);

		JComboBox cbb28 = new JComboBox();
		cbb28.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb28.setBounds(315, 4, 100, 20);
		panel_30.add(cbb28);

		JLabel label_55 = new JLabel("Giờ");
		label_55.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_55.setBounds(445, 4, 70, 20);
		panel_30.add(label_55);

		JPanel panel_31 = new JPanel();
		panel_31.setLayout(null);
		panel_31.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_31);

		JCheckBox ckb29 = new JCheckBox("Ngày 29");
		ckb29.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb29.setBounds(80, 4, 97, 20);
		panel_31.add(ckb29);

		JLabel label_56 = new JLabel("Làm tăng ca");
		label_56.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_56.setBounds(205, 4, 100, 20);
		panel_31.add(label_56);

		JComboBox cbb29 = new JComboBox();
		cbb29.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb29.setBounds(315, 4, 100, 20);
		panel_31.add(cbb29);

		JLabel label_57 = new JLabel("Giờ");
		label_57.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_57.setBounds(445, 4, 70, 20);
		panel_31.add(label_57);

		JPanel panel_18 = new JPanel();
		panel_18.setLayout(null);
		panel_18.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_18);

		JCheckBox ckb30 = new JCheckBox("Ngày 30");
		ckb30.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb30.setBounds(80, 4, 97, 20);
		panel_18.add(ckb30);

		JLabel label_30 = new JLabel("Làm tăng ca");
		label_30.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_30.setBounds(205, 4, 100, 20);
		panel_18.add(label_30);

		JComboBox cbb30 = new JComboBox();
		cbb30.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb30.setBounds(315, 4, 100, 20);
		panel_18.add(cbb30);

		JLabel label_31 = new JLabel("Giờ");
		label_31.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_31.setBounds(445, 4, 70, 20);
		panel_18.add(label_31);

		JPanel panel_32 = new JPanel();
		panel_32.setLayout(null);
		panel_32.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_32);

		JCheckBox ckb31 = new JCheckBox("Ngày 31");
		ckb31.setFont(new Font("Tahoma", Font.PLAIN, 14));
		ckb31.setBounds(80, 4, 97, 20);
		panel_32.add(ckb31);

		JLabel label_58 = new JLabel("Làm tăng ca");
		label_58.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_58.setBounds(205, 4, 100, 20);
		panel_32.add(label_58);

		JComboBox cbb31 = new JComboBox();
		cbb31.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5" }));
		cbb31.setBounds(315, 4, 100, 20);
		panel_32.add(cbb31);

		JLabel label_59 = new JLabel("Giờ");
		label_59.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_59.setBounds(445, 4, 70, 20);
		panel_32.add(label_59);

		JPanel footer = new JPanel();
		footer.setBorder(
				new TitledBorder(null, "Ch\u1EE9c n\u0103ng", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		footer.setBounds(10, 575, 1322, 56);
		body.add(footer);
		footer.setLayout(null);

		JButton btnThoat = new JButton("Về trang chủ");
		btnThoat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				fMain f = new fMain();
				f.setVisible(true);
			}
		});
		btnThoat.setBounds(929, 20, 130, 25);
		btnThoat.setFont(new Font("Tahoma", Font.PLAIN, 16));
		footer.add(btnThoat);

		JLabel lblThng_1 = new JLabel("Thưởng");
		lblThng_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblThng_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblThng_1.setBounds(123, 20, 98, 25);
		footer.add(lblThng_1);

		JComboBox cbbThuong = new JComboBox();
		cbbThuong.setBounds(255, 22, 220, 25);
		int c = 0;
		String[] rs = new String[ThuongBUS.getSttListThuongDTO().size()];
		for (ThuongDTO item : ThuongBUS.getSttListThuongDTO()) {
			rs[c] = item.getTenLoaiThuong();
			c++;
		}
		cbbThuong.setModel(new DefaultComboBoxModel(rs));
		footer.add(cbbThuong);
		JButton btnLuu = new JButton("Lưu dữ liệu");
		btnLuu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				String[] ngay = new String[31];
				String n0 = ckb1.isSelected() ? "8-" + cbb1.getSelectedItem() : "0-0";
				ngay[0] = n0;
				String n1 = ckb2.isSelected() ? "8-" + cbb2.getSelectedItem() : "0-0";
				ngay[1] = n1;
				String n2 = ckb3.isSelected() ? "8-" + cbb3.getSelectedItem() : "0-0";
				ngay[2] = n2;
				String n3 = ckb4.isSelected() ? "8-" + cbb4.getSelectedItem() : "0-0";
				ngay[3] = n3;
				String n4 = ckb5.isSelected() ? "8-" + cbb5.getSelectedItem() : "0-0";
				ngay[4] = n4;
				String n5 = ckb6.isSelected() ? "8-" + cbb6.getSelectedItem() : "0-0";
				ngay[5] = n5;
				String n6 = ckb7.isSelected() ? "8-" + cbb7.getSelectedItem() : "0-0";
				ngay[6] = n6;
				String n7 = ckb8.isSelected() ? "8-" + cbb8.getSelectedItem() : "0-0";
				ngay[7] = n7;
				String n8 = ckb9.isSelected() ? "8-" + cbb9.getSelectedItem() : "0-0";
				ngay[8] = n8;
				String n9 = ckb10.isSelected() ? "8-" + cbb10.getSelectedItem() : "0-0";
				ngay[9] = n9;
				String n10 = ckb11.isSelected() ? "8-" + cbb11.getSelectedItem() : "0-0";
				ngay[10] = n10;
				String n11 = ckb12.isSelected() ? "8-" + cbb12.getSelectedItem() : "0-0";
				ngay[11] = n11;
				String n12 = ckb13.isSelected() ? "8-" + cbb13.getSelectedItem() : "0-0";
				ngay[12] = n12;
				String n13 = ckb14.isSelected() ? "8-" + cbb14.getSelectedItem() : "0-0";
				ngay[13] = n13;
				String n14 = ckb15.isSelected() ? "8-" + cbb15.getSelectedItem() : "0-0";
				ngay[14] = n14;
				String n15 = ckb16.isSelected() ? "8-" + cbb16.getSelectedItem() : "0-0";
				ngay[15] = n15;
				String n16 = ckb17.isSelected() ? "8-" + cbb17.getSelectedItem() : "0-0";
				ngay[16] = n16;
				String n17 = ckb18.isSelected() ? "8-" + cbb18.getSelectedItem() : "0-0";
				ngay[17] = n17;
				String n18 = ckb19.isSelected() ? "8-" + cbb19.getSelectedItem() : "0-0";
				ngay[18] = n18;
				String n19 = ckb20.isSelected() ? "8-" + cbb20.getSelectedItem() : "0-0";
				ngay[19] = n19;
				String n20 = ckb21.isSelected() ? "8-" + cbb21.getSelectedItem() : "0-0";
				ngay[20] = n20;
				String n21 = ckb22.isSelected() ? "8-" + cbb22.getSelectedItem() : "0-0";
				ngay[21] = n21;
				String n22 = ckb23.isSelected() ? "8-" + cbb23.getSelectedItem() : "0-0";
				ngay[22] = n22;
				String n23 = ckb24.isSelected() ? "8-" + cbb24.getSelectedItem() : "0-0";
				ngay[23] = n23;
				String n24 = ckb25.isSelected() ? "8-" + cbb25.getSelectedItem() : "0-0";
				ngay[24] = n24;
				String n25 = ckb26.isSelected() ? "8-" + cbb26.getSelectedItem() : "0-0";
				ngay[25] = n25;
				String n26 = ckb27.isSelected() ? "8-" + cbb27.getSelectedItem() : "0-0";
				ngay[26] = n26;
				String n27 = ckb28.isSelected() ? "8-" + cbb28.getSelectedItem() : "0-0";
				ngay[27] = n27;
				String n28 = ckb29.isSelected() ? "8-" + cbb29.getSelectedItem() : "0-0";
				ngay[28] = n28;
				String n29 = ckb30.isSelected() ? "8-" + cbb30.getSelectedItem() : "0-0";
				ngay[29] = n29;
				String n30 = ckb31.isSelected() ? "8-" + cbb31.getSelectedItem() : "0-0";
				ngay[30] = n30;

				ChamCongDTO chamCongDTO = new ChamCongDTO();
				String manv = txfMaNV.getText();
				if (!manv.isEmpty()) {
					String thang = txfThang.getText();
					chamCongDTO.setMaChamCong("T" + thang + "R" + manv);
					chamCongDTO.setNgay(ngay);
					ChamCongBUS chamCongBUS = new ChamCongBUS();
					LuongChiTietBUS luong = new LuongChiTietBUS();
					// tạo luongchitietdto de them vao hàm

					LuongChiTietDTO dto = new LuongChiTietDTO();
					dto.setThang(Integer.parseInt(thang));
					dto.setMaChamCong(chamCongDTO.getMaChamCong());
					dto.setMaNhanVien(manv);
					String thuong = cbbThuong.getSelectedItem().toString();
					for (ThuongDTO item : ThuongBUS.getSttListThuongDTO()) {
						if (item.getTenLoaiThuong().equals(thuong)) {
							dto.setMaThuong(item.getMaThuong());
							break;
						}
					}
					if (chamCongBUS.ChamCongNhanVien(chamCongDTO)) {
						luong.ThemLuongChiTiet(dto);
						JOptionPane.showMessageDialog(null, "Lưu dữ liệu thành công");
					} else {
						JOptionPane.showMessageDialog(null, "Lưu dữ liệu thất bại");
					}
				}else
				{
					JOptionPane.showMessageDialog(null, "Chưa chọn nhân viên");
				}

			}
		});
		btnLuu.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnLuu.setBounds(627, 20, 130, 25);
		footer.add(btnLuu);

	}
}
