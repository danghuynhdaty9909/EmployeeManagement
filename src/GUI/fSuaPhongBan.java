package GUI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.EmptyBorder;

import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import DAO.NhanVienDAO;
import DTO.PhongBanDTO;

import javax.swing.border.BevelBorder;

public class fSuaPhongBan extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtBoss;
	private JTextField txtAddress;
	private JButton btGetBoss;
	private JButton btCancel;
	private JButton btFix;
	private NhanVienBUS nhanvienBus = new NhanVienBUS();
	private PhongBanBUS phongbanBus = new PhongBanBUS();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fSuaPhongBan frame = new fSuaPhongBan();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public fSuaPhongBan() {
		// nimbus look and feel
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		createFrame();
		loadData(fQuanLyPhongBan.getPhongbanHienhanh());
		getBoss();
		clickFix();
		clickCancel();
	}

	public void createFrame() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\sua.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 659, 415);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		contentPane.setLayout(null);

		JLabel lbName = new JLabel("Tên Phòng Ban:");
		lbName.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbName.setBounds(95, 110, 125, 25);
		contentPane.add(lbName);

		JLabel lbBoss = new JLabel("Trưởng Phòng:");
		lbBoss.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbBoss.setBounds(95, 155, 125, 25);
		contentPane.add(lbBoss);

		JLabel lbAddress = new JLabel("Địa Chỉ Phòng:");
		lbAddress.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbAddress.setBounds(95, 200, 125, 25);
		contentPane.add(lbAddress);

		txtName = new JTextField();
		txtName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtName.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtName.setBounds(270, 110, 257, 25);
		contentPane.add(txtName);
		txtName.setColumns(10);

		txtBoss = new JTextField();
		txtBoss.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtBoss.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtBoss.setBounds(270, 155, 189, 25);
		contentPane.add(txtBoss);
		txtBoss.setColumns(10);

		txtAddress = new JTextField();
		txtAddress.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAddress.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtAddress.setBounds(270, 200, 257, 25);
		contentPane.add(txtAddress);
		txtAddress.setColumns(10);

		JLabel lbTitle = new JLabel("Sửa Phòng Ban");
		lbTitle.setForeground(Color.BLUE);
		lbTitle.setBackground(new Color(0, 100, 0));
		lbTitle.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lbTitle.setBounds(10, 29, 600, 33);
		contentPane.add(lbTitle);

		btFix = new JButton("Sửa");
		btFix.setBackground(Color.WHITE);
		btFix.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btFix.setBounds(202, 289, 86, 25);
		contentPane.add(btFix);

		btCancel = new JButton("Hủy");
		btCancel.setBackground(Color.WHITE);
		btCancel.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btCancel.setBounds(424, 289, 77, 25);
		contentPane.add(btCancel);

		btGetBoss = new JButton("New button");
		btGetBoss.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btGetBoss.setBackground(Color.WHITE);
		btGetBoss.setBounds(478, 155, 49, 25);
		contentPane.add(btGetBoss);
	}

	public void loadData(PhongBanDTO phongban) {
		txtName.setText(phongban.getTenPhong());
		txtAddress.setText(phongban.getDiaChi());
		txtBoss.setText(nhanvienBus.getNhanVienByID(phongban.getTruongPhong()).getHo() + " "
				+ nhanvienBus.getNhanVienByID(phongban.getTruongPhong()).getTen());
	}

	public void getBoss() {
		btGetBoss.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				fQuanLyPhongBan.getPhongbanHienhanh().setDiaChi(txtAddress.getText());
				fQuanLyPhongBan.getPhongbanHienhanh().setTenPhong(txtName.getText());
				new fDSNhanVienTheoPhong().setVisible(true);
			}
		});
	}

	public void clickFix() {
		btFix.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int respone = JOptionPane.showConfirmDialog(null, "Bạn có chắc muốn lưu thông tin này?", "Confirm",
						JOptionPane.YES_NO_OPTION);
				if (respone == JOptionPane.YES_OPTION) {
					fQuanLyPhongBan.getPhongbanHienhanh().setDiaChi(txtAddress.getText());
					fQuanLyPhongBan.getPhongbanHienhanh().setTenPhong(txtName.getText());
					fQuanLyPhongBan.getPhongbanHienhanh().setTruongPhong(fDSNhanVienTheoPhong.getNewBoss().getMaNV());
					// kiem tra xem phong update co phai la phng giam doc khong
					boolean flag = false;
					if (fQuanLyPhongBan.getPhongbanHienhanh().getMaPhongBan().equals("P0")) {
						flag = nhanvienBus.updateOneCol("macv", "CEO",
								fQuanLyPhongBan.getPhongbanHienhanh().getTruongPhong());
					} else {
						flag = nhanvienBus.updateOneCol("macv", "TP",
								fQuanLyPhongBan.getPhongbanHienhanh().getTruongPhong());
					}
					if (flag) {
						boolean flag_1 = nhanvienBus.updateOneCol("macv", fDSNhanVienTheoPhong.getOldBoss().getMacv(),
								fDSNhanVienTheoPhong.getOldBoss().getMaNV());
						if (flag_1) {
							if (phongbanBus.update(fQuanLyPhongBan.getPhongbanHienhanh())) {
								setVisible(false);
								JOptionPane.showMessageDialog(null, "Sửa thành công!");
								new fQuanLyPhongBan().setVisible(true);
							}
						} else {
							JOptionPane.showMessageDialog(null, "Sửa thất bại!");
						}
					} else {
						JOptionPane.showMessageDialog(null, "Sửa thất bại!");
					}
				}
			}
		});
	}

	public void clickCancel() {
		btCancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				new fQuanLyPhongBan().setVisible(true);

			}
		});
	}

	public JTextField getTxtName() {
		return txtName;
	}

	public void setTxtName(JTextField txtName) {
		this.txtName = txtName;
	}

	public JTextField getTxtBoss() {
		return txtBoss;
	}

	public void setTxtBoss(JTextField txtBoss) {
		this.txtBoss = txtBoss;
	}

	public JTextField getTxtAddress() {
		return txtAddress;
	}

	public void setTxtAddress(JTextField txtAddress) {
		this.txtAddress = txtAddress;
	}

	public JButton getBtGetBoss() {
		return btGetBoss;
	}

	public void setBtGetBoss(JButton btGetBoss) {
		this.btGetBoss = btGetBoss;
	}

	public JButton getBtCancel() {
		return btCancel;
	}

	public void setBtCancel(JButton btCancel) {
		this.btCancel = btCancel;
	}

	public JButton getBtFix() {
		return btFix;
	}

	public void setBtFix(JButton btFix) {
		this.btFix = btFix;
	}

}
