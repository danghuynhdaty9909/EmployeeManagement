package GUI;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import BUS.LuongBUS;
import BUS.LuongChiTietBUS;
import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import BUS.PhuCapBUS;
import BUS.ThuongBUS;
import DTO.NhanVienDTO;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import javax.swing.JRadioButtonMenuItem;

@SuppressWarnings("serial")
public class fMain extends JFrame {

	@SuppressWarnings("unused")
	private JPanel contentPane;
	@SuppressWarnings("unused")
	private JFrame frameChinh;
	private JLabel lbQuanLiNhanSu;
	private JLabel lbIQuanLiPhongBan;
	private JLabel lbQLNS;
	private JLabel lbQLPB;
	private JLabel lblThongKe;
	private JLabel lbQLLuong;
	private JLabel lbIQuanLiLuong;
	private JLabel lbThongKe;
	private JLabel lbIDanhSachNhanVien;
	private JLabel lbDSNS;
	private JLabel lblCngC;
	private JLabel lblCongCu;

	// các BUS loaddata khi chạy ứng dụng
	private PhongBanBUS phongBanBUS = new PhongBanBUS();
	private NhanVienBUS nhanVienBUS = new NhanVienBUS();
	private LuongBUS luongBUS = new LuongBUS();
	private LuongChiTietBUS luongChiTietBUS = new LuongChiTietBUS();

	private ThuongBUS thuongBUS = new ThuongBUS();
	private PhuCapBUS phuCapBUS = new PhuCapBUS();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fMain frame = new fMain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public fMain() {

		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, you can set the GUI to another look
			// and feel.
		}
		setExtendedState(JFrame.MAXIMIZED_BOTH);// set full màn hình

		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\officer.png"));
		setTitle("Quản Lý Nhân Sự");
		setBounds(100, 100, 1366, 768);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		JLabel lbChinh = new JLabel("Chương Trình Quản Lý Nhân Sự");
		lbChinh.setBounds(0, 2, 1340, 121);
		lbChinh.setHorizontalAlignment(SwingConstants.CENTER);
		lbChinh.setForeground(Color.BLACK);
		lbChinh.setFont(new Font("Times New Roman", Font.BOLD, 28));
		getContentPane().add(lbChinh);

		lbQuanLiNhanSu = new JLabel("");
		lbQuanLiNhanSu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				dispose();
				fQuanLyNhanSu f = new fQuanLyNhanSu();
				f.setVisible(true);
			}
		});
		lbQuanLiNhanSu.setBounds(215, 134, 190, 150);
		lbQuanLiNhanSu.setHorizontalAlignment(SwingConstants.CENTER);
		lbQuanLiNhanSu.setIcon(new ImageIcon("img\\QLNS.png"));
		getContentPane().add(lbQuanLiNhanSu);

		lbIQuanLiPhongBan = new JLabel("");
		lbIQuanLiPhongBan.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new fQuanLyPhongBan().setVisible(true);
			}
		});
		lbIQuanLiPhongBan.setBounds(581, 134, 190, 150);
		lbIQuanLiPhongBan.setHorizontalAlignment(SwingConstants.CENTER);
		lbIQuanLiPhongBan.setIcon(new ImageIcon("img\\QTHT.png"));
		getContentPane().add(lbIQuanLiPhongBan);

		lbQLNS = new JLabel("Quản Lý Nhân Sự");
		lbQLNS.setBounds(215, 290, 190, 25);
		lbQLNS.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbQLNS.setHorizontalAlignment(SwingConstants.CENTER);
		lbQLNS.setVerticalAlignment(SwingConstants.TOP);
		getContentPane().add(lbQLNS);

		lbQLPB = new JLabel("Quản Lý Phòng Ban");
		lbQLPB.setBounds(581, 290, 190, 25);
		lbQLPB.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbQLPB.setHorizontalAlignment(SwingConstants.CENTER);
		lbQLPB.setVerticalAlignment(SwingConstants.TOP);
		getContentPane().add(lbQLPB);

		lbIQuanLiLuong = new JLabel("");
		lbIQuanLiLuong.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				fQuanLyLuong f = new fQuanLyLuong();
				f.setVisible(true);
			}
		});
		lbIQuanLiLuong.setBounds(215, 414, 190, 150);
		lbIQuanLiLuong.setHorizontalAlignment(SwingConstants.CENTER);
		lbIQuanLiLuong.setIcon(new ImageIcon("img\\BCTK.png"));
		getContentPane().add(lbIQuanLiLuong);

		lblThongKe = new JLabel("");
		lblThongKe.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new fThongKeNhanVien().setVisible(true);
			}
		});
		lblThongKe.setBounds(933, 409, 190, 150);
		lblThongKe.setHorizontalAlignment(SwingConstants.CENTER);
		lblThongKe.setIcon(new ImageIcon("img\\iconThongKe.png"));
		getContentPane().add(lblThongKe);

		lbQLLuong = new JLabel("Quản Lý Lương");
		lbQLLuong.setBounds(215, 571, 190, 25);
		lbQLLuong.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbQLLuong.setVerticalAlignment(SwingConstants.TOP);
		lbQLLuong.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(lbQLLuong);

		lbThongKe = new JLabel("Thống Kê");
		lbThongKe.setForeground(Color.BLACK);
		lbThongKe.setBounds(933, 570, 190, 25);
		lbThongKe.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbThongKe.setVerticalAlignment(SwingConstants.TOP);
		lbThongKe.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(lbThongKe);

		lbIDanhSachNhanVien = new JLabel("");
		lbIDanhSachNhanVien.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				fThemChamCong f = new fThemChamCong();
				dispose();
				f.setVisible(true);

			}
		});
		lbIDanhSachNhanVien.setHorizontalAlignment(SwingConstants.CENTER);
		lbIDanhSachNhanVien.setIcon(new ImageIcon("img\\danhsach.png"));
		lbIDanhSachNhanVien.setBounds(581, 414, 190, 150);
		getContentPane().add(lbIDanhSachNhanVien);

		lbDSNS = new JLabel("Chấm Công");
		lbDSNS.setVerticalAlignment(SwingConstants.TOP);
		lbDSNS.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbDSNS.setHorizontalAlignment(SwingConstants.CENTER);
		lbDSNS.setBounds(581, 570, 190, 19);
		getContentPane().add(lbDSNS);

		lblCngC = new JLabel("Công Cụ");
		lblCngC.setVerticalAlignment(SwingConstants.TOP);
		lblCngC.setHorizontalAlignment(SwingConstants.CENTER);
		lblCngC.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblCngC.setBounds(933, 290, 190, 25);
		getContentPane().add(lblCngC);

		lblCongCu = new JLabel("");
		lblCongCu.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				dispose();
				fExport f = new fExport();
				f.setVisible(true);
			}
		});
		lblCongCu.setIcon(new ImageIcon("img\\congcu.png"));
		lblCongCu.setHorizontalAlignment(SwingConstants.CENTER);
		lblCongCu.setBounds(933, 118, 190, 166);
		getContentPane().add(lblCongCu);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem itemQLNS = new JMenuItem("Quản lý nhân sự");
		itemQLNS.setFont(new Font("Tahoma", Font.PLAIN, 14));
		itemQLNS.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		mnFile.add(itemQLNS);
		itemQLNS.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				new fQuanLyNhanSu().setVisible(true);
			}
		});
		itemQLNS.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==(KeyEvent.VK_CONTROL+KeyEvent.VK_N)){
					itemQLNS.doClick();
				}
				
			}
		});
		
		JMenuItem itemQLPB = new JMenuItem("Quản lý phòng ban");
		itemQLPB.setFont(new Font("Tahoma", Font.PLAIN, 14));
		itemQLPB.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_MASK));
		mnFile.add(itemQLPB);
		itemQLPB.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				new fQuanLyPhongBan().setVisible(true);
			}
		});
		itemQLPB.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==(KeyEvent.VK_CONTROL+KeyEvent.VK_P)){
					itemQLPB.doClick();
				}
				
			}
		});
		
		JMenuItem itemQLL = new JMenuItem("Quản lý lương");
		itemQLL.setFont(new Font("Tahoma", Font.PLAIN, 14));
		itemQLL.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK));
		mnFile.add(itemQLL);
		itemQLL.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				new fQuanLyLuong().setVisible(true);
			}
		});
		itemQLL.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==(KeyEvent.VK_CONTROL+KeyEvent.VK_L)){
					itemQLL.doClick();
				}
				
			}
		});
		
		JRadioButtonMenuItem itemTK = new JRadioButtonMenuItem("Thống kê");
		itemTK.setFont(new Font("Tahoma", Font.PLAIN, 14));
		itemTK.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_MASK));
		mnFile.add(itemTK);
		itemTK.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				new fThongKeNhanVien().setVisible(true);
			}
		});
		itemTK.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==(KeyEvent.VK_CONTROL+KeyEvent.VK_T)){
					itemTK.doClick();
				}
				
			}
		});
		JMenuItem itemClose = new JMenuItem("Đóng");
		itemClose.setFont(new Font("Tahoma", Font.PLAIN, 14));
		itemClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK));
		mnFile.add(itemClose);
		itemClose.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int respone=JOptionPane.showConfirmDialog(null, "Bạn có chắc muốn thoát chương trình?","Confirm",JOptionPane.YES_NO_OPTION);
				if(respone==JOptionPane.YES_OPTION){
					System.exit(0);
				}
			}
		});
		itemClose.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==(KeyEvent.VK_ALT+KeyEvent.VK_F4)){
					itemClose.doClick();
				}
				
			}
		});

		lbDSNS.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {

			}
		});

		// load dữ liệu cho danh sách tĩnh phòng ban
		PhongBanBUS.setSttArrayPhongBanDTO(phongBanBUS.getAllPhongBan());

		// load dữ liệu cho danh sách tĩnh nhân viên
		NhanVienBUS.setSttListNhanVienDTO(nhanVienBUS.getAllNhanVienDTO());

		// load du lieu cho danh sách tĩnh luong
		LuongBUS.setSttListLuongDTO(luongBUS.getAllLuong());

		// load du lieu cho danh sach thuong
		ThuongBUS.setSttListThuongDTO(thuongBUS.getAllThuong());

		// load du lieu cho danh sach tinh phu cap
		PhuCapBUS.setSttListPhuCap(phuCapBUS.getAllPhuCap());

	}
}
