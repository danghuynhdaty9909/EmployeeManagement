package GUI;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import javax.swing.JOptionPane;

import java.awt.EventQueue;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;

import java.util.ArrayList;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import BUS.NhanVienBUS;
import DTO.NhanVienDTO;

public class fDanhSachNhanVien extends JFrame {

	private JPanel contentPane;
	private DefaultTableModel modelNhanVien;

	private JTable table;
	private JTable tblDSNV;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fDanhSachNhanVien frame = new fDanhSachNhanVien();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public fDanhSachNhanVien() {
		setTitle("Danh Sách Nhân Viên");
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, you can set the GUI to another look
			// and feel.
		}
		setExtendedState(JFrame.MAXIMIZED_BOTH);// set full màn hình
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblHeader = new JLabel("Danh Sách Nhân Viên");
		lblHeader.setHorizontalAlignment(SwingConstants.CENTER);
		lblHeader.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblHeader.setBounds(10, 11, 1342, 30);
		contentPane.add(lblHeader);

		JPanel body = new JPanel();
		body.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		body.setBounds(20, 52, 1342, 569);
		contentPane.add(body);
		body.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 1322, 547);
		body.add(scrollPane);


		tblDSNV = new JTable();
		tblDSNV.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "Mã Nhân Viên", "Họ", "Tên",
				"Giới tính", "Ngày Sinh", "Địa chỉ", "Số điện thoại", "Email", "Phòng Ban" }));
		scrollPane.setViewportView(tblDSNV);

		JPanel footer = new JPanel();
		footer.setBounds(10, 628, 1342, 66);
		contentPane.add(footer);
		footer.setLayout(null);

		JButton btnThoat = new JButton("Hủy");
		btnThoat.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnThoat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();

			}
		});
		btnThoat.setBounds(470, 21, 89, 29);
		footer.add(btnThoat);

		JButton btnChonNhanVien = new JButton("Chọn Nhân Viên");
		btnChonNhanVien.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// code chọn nhân viên
				int row = tblDSNV.getSelectedRow();
				if (row > -1) {
					String maNV = tblDSNV.getValueAt(row, 0).toString();

					NhanVienBUS nvb = new NhanVienBUS();
					int dialogButton = JOptionPane.YES_NO_OPTION;
					int dialogResult = JOptionPane.showConfirmDialog(null, "Bạn muốn chọn nhân viên "
							+ tblDSNV.getValueAt(row, 1) + " " + tblDSNV.getValueAt(row, 2) + "?", "Thông báo",
							dialogButton);
					if (dialogResult == JOptionPane.YES_OPTION) {
						NhanVienBUS nhanVienBUS = new NhanVienBUS();
						NhanVienDTO item = nhanVienBUS.getNhanVienByID(maNV);
						NhanVienBUS.setSttNhanVienDTO(item);
						dispose();
					}
				} else {
					JOptionPane.showMessageDialog(null, "Chưa chọn nhân viên nào!");
				}
			}
		});
		btnChonNhanVien.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnChonNhanVien.setBounds(750, 21, 151, 29);
		footer.add(btnChonNhanVien);

		// load du lieu cho model
		SetModelDSNV();

	}

	public void SetModelDSNV() {
		modelNhanVien = new DefaultTableModel(new Object[][] {}, new String[] { "Mã Nhân Viên", "Họ", "Tên",
				"Giới tính", "Ngày Sinh", "Địa chỉ", "Số điện thoại", "Email", "Phòng Ban" });
		Vector<String> row = null;
		for (NhanVienDTO item : NhanVienBUS.getSttListNhanVienDTO()) {
			row = new Vector<String>();
			row.add(item.getMaNV());
			row.add(item.getHo());
			row.add(item.getTen());
			row.add(item.getGioiTinh());
			row.add(item.getNgaySinh().toString());
			row.add(item.getDiaChi());
			row.add(item.getSoDT());
			row.add(item.getEmail());
			row.add(item.getMaPhongBan());
			modelNhanVien.addRow(row);
		}
		tblDSNV.setModel(modelNhanVien);
	}
}
