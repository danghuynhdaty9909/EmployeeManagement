package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.EmptyBorder;

//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.xssf.usermodel.XSSFSheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import BUS.PhongBanBUS;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class fExport extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fExport frame = new fExport();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public fExport() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, you can set the GUI to another look
			// and feel.
		}
		setTitle("Xuất dữ liệu");
		
		setBounds(100, 100, 600, 500);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Xuất dữ liệu");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblNewLabel.setBounds(10, 11, 564, 40);
		contentPane.add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 62, 564, 388);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel tfx = new JLabel("Bảng cần xuất");
		tfx.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tfx.setBounds(151, 116, 113, 20);
		panel.add(tfx);
		
		JComboBox cbbBang = new JComboBox();
		cbbBang.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cbbBang.setModel(new DefaultComboBoxModel(new String[] {"Chấm Công", "Chức Vụ", "Hợp Đồng Lao Động", "Lương", "Lương Chi Tiết", "Nhân Viên", "Phòng Ban", "Phụ Cấp", "Thưởng", "Trình Độ học vấn"}));
		cbbBang.setBounds(299, 116, 168, 22);
		panel.add(cbbBang);
		
		JButton btnXuat = new JButton("Xuất dữ liệu");
		
				
				
			

		btnXuat.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnXuat.setBounds(231, 228, 128, 25);
		panel.add(btnXuat);
	}
	
//	public void export(ArrayList a)
//	{
//		XSSFWorkbook workbook = new XSSFWorkbook();
//		XSSFSheet sheet = workbook.createSheet("Data");
//		Object[][] bookData = {
//				{"Head First Java", "Kathy Serria", 79},
//				{"Effective Java", "Joshua Bloch", 36},
//				{"Clean Code", "Robert martin", 42},
//				{"Thinking in Java", "Bruce Eckel", 35},
//		};
//
//		int rowCount = 1;
//		
//		for (Object[] aBook : bookData) {
//			Row row = sheet.createRow(++rowCount);
//			
//			int columnCount = 0;
//			
//			for (Object field : aBook) {
//				Cell cell = row.createCell(++columnCount);
//				if (field instanceof String) {
//					cell.setCellValue((String) field);
//				} else if (field instanceof Integer) {
//					cell.setCellValue((Integer) field);
//				}
//			}
//			
//		}
//		
//		
//		try (FileOutputStream outputStream = new FileOutputStream("JavaBooks.xlsx")) {
//			workbook.write(outputStream);
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
	
}
