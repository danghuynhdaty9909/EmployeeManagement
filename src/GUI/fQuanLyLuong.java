package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Rectangle;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import BUS.LuongChiTietBUS;
import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import DTO.LuongChiTietDTO;
import DTO.NhanVienDTO;
import DTO.PhongBanDTO;

import javax.swing.DefaultComboBoxModel;
import java.awt.Component;
import java.awt.Cursor;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

public class fQuanLyLuong extends JFrame {

	private PhongBanBUS phongBanBUS = new PhongBanBUS();
	private NhanVienBUS nhanVienBUS = new NhanVienBUS();
	private ArrayList<PhongBanDTO> listPhongBanDTO = null;
	private LuongChiTietBUS luongChiTietBUS = new LuongChiTietBUS();
	private JPanel contentPane;
	private JTable tblDanhSachPhongBan;
	private DefaultTableModel modelPhongBan;
	private DefaultTableModel modelNhanVien;
	private JTable tblDanhSachNhanVien;
	private JTextField txfMaNV;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fQuanLyLuong frame = new fQuanLyLuong();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public fQuanLyLuong() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\money.png"));
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent arg0) {
				txfMaNV.setText(NhanVienBUS.getSttNhanVienDTO().getMaNV());
				
			}
		});
		setTitle("Quản Lý Lương");
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, you can set the GUI to another look
			// and feel.
		}
		setExtendedState(JFrame.MAXIMIZED_BOTH);// set full màn hình
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		contentPane = new JPanel();
		contentPane.setSize(new Dimension(1000, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLocationRelativeTo(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel left = new JPanel();
		left.setBounds(20, 76, 400, 618);
		contentPane.add(left);
		left.setLayout(null);

		JPanel pnDanhSach = new JPanel();
		pnDanhSach.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Danh sách phòng ban",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnDanhSach.setBounds(10, 135, 377, 483);
		left.add(pnDanhSach);
		pnDanhSach.setLayout(null);

		JScrollPane scrDanhSachPhongBan = new JScrollPane();
		scrDanhSachPhongBan.setBounds(10, 24, 357, 348);
		pnDanhSach.add(scrDanhSachPhongBan);

		tblDanhSachPhongBan = new JTable();
		tblDanhSachPhongBan.setModel(modelPhongBan = new DefaultTableModel(new Object[][] {},
				new String[] { "Mã phòng ban", "Tên phòng ban", "Trưởng phòng" }));
		scrDanhSachPhongBan.setViewportView(tblDanhSachPhongBan);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "L\u1ECDc d\u1EEF li\u1EC7u", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(10, 383, 357, 89);
		pnDanhSach.add(panel);
		
		JLabel label = new JLabel("");
		label.setBounds(210, 21, 0, 0);
		panel.add(label);
		
		JComboBox cbbThang1 = new JComboBox();
		cbbThang1.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"}));
		cbbThang1.setBounds(150, 21, 160, 20);
		panel.add(cbbThang1);
		
		JLabel label_2 = new JLabel("Lọc theo tháng:");
		label_2.setHorizontalAlignment(SwingConstants.RIGHT);
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_2.setBounds(23, 21, 103, 20);
		panel.add(label_2);
		
		JButton btnXuatDuLieu = new JButton("Xuất dữ liệu");
		btnXuatDuLieu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int row = tblDanhSachPhongBan.getSelectedRow();
				if(row>-1)
				{
					String phong = tblDanhSachPhongBan.getValueAt(row, 0).toString();
					int thang = Integer.valueOf(cbbThang1.getSelectedItem().toString());
					LuongChiTietBUS luongChiTietBUS = new LuongChiTietBUS();
					String manv = txfMaNV.getText();

					if (luongChiTietBUS.setModelTableNhanVien(phong,thang) != null) {
						JOptionPane.showMessageDialog(null, "Vui lòng đợi chương trình tải dữ liệu!");
						modelNhanVien = new DefaultTableModel(new Object[][] {}, new String[] { "Mã nhân viên", "Tên nhân viên",
								"Lương cơ bản", "Thưởng", "Phụ cấp", "Giờ làm bình thường", "Giờ làm tăng ca", "Thực lãnh" });
						modelNhanVien = luongChiTietBUS.setModelTableNhanVien(phong,thang);

						tblDanhSachNhanVien.setModel(modelNhanVien);
					} else {
						JOptionPane.showMessageDialog(null, "Không có dữ liệu!");
					}
				}else
				{
					JOptionPane.showMessageDialog(null, "Chưa chọn Phòng cần xuất dữ liệu!");
				}
				

			
			
			}
		});
		btnXuatDuLieu.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnXuatDuLieu.setBounds(182, 52, 128, 23);
		panel.add(btnXuatDuLieu);
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "T\u00ECm ki\u1EBFm nhanh", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBounds(10, 0, 377, 124);
		left.add(panel_1);
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(210, 21, 0, 0);
		panel_1.add(label_1);
		
		JComboBox cbbThang = new JComboBox();
		cbbThang.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"}));
		cbbThang.setBounds(150, 54, 160, 20);
		panel_1.add(cbbThang);
		
		JLabel label_3 = new JLabel("Lọc theo tháng:");
		label_3.setHorizontalAlignment(SwingConstants.RIGHT);
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_3.setBounds(35, 52, 103, 20);
		panel_1.add(label_3);
		
		txfMaNV = new JTextField();
		txfMaNV.setColumns(10);
		txfMaNV.setBounds(150, 21, 160, 20);
		panel_1.add(txfMaNV);
		
		JLabel label_4 = new JLabel("Mã nhân viên:");
		label_4.setHorizontalAlignment(SwingConstants.RIGHT);
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 15));
		label_4.setBounds(25, 21, 103, 20);
		panel_1.add(label_4);
		
		JButton btnDSNV = new JButton("...");
		btnDSNV.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fDanhSachNhanVien f = new fDanhSachNhanVien();
				f.setVisible(true);
			}
		});
		btnDSNV.setBounds(317, 21, 29, 23);
		panel_1.add(btnDSNV);
		
		JButton btnTimKiem = new JButton("Tìm kiếm");
		btnTimKiem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int thang = Integer.valueOf(cbbThang.getSelectedItem().toString());
				LuongChiTietBUS luongChiTietBUS = new LuongChiTietBUS();
				String manv = txfMaNV.getText();
				if (luongChiTietBUS.CheckNhanVien(manv, String.valueOf(thang))) {
					JOptionPane.showMessageDialog(null, "Vui lòng đợi chương trình tải dữ liệu!");
					
					modelNhanVien = luongChiTietBUS.TimKiemNhanVien(thang, manv);
					tblDanhSachNhanVien.setModel(modelNhanVien);
				} else {
					JOptionPane.showMessageDialog(null, "Không có dữ liệu!");
				}

			}
		});
		btnTimKiem.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnTimKiem.setBounds(185, 82, 125, 31);
		panel_1.add(btnTimKiem);

		JPanel header = new JPanel();
		header.setBounds(20, 11, 1332, 54);
		contentPane.add(header);
		header.setLayout(null);

		JLabel lblNewLabel = new JLabel("QUẢN LÝ LƯƠNG NHÂN VIÊN");
		lblNewLabel.setForeground(Color.BLACK);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel.setBounds(10, 0, 1312, 54);
		header.add(lblNewLabel);

		JPanel right = new JPanel();
		right.setBorder(new TitledBorder(null, "B\u1EA3ng l\u01B0\u01A1ng chi ti\u1EBFt", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		right.setBounds(430, 76, 903, 560);
		right.setLayout(null);
		contentPane.add(right);

		JScrollPane scrDanhSachNhanVien = new JScrollPane();
		scrDanhSachNhanVien.setSize(883, 525);
		scrDanhSachNhanVien.setLocation(10, 24);

		right.add(scrDanhSachNhanVien);

		tblDanhSachNhanVien = new JTable();
		tblDanhSachNhanVien.setModel(
				modelNhanVien = new DefaultTableModel(new Object[][] {}, new String[] { "Mã nhân viên", "Tên nhân viên",
						"Lương cơ bản", "Thưởng", "Phụ cấp", "Giờ làm bình thường", "Giờ làm tăng ca", "Thực lãnh" }));
		scrDanhSachNhanVien.setViewportView(tblDanhSachNhanVien);

		JPanel tool = new JPanel();
		tool.setBorder(
				new TitledBorder(null, "Ch\u1EE9c n\u0103ng", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		tool.setBounds(430, 640, 903, 55);
		contentPane.add(tool);
		tool.setLayout(null);

		JButton btnBack = new JButton("Về Trang Chủ");
		btnBack.setIcon(new ImageIcon("img\\home.png"));
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				fMain f = new fMain();
				f.setVisible(true);
			}
		});
		btnBack.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnBack.setBounds(735, 15, 157, 25);
		tool.add(btnBack);

		// code cho phần load data lên table phòng ban

		// khởi tạo danh sách phongban
		listPhongBanDTO = phongBanBUS.getAllPhongBan();
		// set data cho bảng phòng ban

		setModelPhongBan(PhongBanBUS.getSttArrayPhongBanDTO());

	}

	// load dữ liệu cho bảng phòng ban
	public void setModelPhongBan(ArrayList<PhongBanDTO> listPhongBan) {

		Vector<String> row;
		if (listPhongBan != null) {
			for (PhongBanDTO item : listPhongBan) {
				row = new Vector<String>();
				row.add(item.getMaPhongBan());
				row.add(item.getTenPhong());
				String truongPhong = item.getTruongPhong();
				NhanVienDTO nv = nhanVienBUS.getNhanVienByID(truongPhong);
				row.add(nv.getHo() + " " + nv.getTen());
				modelPhongBan.addRow(row);
			}
			tblDanhSachPhongBan.setModel(modelPhongBan);
		}
	}
}


