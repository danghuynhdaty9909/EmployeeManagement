package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import BUS.ChucVuBUS;
import BUS.HopDongLaoDongBUS;
import BUS.LuongChiTietBUS;
import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import BUS.TrinhDoHocVanBUS;
import DTO.ChucVuDTO;
import DTO.HopDongLaoDongDTO;
import DTO.LuongChiTietDTO;
import DTO.NhanVienDTO;
import DTO.PhongBanDTO;
import DTO.TrinhDoHocVanDTO;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

import java.awt.Component;
import java.awt.Cursor;
import javax.swing.JButton;
import javax.swing.border.LineBorder;

public class fQuanLyNhanSu extends JFrame {

	private JPanel contentPane;
	private JTextField txtTuKhoa;
	private JTable table;
	private DefaultTableModel model;
	private JTextField txtMa;
	private JTextField txtHoten;
	private JTextField txtNgaySinh;
	private JTextField txtDiaChi;
	private JTextField txtDanToc;
	private JTextField txtTonGiao;
	private JTextField txtSoDT;
	private JTextField txtSoTaiKhoan;
	private JTextField txtCmnd;
	private JTextField txtMaPhong;
	private JTextField txtTenPhong;
	private JTextField txtMaChucVu;
	private JTextField txtTenChucVu;
	private JTextField txtMaTrinhDo;
	private JTextField txtTrinhDo;
	private JTextField txtChuyenNganh;
	private JTextField txtMaHD;
	private JTextField txtLoaiHD;
	private JTextField txtNgayBD;
	private JTextField txtNgayKy;
	private JTextField txtNgayKT;
	private JTextField txtMaLuong;
	private JTextField txtLuongThuc;
	private JLabel lbHinhAnh;
	private JButton btSuaAnh;
	private JButton btXoaAnh;
	private JButton btSuaNhanVien;
	private JButton btTroVe;
	private JButton btXoaNhanVien;
	private JButton btnThemNhanVien;
	private JButton btTim;
	private JComboBox cbTimKiem;
	private JComboBox cbGioiTinh;
	private static ArrayList<NhanVienDTO> list;
	private ChucVuBUS chucvuBus = new ChucVuBUS();
	private PhongBanBUS phongbanBus = new PhongBanBUS();
	private TrinhDoHocVanBUS trinhdoBus = new TrinhDoHocVanBUS();
	private HopDongLaoDongBUS hopdongBus = new HopDongLaoDongBUS();
	private NhanVienBUS nhanvienBus = new NhanVienBUS();
	private LuongChiTietBUS luongchitietBus = new LuongChiTietBUS();
	private static NhanVienDTO nhanvienHienHanh = new NhanVienDTO();
	private static PhongBanDTO phongbanDto = new PhongBanDTO();
	private static ChucVuDTO chucvuDto = new ChucVuDTO();
	private static TrinhDoHocVanDTO trinhdoDto = new TrinhDoHocVanDTO();
	private static HopDongLaoDongDTO hopdongDto = new HopDongLaoDongDTO();
	private static LuongChiTietDTO luongchitietDto = new LuongChiTietDTO();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fQuanLyNhanSu frame = new fQuanLyNhanSu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public fQuanLyNhanSu() {

		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, you can set the GUI to another look
			// and feel.
		}
		setExtendedState(JFrame.MAXIMIZED_BOTH);// set full màn hình
		setTitle("QUẢN LÝ NHÂN SỰ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		contentPane = new JPanel();
		contentPane.setForeground(Color.BLACK);
		contentPane.setLocation(-1040, -318);
		contentPane.setSize(new Dimension(1000, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLocationRelativeTo(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(40, 95, 450, 585);
		contentPane.add(panel);
		panel.setLayout(null);

		JPanel pnTimKiem = new JPanel();
		pnTimKiem.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "T\u00ECm ki\u1EBFm nhanh",
				TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLACK));
		pnTimKiem.setBounds(10, 0, 420, 137);
		panel.add(pnTimKiem);
		pnTimKiem.setLayout(null);

		JLabel label = new JLabel("");
		label.setBounds(210, 21, 0, 0);
		pnTimKiem.add(label);

		JLabel lbTimKiem = new JLabel("Tìm kiếm theo:");
		lbTimKiem.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbTimKiem.setBounds(50, 30, 110, 20);
		pnTimKiem.add(lbTimKiem);

		cbTimKiem = new JComboBox();
		cbTimKiem.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cbTimKiem.setModel(new DefaultComboBoxModel(
				new String[] { "Mã nhân viên", "Mã phòng ban", "Tên", "Mã chức vụ", "Email", "Toàn danh sách" }));
		cbTimKiem.setSelectedIndex(5);
		cbTimKiem.setBackground(Color.WHITE);
		cbTimKiem.setBounds(160, 30, 200, 20);
		pnTimKiem.add(cbTimKiem);

		JLabel lbTuKhoa = new JLabel("Từ khóa:");
		lbTuKhoa.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lbTuKhoa.setBounds(50, 60, 80, 20);
		pnTimKiem.add(lbTuKhoa);

		txtTuKhoa = new JTextField();
		txtTuKhoa.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtTuKhoa.setBorder(new BevelBorder(BevelBorder.RAISED, new Color(192, 192, 192), null, null, null));
		txtTuKhoa.setBounds(160, 60, 200, 20);
		pnTimKiem.add(txtTuKhoa);
		txtTuKhoa.setColumns(10);

		btTim = new JButton("Tìm");
		btTim.setBackground(Color.LIGHT_GRAY);
		btTim.setIcon(new ImageIcon("img\\tim.png"));
		btTim.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btTim.setBounds(255, 91, 105, 35);
		pnTimKiem.add(btTim);

		JPanel pnDanhSach = new JPanel();
		pnDanhSach.setBorder(
				new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Danh s\u00E1ch nh\u00E2n vi\u00EAn",
						TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnDanhSach.setBounds(10, 148, 420, 437);
		panel.add(pnDanhSach);
		pnDanhSach.setLayout(null);

		JScrollPane scrDanhSach = new JScrollPane();
		scrDanhSach.setBounds(10, 30, 400, 396);
		pnDanhSach.add(scrDanhSach);

		table = new JTable();
		table.setModel(model = new DefaultTableModel(new Object[][] {}, new String[] { "M\u00E3 nh\u00E2n vi\u00EAn",
				"H\u1ECD t\u00EAn", "Ng\u00E0y sinh", "Gi\u1EDBi t\u00EDnh" }));
		table.getColumnModel().getColumn(1).setPreferredWidth(140);
		table.getColumnModel().getColumn(2).setPreferredWidth(60);
		table.getColumnModel().getColumn(3).setPreferredWidth(49);
		scrDanhSach.setViewportView(table);

		JPanel pnThongTin = new JPanel();
		pnThongTin.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"Th\u00F4ng tin nh\u00E2n vi\u00EAn", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLACK));
		pnThongTin.setBounds(500, 95, 485, 210);
		contentPane.add(pnThongTin);
		pnThongTin.setLayout(null);

		JLabel lbMa = new JLabel("Mã NV:");
		lbMa.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbMa.setBounds(20, 30, 61, 20);
		pnThongTin.add(lbMa);

		txtMa = new JTextField();
		txtMa.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtMa.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtMa.setBounds(95, 30, 95, 20);
		pnThongTin.add(txtMa);
		txtMa.setColumns(10);

		JLabel lbHoten = new JLabel("Họ và tên:");
		lbHoten.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbHoten.setBounds(205, 30, 80, 20);
		pnThongTin.add(lbHoten);

		txtHoten = new JTextField();
		txtHoten.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtHoten.setFont(new Font("Tahoma", Font.PLAIN, 16));
		txtHoten.setBounds(283, 30, 177, 20);
		pnThongTin.add(txtHoten);
		txtHoten.setColumns(10);

		JLabel lbGoiTinh = new JLabel("Giới tính:");
		lbGoiTinh.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbGoiTinh.setBounds(20, 60, 67, 20);
		pnThongTin.add(lbGoiTinh);

		cbGioiTinh = new JComboBox();
		cbGioiTinh.setFont(new Font("Tahoma", Font.PLAIN, 14));
		cbGioiTinh.setBackground(Color.WHITE);
		cbGioiTinh.setModel(new DefaultComboBoxModel(new String[] { "Nam", "Nữ" }));
		cbGioiTinh.setBounds(95, 60, 95, 20);
		pnThongTin.add(cbGioiTinh);

		JLabel lbNgaySinh = new JLabel("Ngày sinh:");
		lbNgaySinh.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbNgaySinh.setBounds(205, 60, 80, 20);
		pnThongTin.add(lbNgaySinh);

		txtNgaySinh = new JTextField();
		txtNgaySinh.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtNgaySinh.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtNgaySinh.setBounds(283, 61, 177, 20);
		pnThongTin.add(txtNgaySinh);
		txtNgaySinh.setColumns(10);

		JLabel lbDiaChi = new JLabel("Dân tộc:");
		lbDiaChi.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbDiaChi.setBounds(20, 120, 60, 20);
		pnThongTin.add(lbDiaChi);

		txtDiaChi = new JTextField();
		txtDiaChi.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtDiaChi.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtDiaChi.setBounds(95, 90, 365, 20);
		pnThongTin.add(txtDiaChi);
		txtDiaChi.setColumns(10);

		JLabel lbDanToc = new JLabel("Địa chỉ:");
		lbDanToc.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbDanToc.setBounds(20, 90, 60, 20);
		pnThongTin.add(lbDanToc);

		txtDanToc = new JTextField();
		txtDanToc.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtDanToc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtDanToc.setBounds(95, 120, 95, 20);
		pnThongTin.add(txtDanToc);
		txtDanToc.setColumns(10);

		JLabel lbTonGiao = new JLabel("Tôn giáo:");
		lbTonGiao.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbTonGiao.setBounds(205, 120, 70, 20);
		pnThongTin.add(lbTonGiao);

		txtTonGiao = new JTextField();
		txtTonGiao.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtTonGiao.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtTonGiao.setBounds(283, 120, 177, 20);
		pnThongTin.add(txtTonGiao);
		txtTonGiao.setColumns(10);

		JLabel lbSoDT = new JLabel("Số ĐT:");
		lbSoDT.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbSoDT.setBounds(20, 150, 70, 20);
		pnThongTin.add(lbSoDT);

		txtSoDT = new JTextField();
		txtSoDT.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		txtSoDT.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtSoDT.setBounds(95, 150, 95, 20);
		pnThongTin.add(txtSoDT);
		txtSoDT.setColumns(10);

		JLabel lbSoTaiKhoan = new JLabel("Tài khoản:");
		lbSoTaiKhoan.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbSoTaiKhoan.setBounds(205, 150, 80, 20);
		pnThongTin.add(lbSoTaiKhoan);

		txtSoTaiKhoan = new JTextField();
		txtSoTaiKhoan.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		txtSoTaiKhoan.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtSoTaiKhoan.setBounds(283, 150, 177, 20);
		pnThongTin.add(txtSoTaiKhoan);
		txtSoTaiKhoan.setColumns(10);

		JLabel lbSoCMNN = new JLabel("Số CMND:");
		lbSoCMNN.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbSoCMNN.setBounds(20, 180, 73, 20);
		pnThongTin.add(lbSoCMNN);

		txtCmnd = new JTextField();
		txtCmnd.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtCmnd.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtCmnd.setBounds(95, 180, 95, 20);
		pnThongTin.add(txtCmnd);
		txtCmnd.setColumns(10);

		JLabel lbTieuDe = new JLabel("HỒ SƠ NHÂN VIÊN");
		lbTieuDe.setForeground(Color.BLUE);
		lbTieuDe.setHorizontalAlignment(SwingConstants.CENTER);
		lbTieuDe.setAlignmentX(Component.CENTER_ALIGNMENT);
		lbTieuDe.setFont(new Font("Times New Roman", Font.BOLD, 25));
		lbTieuDe.setBounds(40, 24, 1282, 40);
		contentPane.add(lbTieuDe);

		JPanel pnDonVi = new JPanel();
		pnDonVi.setBorder(
				new TitledBorder(null, "\u0110\u01A1n v\u1ECB", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnDonVi.setBounds(500, 320, 485, 100);
		contentPane.add(pnDonVi);
		pnDonVi.setLayout(null);

		JLabel lbMaPhong = new JLabel("Mã phòng:");
		lbMaPhong.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbMaPhong.setBounds(10, 23, 76, 20);
		pnDonVi.add(lbMaPhong);

		txtMaPhong = new JTextField();
		txtMaPhong.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtMaPhong.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtMaPhong.setBounds(95, 25, 95, 20);
		pnDonVi.add(txtMaPhong);
		txtMaPhong.setColumns(10);

		JLabel lbTenPhong = new JLabel("Tên phòng:");
		lbTenPhong.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbTenPhong.setBounds(200, 25, 90, 20);
		pnDonVi.add(lbTenPhong);

		txtTenPhong = new JTextField();
		txtTenPhong.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		txtTenPhong.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtTenPhong.setBounds(297, 25, 178, 20);
		pnDonVi.add(txtTenPhong);
		txtTenPhong.setColumns(10);

		JLabel lbMaChucVu = new JLabel("Mã chức vụ:");
		lbMaChucVu.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbMaChucVu.setBounds(10, 65, 90, 20);
		pnDonVi.add(lbMaChucVu);

		txtMaChucVu = new JTextField();
		txtMaChucVu.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		txtMaChucVu.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtMaChucVu.setBounds(95, 65, 95, 20);
		pnDonVi.add(txtMaChucVu);
		txtMaChucVu.setColumns(10);

		JLabel lbTenChucVu = new JLabel("Tên chức vụ:");
		lbTenChucVu.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbTenChucVu.setBounds(200, 65, 95, 20);
		pnDonVi.add(lbTenChucVu);

		txtTenChucVu = new JTextField();
		txtTenChucVu.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtTenChucVu.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		txtTenChucVu.setBounds(297, 65, 178, 20);
		pnDonVi.add(txtTenChucVu);
		txtTenChucVu.setColumns(10);

		JPanel pnTrinhDo = new JPanel();
		pnTrinhDo.setBorder(
				new TitledBorder(null, "Tr\u00ECnh \u0111\u1ED9", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnTrinhDo.setBounds(500, 450, 485, 100);
		contentPane.add(pnTrinhDo);
		pnTrinhDo.setLayout(null);

		JLabel lbMaTrinhDo = new JLabel("Mã trình độ:");
		lbMaTrinhDo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbMaTrinhDo.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		lbMaTrinhDo.setBounds(10, 25, 90, 20);
		pnTrinhDo.add(lbMaTrinhDo);

		txtMaTrinhDo = new JTextField();
		txtMaTrinhDo.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		txtMaTrinhDo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtMaTrinhDo.setBounds(99, 25, 85, 20);
		pnTrinhDo.add(txtMaTrinhDo);
		txtMaTrinhDo.setColumns(10);

		JLabel lbTrinhDo = new JLabel("Trình độ:");
		lbTrinhDo.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbTrinhDo.setBounds(200, 25, 80, 20);
		pnTrinhDo.add(lbTrinhDo);

		txtTrinhDo = new JTextField();
		txtTrinhDo.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtTrinhDo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtTrinhDo.setBounds(295, 25, 180, 20);
		pnTrinhDo.add(txtTrinhDo);
		txtTrinhDo.setColumns(10);

		JLabel lbChuyenNganh = new JLabel("Chuyên ngành:");
		lbChuyenNganh.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbChuyenNganh.setBounds(10, 60, 120, 20);
		pnTrinhDo.add(lbChuyenNganh);

		txtChuyenNganh = new JTextField();
		txtChuyenNganh.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		txtChuyenNganh.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtChuyenNganh.setBounds(155, 60, 320, 20);
		pnTrinhDo.add(txtChuyenNganh);
		txtChuyenNganh.setColumns(10);

		JPanel pnHinhAnh = new JPanel();
		pnHinhAnh.setBorder(
				new TitledBorder(null, "H\u00ECnh \u1EA3nh", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnHinhAnh.setBounds(1013, 95, 309, 210);
		contentPane.add(pnHinhAnh);
		pnHinhAnh.setLayout(null);

		lbHinhAnh = new JLabel("");
		lbHinhAnh.setBorder(new LineBorder(Color.LIGHT_GRAY));
		lbHinhAnh.setBounds(10, 35, 115, 135);
		pnHinhAnh.add(lbHinhAnh);

		btSuaAnh = new JButton("Sửa");
		btSuaAnh.setForeground(new Color(50, 205, 50));
		btSuaAnh.setBackground(Color.WHITE);
		btSuaAnh.setHorizontalAlignment(SwingConstants.LEFT);
		btSuaAnh.setIcon(new ImageIcon("img\\themanh.png"));
		btSuaAnh.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btSuaAnh.setBounds(150, 51, 120, 30);
		pnHinhAnh.add(btSuaAnh);

		btXoaAnh = new JButton("Xóa");
		btXoaAnh.setBackground(Color.WHITE);
		btXoaAnh.setForeground(new Color(255, 0, 0));
		btXoaAnh.setHorizontalAlignment(SwingConstants.LEFT);
		btXoaAnh.setIcon(new ImageIcon("img\\xoaanh.png"));
		btXoaAnh.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btXoaAnh.setBounds(150, 125, 120, 30);
		pnHinhAnh.add(btXoaAnh);

		JPanel pnHopDong = new JPanel();
		pnHopDong.setBorder(new TitledBorder(
				new TitledBorder(UIManager.getBorder("TitledBorder.border"),
						"H\u1EE3p \u0111\u1ED3ng lao \u0111\u1ED9ng", TitledBorder.LEADING, TitledBorder.TOP, null,
						new Color(0, 0, 0)),
				"H\u1EE3p \u0111\u1ED3ng lao \u0111\u1ED9ng", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnHopDong.setBounds(1012, 319, 310, 231);
		contentPane.add(pnHopDong);
		pnHopDong.setLayout(null);

		JLabel lbMaHD = new JLabel("Mã HD:");
		lbMaHD.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbMaHD.setBounds(10, 40, 60, 20);
		pnHopDong.add(lbMaHD);

		JLabel lbLoaiHD = new JLabel("Loại HD:");
		lbLoaiHD.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbLoaiHD.setBounds(10, 70, 71, 20);
		pnHopDong.add(lbLoaiHD);

		JLabel lbNgayBD = new JLabel("Ngày BĐ:");
		lbNgayBD.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbNgayBD.setBounds(10, 100, 71, 20);
		pnHopDong.add(lbNgayBD);

		JLabel lbNgayKy = new JLabel("Ngày ký:");
		lbNgayKy.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbNgayKy.setBounds(10, 130, 71, 20);
		pnHopDong.add(lbNgayKy);

		JLabel lbNgayKT = new JLabel("Ngày KT:");
		lbNgayKT.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbNgayKT.setBounds(10, 160, 71, 20);
		pnHopDong.add(lbNgayKT);

		txtMaHD = new JTextField();
		txtMaHD.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtMaHD.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		txtMaHD.setBounds(102, 40, 198, 20);
		pnHopDong.add(txtMaHD);
		txtMaHD.setColumns(10);

		txtLoaiHD = new JTextField();
		txtLoaiHD.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtLoaiHD.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		txtLoaiHD.setColumns(10);
		txtLoaiHD.setBounds(102, 70, 198, 20);
		pnHopDong.add(txtLoaiHD);

		txtNgayBD = new JTextField();
		txtNgayBD.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtNgayBD.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		txtNgayBD.setColumns(10);
		txtNgayBD.setBounds(102, 100, 198, 20);
		pnHopDong.add(txtNgayBD);

		txtNgayKy = new JTextField();
		txtNgayKy.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtNgayKy.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtNgayKy.setColumns(10);
		txtNgayKy.setBounds(102, 130, 198, 20);
		pnHopDong.add(txtNgayKy);

		txtNgayKT = new JTextField();
		txtNgayKT.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtNgayKT.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtNgayKT.setColumns(10);
		txtNgayKT.setBounds(102, 160, 198, 20);
		pnHopDong.add(txtNgayKT);

		JPanel pnLuong = new JPanel();
		pnLuong.setBorder(
				new TitledBorder(null, "L\u01B0\u01A1ng", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnLuong.setBounds(1012, 565, 310, 115);
		contentPane.add(pnLuong);
		pnLuong.setLayout(null);

		JLabel lbMaLuong = new JLabel("Mã bậc lương:");
		lbMaLuong.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbMaLuong.setBounds(10, 30, 120, 20);
		pnLuong.add(lbMaLuong);

		JLabel lbPhuCap = new JLabel("Phụ cấp:");
		lbPhuCap.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbPhuCap.setBounds(10, 75, 99, 20);
		pnLuong.add(lbPhuCap);

		txtMaLuong = new JTextField();
		txtMaLuong.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtMaLuong.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtMaLuong.setBounds(140, 30, 160, 20);
		pnLuong.add(txtMaLuong);
		txtMaLuong.setColumns(10);

		txtLuongThuc = new JTextField();
		txtLuongThuc.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		txtLuongThuc.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtLuongThuc.setColumns(10);
		txtLuongThuc.setBounds(140, 75, 160, 20);
		pnLuong.add(txtLuongThuc);

		JPanel pnChucNang = new JPanel();
		pnChucNang.setBorder(
				new TitledBorder(null, "Ch\u1EE9c n\u0103ng", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnChucNang.setBounds(500, 565, 485, 115);
		contentPane.add(pnChucNang);
		pnChucNang.setLayout(null);

		btnThemNhanVien = new JButton("Thêm nhân viên");
		btnThemNhanVien.setBackground(SystemColor.inactiveCaptionBorder);
		btnThemNhanVien.setHorizontalAlignment(SwingConstants.LEFT);
		btnThemNhanVien.setIcon(new ImageIcon("img\\them.png"));
		btnThemNhanVien.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnThemNhanVien.setBounds(30, 30, 193, 30);
		pnChucNang.add(btnThemNhanVien);

		btXoaNhanVien = new JButton("Xóa nhân viên");
		btXoaNhanVien.setBackground(SystemColor.inactiveCaptionBorder);
		btXoaNhanVien.setHorizontalAlignment(SwingConstants.LEFT);
		btXoaNhanVien.setIcon(new ImageIcon("img\\xoa.png"));
		btXoaNhanVien.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btXoaNhanVien.setBounds(253, 30, 193, 30);
		pnChucNang.add(btXoaNhanVien);

		btSuaNhanVien = new JButton("Sửa nhân viên");
		btSuaNhanVien.setBackground(SystemColor.inactiveCaptionBorder);
		btSuaNhanVien.setHorizontalAlignment(SwingConstants.LEFT);
		btSuaNhanVien.setIcon(new ImageIcon("img\\sua1.png"));
		btSuaNhanVien.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btSuaNhanVien.setBounds(30, 70, 193, 30);
		pnChucNang.add(btSuaNhanVien);

		btTroVe = new JButton("Về trang chủ");
		btTroVe.setBackground(SystemColor.menu);
		btTroVe.setIcon(new ImageIcon("img\\home.png"));
		btTroVe.setHorizontalAlignment(SwingConstants.LEFT);
		btTroVe.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btTroVe.setBounds(253, 70, 193, 30);
		pnChucNang.add(btTroVe);
		this.loadData("", "");
		this.display();
		this.search();
		this.delete();
		this.themNhanVien();
		this.suaNhanVien();
		this.troVe();
		this.suaAnh();
	}

	// tai du lieu len table
	public void loadData(String cond, String value) {
		list = new ArrayList<>();
		list = nhanvienBus.loadDataByCondition(cond, value);
		Vector<String> row;
		if (!list.isEmpty()) {
			for (NhanVienDTO nhanvienDto : list) {
				row = new Vector<>();
				row.add(nhanvienDto.getMaNV());
				row.add(nhanvienDto.getHo() + " " + nhanvienDto.getTen());
				row.add(String.valueOf(nhanvienDto.getNgaySinh()));
				row.add(nhanvienDto.getGioiTinh());
				model.addRow(row);
			}
			table.setModel(model);
		}
	}

	// hiển thị lên field;
	public void display() {
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				int k = table.getSelectedRow();
				if (k >= 0) {
					for (NhanVienDTO nhanvien : list) {
						if (table.getValueAt(k, 0) == nhanvien.getMaNV()) {
							nhanvienHienHanh = nhanvien;
							txtMa.setText(nhanvien.getMaNV());
							txtHoten.setText(nhanvien.getHo() + " " + nhanvien.getTen());
							txtNgaySinh.setText(nhanvien.getNgaySinh().toString());
							if (nhanvien.getGioiTinh().equals("Nam")) {
								cbGioiTinh.setSelectedItem("Nam");
							} else {
								cbGioiTinh.setSelectedItem("Nữ");
							}
							txtDiaChi.setText(nhanvien.getDiaChi());
							txtDanToc.setText(nhanvien.getDanToc());
							txtTonGiao.setText(nhanvien.getTonGiao());
							txtSoDT.setText(nhanvien.getSoDT());
							txtSoTaiKhoan.setText(nhanvien.getSoTaiKhoan());
							txtCmnd.setText(nhanvien.getSoCMND());
							phongbanDto = phongbanBus.getPhongBan(nhanvien.getMaPhongBan());
							txtMaPhong.setText(nhanvien.getMaPhongBan());
							txtTenPhong.setText(phongbanDto.getTenPhong());
							chucvuDto = chucvuBus.getChucVu(nhanvien.getMacv());
							txtMaChucVu.setText(nhanvien.getMacv());
							txtTenChucVu.setText(chucvuDto.getTenChucVu());
							trinhdoDto = trinhdoBus.getTrinhDo(nhanvien.getMaTDHV());
							txtMaTrinhDo.setText(nhanvien.getMaTDHV());
							txtTrinhDo.setText(trinhdoDto.getTenTDHV());
							txtChuyenNganh.setText(trinhdoDto.getChuyenNganh());
							hopdongDto = hopdongBus.getHopDongLaoDong(nhanvien.getMahdld());
							txtMaHD.setText(nhanvien.getMahdld());
							txtLoaiHD.setText(hopdongDto.getLoaiHopDong());
							txtNgayBD.setText(hopdongDto.getNgayBatDau().toString());
							txtNgayKT.setText(hopdongDto.getNgayKetThuc().toString());
							txtNgayKy.setText(hopdongDto.getNgayKy().toString());
							txtMaLuong.setText(nhanvien.getMaBacLuong());
							txtLuongThuc.setText(nhanvien.getMaPhuCap());
							lbHinhAnh.setIcon(new ImageIcon("img\\imgnhansu\\" + nhanvien.getHinhAnh()));
							break;

						}
					}
				}
			}
		});
	}

	// event for button btTim
	public void search() {
		btTim.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (txtTuKhoa.getText().isEmpty() && !cbTimKiem.getSelectedItem().equals("Toàn danh sách")) {
					JOptionPane.showMessageDialog(null, "Xin nhập vào từ khóa cần tìm!!!");
				} else {
					String cond = (String) cbTimKiem.getSelectedItem();
					String value = txtTuKhoa.getText();
					refreshTable();
					switch (cond) {
					case "Mã nhân viên":
						resultSearchId(value);
						break;
					case "Mã phòng ban":
						resultSearchIdPhong(value);
						break;
					case "Tên":
						resultSearchName(value);
						break;
					case "Mã chức vụ":
						resultSearchIdChucVu(value);
						break;
					case "Email":
						resultSearchIdEmail(value);
						break;
					case "Toàn danh sách":
						loadData("", "");
						break;
					default:
						break;
					}
				}
			}
		});
	}

	// tìm theo mã phòng
	public void resultSearchIdPhong(String value) {
		Vector<String> row;
		if (!list.isEmpty()) {
			for (NhanVienDTO nhanvienDto : list) {
				if (nhanvienDto.getMaPhongBan().equals(value)) {
					row = new Vector<>();
					row.add(nhanvienDto.getMaNV());
					row.add(nhanvienDto.getHo() + " " + nhanvienDto.getTen());
					row.add(String.valueOf(nhanvienDto.getNgaySinh()));
					row.add(nhanvienDto.getGioiTinh());
					model.addRow(row);
				}
			}
			table.setModel(model);
		} else {
			JOptionPane.showMessageDialog(null, "Không tìm thấy nhân viên!");
		}
	}

	// tìm theo id
	public void resultSearchId(String value) {
		Vector<String> row;
		if (!list.isEmpty()) {
			for (NhanVienDTO nhanvienDto : list) {
				if (nhanvienDto.getMaNV().equals(value)) {
					row = new Vector<>();
					row.add(nhanvienDto.getMaNV());
					row.add(nhanvienDto.getHo() + " " + nhanvienDto.getTen());
					row.add(String.valueOf(nhanvienDto.getNgaySinh()));
					row.add(nhanvienDto.getGioiTinh());
					model.addRow(row);
					break;
				}
			}
			if (model.getRowCount()<1) {
				JOptionPane.showMessageDialog(null, "Không tìm thấy nhân viên!");
			}
			table.setModel(model);
		}
	}

	// tìm theo tên
	public void resultSearchName(String value) {
		Vector<String> row;
		if (!list.isEmpty()) {
			for (NhanVienDTO nhanvienDto : list) {
				if (nhanvienDto.getTen().equals(value)) {
					row = new Vector<>();
					row.add(nhanvienDto.getMaNV());
					row.add(nhanvienDto.getHo() + " " + nhanvienDto.getTen());
					row.add(String.valueOf(nhanvienDto.getNgaySinh()));
					row.add(nhanvienDto.getGioiTinh());
					model.addRow(row);
				}
			}
			table.setModel(model);
		} else {
			JOptionPane.showMessageDialog(null, "Không tìm thấy nhân viên!");
		}
	}

	// Tìm theo mã chức vụ
	public void resultSearchIdChucVu(String value) {
		Vector<String> row;
		if (!list.isEmpty()) {
			for (NhanVienDTO nhanvienDto : list) {
				if (nhanvienDto.getMacv().equals(value)) {
					row = new Vector<>();
					row.add(nhanvienDto.getMaNV());
					row.add(nhanvienDto.getHo() + " " + nhanvienDto.getTen());
					row.add(String.valueOf(nhanvienDto.getNgaySinh()));
					row.add(nhanvienDto.getGioiTinh());
					model.addRow(row);
				}
			}
			table.setModel(model);
		} else {
			JOptionPane.showMessageDialog(null, "Không tìm thấy nhân viên!");
		}
	}

	// tìm theo email;
	public void resultSearchIdEmail(String value) {
		Vector<String> row;
		if (!list.isEmpty()) {
			for (NhanVienDTO nhanvienDto : list) {
				if (nhanvienDto.getEmail().equals(value)) {
					row = new Vector<>();
					row.add(nhanvienDto.getMaNV());
					row.add(nhanvienDto.getHo() + " " + nhanvienDto.getTen());
					row.add(String.valueOf(nhanvienDto.getNgaySinh()));
					row.add(nhanvienDto.getGioiTinh());
					model.addRow(row);
				}
			}
			table.setModel(model);
		} else {
			JOptionPane.showMessageDialog(null, "Không tìm thấy nhân viên!");
		}
	}

	// refresh lai table
	public void refreshTable() {
		int count = model.getRowCount();
		while (count > 0) {
			model.removeRow(count - 1);
			count--;
		}
	}

	// xóa nhân viên
	public void delete() {
		btXoaNhanVien.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int k = table.getSelectedRow();
				if (k >= 0) {
					String id = (String) table.getValueAt(k, 0);
					String idHopDong = txtMaHD.getText();
					int respone = JOptionPane.showConfirmDialog(null, "Bạn có chắc chắn muốn xóa nhân viên này ? ",
							"Confirm", JOptionPane.YES_NO_OPTION);
					if (respone == JOptionPane.YES_OPTION) {
						// xoa mã hop dong, xóa luong và xoa nhan vien
						if (luongchitietBus.delete(id) && nhanvienBus.deleteEmployee(id)
								&& hopdongBus.delete(idHopDong)) {
							refreshTable();
							loadData("", "");
							JOptionPane.showMessageDialog(null, "Xóa thành công!");
						} else {
							JOptionPane.showMessageDialog(null, "Xóa thất bại");
						}
					}
				} else {
					JOptionPane.showMessageDialog(null, "Mời bạn chọn dòng chứa nhân viên cần xóa rồi bấm xóa!");
				}
			}
		});
	}

	public void themNhanVien() {
		btnThemNhanVien.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				fThemNhanVien f = new fThemNhanVien();
				f.setVisible(true);
			}
		});
	}

	public void suaNhanVien() {
		btSuaNhanVien.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (table.getSelectedRow() >= 0) {
					setVisible(false);
					new fSuaNhanVien().setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "Chọn viên cần sửa!");
				}

			}
		});
	}

	public void troVe() {
		btTroVe.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				new fMain().setVisible(true);

			}
		});
	}

	public void suaAnh() {
		btSuaAnh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> linkList = nhanvienBus.getListOneCol("hinhanh");
				String linkAnh = "";
				boolean flag;
				do {
					flag = true;
					JFileChooser fileChooser = new JFileChooser();
					int returnValue = fileChooser.showOpenDialog(contentPane);
					if (returnValue == JFileChooser.APPROVE_OPTION) {
						File selectedFile = fileChooser.getSelectedFile();
						linkAnh = selectedFile.getName().toString();
						for (String st : linkList) {
							if (st.equals(linkAnh)) {
								JOptionPane.showMessageDialog(null,
										"Ảnh này đã bị trùng với nhân viên khác!! Vui lòng chọn lại ảnh.");
								flag = false;
								break;
							}
						}
					}
				} while (!flag);
				if (nhanvienBus.updateOneCol("hinhanh", linkAnh, nhanvienHienHanh.getMaNV())) {
					JOptionPane.showMessageDialog(null, "Sửa ảnh thành công!!");
					lbHinhAnh.setIcon(new ImageIcon("img\\imgnhansu\\" + linkAnh));
				} else {
					JOptionPane.showMessageDialog(null, "Sửa ảnh thất bại!!");
				}

			}
		});

	}

	public void xoaAnh() {
		btXoaAnh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (nhanvienBus.updateOneCol("hinhanh", "", nhanvienHienHanh.getMaNV())) {
					JOptionPane.showMessageDialog(null, "Xóa ảnh thành công!!");
					nhanvienHienHanh.setHinhAnh("");
				} else {
					JOptionPane.showMessageDialog(null, "Xóa ảnh thất bại!!");
				}
			}
		});
	}

	public static NhanVienDTO getNhanvienHienHanh() {
		return nhanvienHienHanh;
	}

	public static void setNhanvienHienHanh(NhanVienDTO nhanvienHienHanh) {
		fQuanLyNhanSu.nhanvienHienHanh = nhanvienHienHanh;
	}

	public static ArrayList<NhanVienDTO> getList() {
		return list;
	}

	public static void setList(ArrayList<NhanVienDTO> list) {
		fQuanLyNhanSu.list = list;
	}

	public static PhongBanDTO getPhongbanDto() {
		return phongbanDto;
	}

	public static void setPhongbanDto(PhongBanDTO phongbanDto) {
		fQuanLyNhanSu.phongbanDto = phongbanDto;
	}

	public static ChucVuDTO getChucvuDto() {
		return chucvuDto;
	}

	public static void setChucvuDto(ChucVuDTO chucvuDto) {
		fQuanLyNhanSu.chucvuDto = chucvuDto;
	}

	public static TrinhDoHocVanDTO getTrinhdoDto() {
		return trinhdoDto;
	}

	public static void setTrinhdoDto(TrinhDoHocVanDTO trinhdoDto) {
		fQuanLyNhanSu.trinhdoDto = trinhdoDto;
	}

	public static HopDongLaoDongDTO getHopdongDto() {
		return hopdongDto;
	}

	public static void setHopdongDto(HopDongLaoDongDTO hopdongDto) {
		fQuanLyNhanSu.hopdongDto = hopdongDto;
	}

	public static LuongChiTietDTO getLuongchitietDto() {
		return luongchitietDto;
	}

	public static void setLuongchitietDto(LuongChiTietDTO luongchitietDto) {
		fQuanLyNhanSu.luongchitietDto = luongchitietDto;
	}

}
