package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.EmptyBorder;

import BUS.UserBUS;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.ActionEvent;

public class fLogin extends JFrame {

	private UserBUS userBUS = new UserBUS();
	private JPanel contentPane;
	private JTextField txfUsername;
	private JPasswordField pwfPassword;
	private JButton btnDangNhap;

	/**
	 * Launch the application.-
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fLogin frame = new fLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public fLogin() {
		// nimbus look and feel
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 280);
		setLocationRelativeTo(null);
		setTitle("Đăng nhập hệ thống");
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 374, 54);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Đăng Nhập");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblNewLabel.setBounds(0, 0, 374, 54);
		panel.add(lblNewLabel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 64, 384, 176);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblTnngNhp = new JLabel("Tên đăng nhập");
		lblTnngNhp.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTnngNhp.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblTnngNhp.setBounds(50, 26, 110, 20);
		panel_1.add(lblTnngNhp);
		
		JLabel lblMtKhu = new JLabel("Mật khẩu");
		lblMtKhu.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMtKhu.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMtKhu.setBounds(50, 75, 110, 20);
		panel_1.add(lblMtKhu);
		
		txfUsername = new JTextField("y");
		txfUsername.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txfUsername.setBounds(180, 24, 130, 25);
		panel_1.add(txfUsername);
		txfUsername.setColumns(10);
		
		pwfPassword = new JPasswordField("1");
		pwfPassword.setBounds(180, 75, 130, 25);
		panel_1.add(pwfPassword);
		
		pwfPassword.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER){
					btnDangNhap.doClick();
				}
			}
		});
		
		btnDangNhap = new JButton("Đăng Nhập");
		btnDangNhap.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username = txfUsername.getText();
				String password = new String(pwfPassword.getPassword());
				if(userBUS.Login(username, password))
				{
					dispose();
					fMain f = new fMain();
					f.setVisible(true);
				}else
				{
					JOptionPane.showMessageDialog(null, "Sai tên đăng nhập hoặc mật khẩu");
				}
			}
		});
		btnDangNhap.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnDangNhap.setBounds(187, 135, 123, 30);
		panel_1.add(btnDangNhap);
		
		JButton btnThoat = new JButton("Thoát");
		btnThoat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnThoat.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnThoat.setBounds(80, 135, 80, 30);
		panel_1.add(btnThoat);
	}
}
