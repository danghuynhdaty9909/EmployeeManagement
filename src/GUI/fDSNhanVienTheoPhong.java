package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import BUS.NhanVienBUS;
import DTO.NhanVienDTO;
import DTO.PhongBanDTO;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JButton;

@SuppressWarnings("serial")
public class fDSNhanVienTheoPhong extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private DefaultTableModel model;
	private JButton btOk;
	private JButton btClose;
	private static String idPosition = "";
	private NhanVienBUS nhanvienBUS = new NhanVienBUS();
	private static PhongBanDTO newPhongban = fQuanLyPhongBan.getPhongbanHienhanh();
	private static NhanVienDTO newBoss = new NhanVienDTO();
	private static NhanVienDTO oldBoss = new NhanVienDTO();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fDSNhanVienTheoPhong frame = new fDSNhanVienTheoPhong();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public fDSNhanVienTheoPhong() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 93, 1330, 387);
		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		table.setFont(new Font("SansSerif", Font.BOLD, 12));
		table.setModel(model = new DefaultTableModel(new Object[][] {},
				new String[] { "M\u00E3 Nh\u00E2n Vi\u00EAn", "H\u1ECD", "T\u00EAn", "Gi\u1EDBi T\u00EDnh", "Email",
						"S\u1ED1 \u0110i\u1EC7n Tho\u1EA1i", "S\u1ED1 CMND", "Qu\u00EA Qu\u00E1n",
						"M\u00E3 ph\u00F2ng ban", "M\u00E3 ch\u1EE9c v\u1EE5", "M\u00E3 h\u1EE3p \u0111\u1ED3ng",
						"M\u00E3 TDHV" }));
		scrollPane.setViewportView(table);

		JLabel lblNewLabel = new JLabel("Danh Sách Nhân Viên");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 30));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(89, 11, 1163, 71);
		contentPane.add(lblNewLabel);

		btOk = new JButton("Đồng Ý");
		btOk.setBounds(436, 529, 89, 23);
		contentPane.add(btOk);

		btClose = new JButton("Đóng");
		btClose.setBounds(835, 529, 89, 23);
		contentPane.add(btClose);
		oldBoss = nhanvienBUS.getNhanVienByID(fQuanLyPhongBan.getPhongbanHienhanh().getTruongPhong());
		loadData(fQuanLyPhongBan.getPhongbanHienhanh().getMaPhongBan());
		clickOK();
		clickCancel();
	}

	public void loadData(String id) {

		ArrayList<NhanVienDTO> list = nhanvienBUS.loadDataByCondition("maphongban", id);
		Vector<String> row;
		for (NhanVienDTO staff : list) {
			row = new Vector<>();
			row.add(staff.getMaNV());
			row.add(staff.getHo());
			row.add(staff.getTen());
			row.add(staff.getGioiTinh());
			row.add(staff.getEmail());
			row.add(staff.getSoDT());
			row.add(staff.getSoCMND());
			row.add(staff.getDiaChi());
			row.add(staff.getMaPhongBan());
			row.add(staff.getMacv());
			row.add(staff.getMahdld());
			row.add(staff.getMaTDHV());
			model.addRow(row);
		}
		table.setModel(model);
	}

	public void clickOK() {
		btOk.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int k = table.getSelectedRow();
				if (k < 0) {
					JOptionPane.showMessageDialog(null, "Xin chọn  dòng chứa thông tin nhân viên làm trưởng phòng!!!");
				} else {
					// cập nhật lại mã chức vụ của thằng trưởng phòng cũ
					oldBoss.setMacv((String) table.getValueAt(k, 9));
					newPhongban.setTruongPhong((String) table.getValueAt(k, 0));
					newBoss = nhanvienBUS.getNhanVienByID((String) table.getValueAt(k, 0));
					setVisible(false);
					// ghi lai lên form
					fSuaPhongBan fix = new fSuaPhongBan();
					fix.getTxtAddress().setText(newPhongban.getDiaChi());
					fix.getTxtBoss().setText(newBoss.getHo() + " " + newBoss.getTen());
					fix.getTxtName().setText(newPhongban.getTenPhong());
					fix.setVisible(true);
				}

			}
		});
	}

	public void clickCancel() {
		btClose.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				new fSuaPhongBan().setVisible(true);
			}
		});
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public DefaultTableModel getModel() {
		return model;
	}

	public void setModel(DefaultTableModel model) {
		this.model = model;
	}

	public static String getIdPosition() {
		return idPosition;
	}

	public static void setIdPosition(String idPosition) {
		fDSNhanVienTheoPhong.idPosition = idPosition;
	}

	public static PhongBanDTO getNewPhongban() {
		return newPhongban;
	}

	public static void setNewPhongban(PhongBanDTO newPhongban) {
		fDSNhanVienTheoPhong.newPhongban = newPhongban;
	}

	public static NhanVienDTO getNewBoss() {
		return newBoss;
	}

	public static void setNewBoss(NhanVienDTO newBoss) {
		fDSNhanVienTheoPhong.newBoss = newBoss;
	}

	public static NhanVienDTO getOldBoss() {
		return oldBoss;
	}

	public static void setOldBoss(NhanVienDTO oldBoss) {
		fDSNhanVienTheoPhong.oldBoss = oldBoss;
	}

}
