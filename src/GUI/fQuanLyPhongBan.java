package GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import DTO.NhanVienDTO;
import DTO.PhongBanDTO;

import java.awt.Color;
import javax.swing.ImageIcon;

public class fQuanLyPhongBan extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JButton btAdd;
	private JButton btFix;
	private JButton btClose;
	private DefaultTableModel model;
	private static PhongBanDTO phongbanHienhanh;
	private PhongBanBUS phongbanBus = new PhongBanBUS();
	private NhanVienBUS nhanvienBus = new NhanVienBUS();
	private static ArrayList<PhongBanDTO> list;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fQuanLyPhongBan frame = new fQuanLyPhongBan();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public fQuanLyPhongBan() {
		// nimbus look and feel
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		setTitle("Quản Lý Phòng Ban");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setLocationRelativeTo(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lbTitle = new JLabel("Danh Sách Phòng Ban");
		lbTitle.setForeground(Color.RED);
		lbTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lbTitle.setFont(new Font("Times New Roman", Font.BOLD, 25));
		lbTitle.setBounds(0, 37, 1350, 80);
		contentPane.add(lbTitle);

		btAdd = new JButton("Thêm Mới");
		btAdd.setIcon(new ImageIcon("img\\add.png"));
		btAdd.setForeground(Color.BLACK);
		btAdd.setBackground(Color.WHITE);
		btAdd.setFont(new Font("Tahoma", Font.BOLD, 15));
		btAdd.setBounds(96, 622, 162, 52);
		contentPane.add(btAdd);

		table = new JTable();
		table.setFont(new Font("Tahoma", Font.PLAIN, 15));
		table.setModel(model = new DefaultTableModel(new Object[][] {}, new String[] { "T\u00EAn Ph\u00F2ng",
				"Tr\u01B0\u1EDFng Ph\u00F2ng", "\u0110\u1ECBa Ch\u1EC9 Ph\u00F2ng", "S\u1ED1 Nh\u00E2n Vi\u00EAn" }));
		table.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 17));
		table.setRowHeight(25);

		JScrollPane sc = new JScrollPane(table);
		sc.setBounds(96, 146, 1139, 442);
		contentPane.add(sc);

		btFix = new JButton("Sửa");
		btFix.setIcon(new ImageIcon("img\\sua.png"));
		btFix.setBackground(Color.WHITE);
		btFix.setFont(new Font("Tahoma", Font.BOLD, 15));
		btFix.setBounds(566, 622, 162, 52);
		contentPane.add(btFix);

		btClose = new JButton("Đóng");
		btClose.setIcon(new ImageIcon("img\\cancel2.png"));
		btClose.setBackground(Color.WHITE);
		btClose.setFont(new Font("Tahoma", Font.BOLD, 15));
		btClose.setBounds(1073, 622, 162, 52);
		contentPane.add(btClose);
		loadData();
		themPhongBan();
		suaPhongBan();
		clickClose();
	}

	public void loadData() {
		list = phongbanBus.getAllPhongBan();
		if (list != null) {
			Vector<String> row;
			for (PhongBanDTO department : list) {
				row = new Vector<>();
				row.add(department.getTenPhong());
				row.add(nhanvienBus.getNhanVienByID(department.getTruongPhong()).getHo() + " "
						+ nhanvienBus.getNhanVienByID(department.getTruongPhong()).getTen());
				row.add(department.getDiaChi());
				row.add(String.valueOf(nhanvienBus.countEmployeeByCondition("maphongban", department.getMaPhongBan())));
				model.addRow(row);

			}
			table.setModel(model);
		}
	}

	public void themPhongBan() {
		btAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				new fThemPhongBan().setVisible(true);
			}
		});

	}

	public void suaPhongBan() {
		btFix.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int k = table.getSelectedRow();
				if (k < 0) {
					JOptionPane.showMessageDialog(null, "Chọn dòng chứa phòng ban cần sửa!");
				} else {
					for (PhongBanDTO phongban : list) {
						NhanVienDTO nhanvien = nhanvienBus.getNhanVienByID(phongban.getTruongPhong());
						if (phongban.getDiaChi().equals((String) table.getValueAt(k, 2))
								&& phongban.getTenPhong().equals((String) table.getValueAt(k, 0))
								&& (nhanvien.getHo() + " " + nhanvien.getTen())
										.equals((String) table.getValueAt(k, 1))) {
							phongbanHienhanh = phongban;
							setVisible(false);
							if(phongbanHienhanh!=null){
								fSuaPhongBan fix = new fSuaPhongBan();
								fix.setVisible(true);
								break;
							}
						}
					}
				}

			}
		});
	}

	public void clickClose() {
		btClose.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				new fMain().setVisible(true);
			}
		});
	}

	public static PhongBanDTO getPhongbanHienhanh() {
		return phongbanHienhanh;
	}

	public static void setPhongbanHienhanh(PhongBanDTO phongbanHienhanh) {
		fQuanLyPhongBan.phongbanHienhanh = phongbanHienhanh;
	}

}
