package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class fChamCong extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fChamCong frame = new fChamCong();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public fChamCong() {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, you can set the GUI to another look
			// and feel.
		}
		setTitle("Danh sách chấm công");
		setExtendedState(JFrame.MAXIMIZED_BOTH);// set full màn hình
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel header = new JPanel();
		header.setBounds(0, 0, 1362, 50);
		contentPane.add(header);
		header.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Danh Sách Chấm Công");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblNewLabel.setBounds(10, 11, 1342, 28);
		header.add(lblNewLabel);
		
		JPanel body = new JPanel();
		body.setBounds(10, 60, 1342, 510);
		contentPane.add(body);
		body.setLayout(null);
		
		JScrollPane scrChamCong = new JScrollPane();
		scrChamCong.setBounds(0, 0, 1342, 510);
		body.add(scrChamCong);
		JTable tblDSNV = new JTable();
		tblDSNV.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "Mã chấm công", "Ngày 1", "Tên",
				"Giới tính", "Ngày Sinh", "Địa chỉ", "Số điện thoại", "Email", "Phòng Ban" }));
		scrChamCong.setViewportView(tblDSNV);
		JPanel panel = new JPanel();
		panel.setBounds(10, 581, 1342, 113);
		contentPane.add(panel);
	}
}
