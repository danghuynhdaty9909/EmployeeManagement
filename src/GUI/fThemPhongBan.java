package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import DTO.PhongBanDTO;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.border.BevelBorder;

public class fThemPhongBan extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtBoss;
	private JTextField txtAddress;
	private JButton btAdd;
	private JButton btCancel;
	private JButton btGetBoss;
	private static PhongBanDTO phongbanDto;
	private PhongBanBUS phongbanBus = new PhongBanBUS();
	private NhanVienBUS nhanvienBus = new NhanVienBUS();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fThemPhongBan frame = new fThemPhongBan();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public fThemPhongBan() {
		// nimbus look and feel
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage("C:\\Users\\trung\\workspace\\GUI_Giaodien\\GUI_Giaodien\\img\\add1.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 659, 415);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		contentPane.setLayout(null);

		JLabel lbName = new JLabel("Tên Phòng Ban:");
		lbName.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbName.setBounds(117, 73, 125, 30);
		contentPane.add(lbName);

		JLabel lbBoss = new JLabel("Trưởng Phòng:");
		lbBoss.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbBoss.setBounds(117, 123, 125, 30);
		contentPane.add(lbBoss);

		JLabel lbAddress = new JLabel("Địa Chỉ Phòng:");
		lbAddress.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbAddress.setBounds(117, 175, 125, 30);
		contentPane.add(lbAddress);

		txtName = new JTextField();
		txtName.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtName.setBounds(280, 73, 265, 30);
		contentPane.add(txtName);
		txtName.setColumns(10);

		txtBoss = new JTextField();
		txtBoss.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtBoss.setBounds(280, 123, 220, 30);
		contentPane.add(txtBoss);
		txtBoss.setColumns(10);

		txtAddress = new JTextField();
		txtAddress.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtAddress.setBounds(280, 173, 265, 30);
		contentPane.add(txtAddress);
		txtAddress.setColumns(10);

		JLabel lbTitle = new JLabel("Thêm Phòng Ban");
		lbTitle.setForeground(Color.GREEN);
		lbTitle.setBackground(new Color(0, 100, 0));
		lbTitle.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lbTitle.setBounds(20, 11, 623, 33);
		contentPane.add(lbTitle);

		btAdd = new JButton("Thêm");
		btAdd.setBackground(Color.WHITE);
		btAdd.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btAdd.setBounds(205, 228, 86, 30);
		contentPane.add(btAdd);

		btCancel = new JButton("Hủy");
		btCancel.setBackground(Color.WHITE);
		btCancel.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btCancel.setBounds(423, 228, 77, 30);
		btCancel.addActionListener(this);
		contentPane.add(btCancel);

		btGetBoss = new JButton("New button");
		btGetBoss.setBackground(Color.WHITE);
		btGetBoss.setFont(new Font("Serif", Font.BOLD, 15));
		btGetBoss.setBounds(510, 123, 35, 30);
		contentPane.add(btGetBoss);
		btAdd.addActionListener(this);
		btGetBoss.addActionListener(this);
		phongbanDto = new PhongBanDTO();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btGetBoss) {
			if (txtName.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Tên phòng không thể rỗng!!!");
			} else {
				if (txtAddress.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Địa chỉ phòng không thể rỗng!!!");
				} else {
					if (phongbanBus.check(txtName.getText(), txtAddress.getText())) {
						phongbanDto.setDiaChi(txtAddress.getText());
						phongbanDto.setTenPhong(txtName.getText());
						ArrayList<String> list = phongbanBus.getListOneCol("maphongban", "", "");
						int max = 0, temp = 0;// lấy mã mới
						if (list != null) {
							for (String st : list) {
								temp = Integer.parseInt(st.substring(1));
								if (temp > max) {
									max = temp;
								}
							}
						}
						phongbanDto.setMaPhongBan(String.valueOf("P" + (max + 1)));
						fDSNhanVien ds = new fDSNhanVien();
						phongbanDto.setTruongPhong(fDSNhanVien.getNewIdBoss());
						ds.setVisible(true);
						setVisible(false);
					}else{
						JOptionPane.showMessageDialog(null, "Trùng tên phòng và địa chỉ phòng!");
					}
					
					
				}
			}
		}
		if (e.getSource() == btCancel) {
			setVisible(false);
			new fQuanLyPhongBan().setVisible(true);
		}
		if (e.getSource() == btAdd) {
			if (txtBoss.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Nhấn nút bên phải khung Trưởng phòng để chon trưởng phòng!!!");
			} else {
				phongbanDto.setDiaChi(txtAddress.getText());
				phongbanDto.setTenPhong(txtName.getText());
				phongbanDto.setTruongPhong(fDSNhanVien.getNewIdBoss());
				phongbanDto.setMaPhongBan(fDSNhanVien.getDto().getMaPhongBan());
				boolean flag_1 = phongbanBus.insert(phongbanDto);

				if (flag_1) {
					boolean flag_2 = nhanvienBus.updateOneCol("macv", "TP", phongbanDto.getTruongPhong());
					boolean flag_3 = nhanvienBus.updateOneCol("maphongban", phongbanDto.getMaPhongBan(),
							phongbanDto.getTruongPhong());
					if (flag_2 && flag_3) {

						JOptionPane.showMessageDialog(null, "Thêm thành công!!!");
					}
				} else {
					JOptionPane.showMessageDialog(null, "Thêm thất bại!!!");
				}
			}

		}
	}

	public JTextField getTxtName() {
		return txtName;
	}

	public void setTxtName(JTextField txtName) {
		this.txtName = txtName;
	}

	public JTextField getTxtBoss() {
		return txtBoss;
	}

	public void setTxtBoss(JTextField txtBoss) {
		this.txtBoss = txtBoss;
	}

	public JTextField getTxtAddress() {
		return txtAddress;
	}

	public void setTxtAddress(JTextField txtAddress) {
		this.txtAddress = txtAddress;
	}

	public JButton getBtAdd() {
		return btAdd;
	}

	public void setBtAdd(JButton btAdd) {
		this.btAdd = btAdd;
	}

	public JButton getBtCancel() {
		return btCancel;
	}

	public void setBtCancel(JButton btCancel) {
		this.btCancel = btCancel;
	}

	public JButton getBtGetBoss() {
		return btGetBoss;
	}

	public void setBtGetBoss(JButton btGetBoss) {
		this.btGetBoss = btGetBoss;
	}

	public static PhongBanDTO getPhongbanDto() {
		return phongbanDto;
	}

	public static void setPhongbanDto(PhongBanDTO phongbanDto) {
		fThemPhongBan.phongbanDto = phongbanDto;
	}

}
