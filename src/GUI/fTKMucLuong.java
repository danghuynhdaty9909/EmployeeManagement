package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

import BUS.LuongChiTietBUS;
import DTO.LuongChiTietDTO;

import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Toolkit;

public class fTKMucLuong extends JFrame {

	private JPanel contentPane;
	private JProgressBar jb1;
	private JProgressBar jb2;
	private JProgressBar jb3;
	private JProgressBar jb4;
	private JProgressBar jb5;
	private int ml1 = 0;
	private int ml2 = 0;
	private int ml3 = 0;
	private int ml4 = 0;
	private int ml5 = 0;
	private int sum = 0;
	private int month = 1;
	private ArrayList<LuongChiTietDTO> listLuong;
	private LuongChiTietBUS luongBus = new LuongChiTietBUS();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fTKMucLuong frame = new fTKMucLuong();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public fTKMucLuong() {
		setTitle("Thống Kê Lương");
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, you can set the GUI to another look
			// and feel.
		}
		setExtendedState(JFrame.MAXIMIZED_BOTH);// set full màn hình
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\money.png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1366, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setLocationRelativeTo(null);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBounds(220, 0, 915, 729);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("Thống Kê Lương");
		lblNewLabel.setForeground(Color.BLUE);
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 25));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 11, 915, 80);
		panel.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("5 -> 10 triệu vnd:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(175, 226, 132, 26);
		panel.add(lblNewLabel_1);

		JLabel label = new JLabel("10 -> 15 triệu vnd: ");
		label.setFont(new Font("Tahoma", Font.BOLD, 15));
		label.setBounds(175, 315, 156, 26);
		panel.add(label);

		JLabel lblNewLabel_2 = new JLabel("15 -> 20 triệu vnd");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_2.setBounds(175, 399, 142, 26);
		panel.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("20 -> 25 triệu vnd");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_3.setBounds(175, 485, 142, 26);
		panel.add(lblNewLabel_3);

		JLabel lblTrnTriu = new JLabel("Trên 25 triệu vnd");
		lblTrnTriu.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblTrnTriu.setBounds(175, 575, 142, 26);
		panel.add(lblTrnTriu);

		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4" }));
		listLuong = new ArrayList<>();
		boolean flag = luongBus.TinhLuongThang(month);
		if (flag) {
			listLuong = luongBus.getAllLuongChiTietDTO();
			if (!listLuong.isEmpty()) {
				ml1 = countEmployee(5000000, 10000000,month);
				ml2 = countEmployee(10000000, 15000000,month);
				ml3 = countEmployee(15000000, 20000000,month);
				ml4 = countEmployee(20000000, 25000000,month);
				ml5 = countEmployee(250000000, Integer.MAX_VALUE,month);
				sum = ml1 + ml2 + ml3 + ml4 + ml5;
			} else {

			}

			comboBox.addItemListener(new ItemListener() {

				@Override
				public void itemStateChanged(ItemEvent e) {
					month = Integer.parseInt(comboBox.getSelectedItem().toString().substring(6));
					if (luongBus.TinhLuongThang(month)) {
						listLuong = luongBus.getAllLuongChiTietDTO();
						ml1 = countEmployee(5000000, 10000000,month);
						ml2 = countEmployee(10000000, 15000000,month);
						ml3 = countEmployee(15000000, 20000000,month);
						ml4 = countEmployee(20000000, 25000000,month);
						ml5 = countEmployee(250000000, Integer.MAX_VALUE,month);
						sum = ml1 + ml2 + ml3 + ml4 + ml5;
					}

				}
			});
			int pcMl1 = ml1 * 100 / sum;
			int pcMl2 = ml2 * 100 / sum;
			int pcMl3 = ml3 * 100 / sum;
			int pcMl4 = ml4 * 100 / sum;
			int pcMl5 = ml5 * 100 / sum;

			comboBox.setBounds(632, 152, 72, 20);
			panel.add(comboBox);

			jb1 = new JProgressBar(0, 100);
			jb1.setForeground(Color.GRAY);
			jb1.setValue(pcMl1);
			jb1.setStringPainted(true);
			jb1.setBounds(356, 224, 348, 38);
			panel.add(jb1);

			jb2 = new JProgressBar(0, 100);
			jb2.setForeground(Color.GRAY);
			jb2.setValue(pcMl2);
			jb2.setStringPainted(true);
			jb2.setBounds(356, 303, 348, 38);
			panel.add(jb2);

			jb3 = new JProgressBar(0, 100);
			jb3.setForeground(Color.GRAY);
			jb3.setValue(pcMl3);
			jb3.setStringPainted(true);
			jb3.setBounds(356, 387, 348, 38);
			panel.add(jb3);

			jb4 = new JProgressBar(0, 100);
			jb4.setForeground(Color.GRAY);
			jb4.setValue(pcMl4);
			jb4.setStringPainted(true);
			jb4.setBounds(356, 473, 348, 38);
			panel.add(jb4);

			jb5 = new JProgressBar(0, 100);
			jb5.setForeground(Color.GRAY);
			jb5.setValue(pcMl5);
			jb5.setStringPainted(true);
			jb5.setBounds(356, 563, 348, 38);
			panel.add(jb5);
			JLabel lblNewLabel_4 = new JLabel("Lương:");
			lblNewLabel_4.setForeground(Color.BLUE);
			lblNewLabel_4.setFont(new Font("SansSerif", Font.BOLD, 15));
			lblNewLabel_4.setBounds(567, 152, 55, 17);
			panel.add(lblNewLabel_4);

			JButton btHuy = new JButton("Đóng");
			btHuy.setIcon(new ImageIcon("img\\cancel3.png"));
			btHuy.setForeground(Color.RED);
			btHuy.setFont(new Font("Times New Roman", Font.BOLD, 15));
			btHuy.setBounds(404, 640, 123, 50);
			btHuy.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					setVisible(false);
					new fThongKeNhanVien().setVisible(true);

				}
			});
			panel.add(btHuy);
		}
	}

	public int countEmployee(int minluong, int maxluong, int month) {
		int count = 0;
		for (LuongChiTietDTO luongDto : listLuong) {
			if (luongDto.getThucLanh() > minluong && luongDto.getThucLanh() < maxluong && luongDto.getThang()==month) {
				count++;
			}
		}
		return count;
	}
}
