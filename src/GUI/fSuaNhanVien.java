package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import BUS.ChucVuBUS;
import BUS.HopDongLaoDongBUS;
import BUS.LuongBUS;
import BUS.LuongChiTietBUS;
import BUS.NhanVienBUS;
import BUS.PhongBanBUS;
import BUS.PhuCapBUS;
import BUS.ThuongBUS;
import DTO.HopDongLaoDongDTO;
import DTO.PhongBanDTO;
import DTO.NhanVienDTO;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class fSuaNhanVien extends JFrame {

	private JPanel contentPane;
	private JPanel panel;
	private JTextField txtFirstName;
	private JTextField txtLastName;
	private JTextField txtEmail;
	private JTextField txtId;
	private JTextField txtPhoneNumber;
	private JTextField txtIdCard;
	private JTable table;
	private JTable table_1;
	private JTable table_2;
	private JLabel lbCancel;
	private JLabel lbOK;
	private JTextField txtNation;
	private JTextField txtAddress;
	private JTextField txtAccount;
	private JButton btTakeImg;
	private JLabel lbImg;
	private String linkAnh;
	private JRadioButton rbtMale;
	private JRadioButton rbtFemale;
	@SuppressWarnings("rawtypes")
	private JComboBox cbDay;
	@SuppressWarnings("rawtypes")
	private JComboBox cbMonth;
	@SuppressWarnings("rawtypes")
	private JComboBox cbYear;
	@SuppressWarnings("rawtypes")
	private JComboBox cbPositionName;
	@SuppressWarnings("rawtypes")
	private JComboBox cbDepartmentName;
	@SuppressWarnings("rawtypes")
	private JComboBox cbContractName;
	private HopDongLaoDongBUS hopdongBus = new HopDongLaoDongBUS();
	private ChucVuBUS chucvuBus = new ChucVuBUS();
	private PhongBanBUS phongbanBus = new PhongBanBUS();// create object
	private NhanVienBUS nhanvienBus = new NhanVienBUS();
	private LuongChiTietBUS luongchitietBus = new LuongChiTietBUS();
	private LuongBUS luongBus = new LuongBUS();
	private PhuCapBUS phucapBus = new PhuCapBUS();
	private ThuongBUS thuongBus = new ThuongBUS();
	private JLabel lbTonGiao;
	private JTextField txtTonGiao;
	private JComboBox cbHeSo;
	private JComboBox cbLuong;
	private JComboBox cbNgayKT;
	private JComboBox cbThangKT;
	private JComboBox cbNamKT;
	private NhanVienDTO nhanvien = fQuanLyNhanSu.getNhanvienHienHanh();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					fSuaNhanVien frame = new fSuaNhanVien();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public fSuaNhanVien() {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Windows".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, you can set the GUI to another look
			// and feel.
		}
		setExtendedState(JFrame.MAXIMIZED_BOTH);// set full màn hình
		setTitle("Sửa Nhân Viên");
		setIconImage(Toolkit.getDefaultToolkit().getImage("img\\officer.png"));
		setBounds(100, 100, 1366, 768);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setForeground(Color.BLACK);
		panel.setBounds(149, 11, 1055, 730);
		panel.setLayout(null);

		JLabel lbTitle = new JLabel("Sửa Thông Tin Nhân Viên");
		lbTitle.setForeground(Color.BLACK);
		lbTitle.setFont(new Font("SansSerif", Font.BOLD, 30));
		lbTitle.setBounds(377, 35, 397, 57);
		panel.add(lbTitle);

		JLabel lbFirstName = new JLabel("Họ:");
		lbFirstName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbFirstName.setBounds(200, 245, 30, 20);
		panel.add(lbFirstName);

		txtFirstName = new JTextField();
		txtFirstName.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtFirstName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtFirstName.setBounds(250, 245, 97, 23);
		panel.add(txtFirstName);
		txtFirstName.setColumns(10);

		JLabel lbLastName = new JLabel("Tên:");
		lbLastName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbLastName.setBounds(377, 245, 46, 20);
		panel.add(lbLastName);

		txtLastName = new JTextField();
		txtLastName.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtLastName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtLastName.setBounds(443, 245, 121, 23);
		panel.add(txtLastName);
		txtLastName.setColumns(10);

		ButtonGroup bg = new ButtonGroup();

		rbtMale = new JRadioButton("Nam");
		bg.add(rbtMale);
		rbtMale.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rbtMale.setBounds(743, 207, 52, 23);
		panel.add(rbtMale);

		rbtFemale = new JRadioButton("Nữ");
		bg.add(rbtFemale);
		rbtFemale.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rbtFemale.setBounds(814, 207, 41, 23);
		panel.add(rbtFemale);

		JLabel lbId = new JLabel("Mã Nhân Viên:");
		lbId.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbId.setBounds(200, 207, 112, 20);
		panel.add(lbId);

		txtEmail = new JTextField();
		txtEmail.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtEmail.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtEmail.setBounds(349, 284, 215, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);

		JLabel lbNation = new JLabel("Dân tộc:");
		lbNation.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbNation.setBounds(635, 245, 61, 20);
		panel.add(lbNation);

		JLabel lbEmail = new JLabel("Email:");
		lbEmail.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbEmail.setBounds(200, 283, 46, 20);
		panel.add(lbEmail);

		JLabel lbPhoneNumber = new JLabel("Số Điện Thoại:");
		lbPhoneNumber.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbPhoneNumber.setBounds(200, 324, 105, 20);
		panel.add(lbPhoneNumber);

		JLabel lbIdCard = new JLabel("Số CMND:");
		lbIdCard.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbIdCard.setBounds(200, 363, 78, 20);
		panel.add(lbIdCard);

		JLabel lbDepartmentName = new JLabel("Tên Phòng Ban:");
		lbDepartmentName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbDepartmentName.setBounds(200, 442, 121, 20);
		panel.add(lbDepartmentName);

		JLabel lbPositionName = new JLabel("Tên Chức Vụ: ");
		lbPositionName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbPositionName.setBounds(635, 442, 105, 20);
		panel.add(lbPositionName);

		JLabel lbContractName = new JLabel("Tên HĐLĐ:");
		lbContractName.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbContractName.setBounds(635, 363, 78, 20);
		panel.add(lbContractName);

		txtId = new JTextField();
		txtId.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtId.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtId.setBounds(349, 209, 215, 20);
		panel.add(txtId);
		txtId.setColumns(10);

		txtPhoneNumber = new JTextField();
		txtPhoneNumber.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtPhoneNumber.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtPhoneNumber.setBounds(349, 324, 215, 20);
		panel.add(txtPhoneNumber);
		txtPhoneNumber.setColumns(10);

		txtIdCard = new JTextField();
		txtIdCard.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtIdCard.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtIdCard.setBounds(349, 363, 215, 20);
		panel.add(txtIdCard);
		txtIdCard.setColumns(10);

		JLabel lbAddress = new JLabel("Địa chỉ:");
		lbAddress.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbAddress.setBounds(635, 285, 78, 20);
		panel.add(lbAddress);

		cbPositionName = new JComboBox();
		cbPositionName.setFont(new Font("Tahoma", Font.BOLD, 13));
		ArrayList<String> listPositionName = chucvuBus.getListNameByCond("tencv", "", "");
		Vector<String> namePosition = new Vector<>();
		if (!listPositionName.isEmpty()) {
			for (String st : listPositionName) {
				namePosition.add(st);
			}
		}
		cbPositionName.setModel(new DefaultComboBoxModel(namePosition));
		cbPositionName.setSelectedIndex(4);
		cbPositionName.setBounds(743, 442, 215, 21);
		panel.add(cbPositionName);

		cbDepartmentName = new JComboBox();
		cbDepartmentName.setFont(new Font("Tahoma", Font.BOLD, 13));
		ArrayList<String> listDepartName = phongbanBus.getListOneCol("tenphong", "", "");// get
																							// name
																							// department
																							// list
		Vector<String> name = new Vector<>();
		if (!listDepartName.isEmpty()) {
			for (String st : listDepartName) {
				name.add(st);
			}
		}
		cbDepartmentName.setModel(new DefaultComboBoxModel(name));
		cbDepartmentName.setSelectedIndex(4);
		cbDepartmentName.setBounds(349, 442, 215, 21);
		panel.add(cbDepartmentName);

		JLabel lbSex = new JLabel("Giới Tính:");
		lbSex.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbSex.setBounds(635, 207, 82, 20);
		panel.add(lbSex);

		table = new JTable();
		table.setBounds(1340, 699, -1043, -138);
		panel.add(table);

		getContentPane().add(panel);

		lbCancel = new JLabel("Hủy");
		lbCancel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lbCancel.setIcon(new ImageIcon("img\\cancel.png"));
		lbCancel.setBounds(377, 600, 136, 72);
		panel.add(lbCancel);

		lbOK = new JLabel("Đồng Ý");
		lbOK.setFont(new Font("Tahoma", Font.BOLD, 11));
		lbOK.setIcon(new ImageIcon("img\\accept.png"));
		lbOK.setBounds(743, 589, 121, 72);
		panel.add(lbOK);

		lbImg = new JLabel("");
		lbImg.setBorder(new LineBorder(Color.LIGHT_GRAY));
		linkAnh = fQuanLyNhanSu.getNhanvienHienHanh().getHinhAnh();
		lbImg.setIcon(new ImageIcon(linkAnh));
		lbImg.setBounds(21, 74, 121, 123);
		panel.add(lbImg);

		btTakeImg = new JButton("Đổi ảnh");
		btTakeImg.setBounds(38, 208, 89, 23);
		panel.add(btTakeImg);

		txtNation = new JTextField();
		txtNation.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtNation.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtNation.setBounds(743, 247, 215, 20);
		panel.add(txtNation);
		txtNation.setColumns(10);

		txtAddress = new JTextField();
		txtAddress.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAddress.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtAddress.setBounds(743, 285, 215, 20);
		panel.add(txtAddress);
		txtAddress.setColumns(10);

		JLabel lbBirthday = new JLabel("Ngày Sinh: ");
		lbBirthday.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbBirthday.setBounds(200, 403, 136, 23);
		panel.add(lbBirthday);

		cbDay = new JComboBox();
		cbDay.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbDay.setModel(new DefaultComboBoxModel(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09",
				"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26",
				"27", "28", "29", "30", "31" }));
		cbDay.setBounds(349, 406, 41, 20);
		panel.add(cbDay);

		JLabel lblNewLabel_2 = new JLabel("/");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(400, 403, 30, 28);
		panel.add(lblNewLabel_2);

		cbMonth = new JComboBox();
		cbMonth.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbMonth.setModel(new DefaultComboBoxModel(
				new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));
		cbMonth.setBounds(429, 406, 46, 20);
		panel.add(cbMonth);

		JLabel label = new JLabel("/");
		label.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(480, 403, 23, 28);
		panel.add(label);

		JLabel lbAccount = new JLabel("Số Tài Khoản:");
		lbAccount.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbAccount.setBounds(635, 324, 100, 14);
		panel.add(lbAccount);

		txtAccount = new JTextField();
		txtAccount.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtAccount.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtAccount.setBounds(743, 324, 215, 20);
		panel.add(txtAccount);
		txtAccount.setColumns(10);

		cbYear = new JComboBox();
		cbYear.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbYear.setModel(new DefaultComboBoxModel(
				new String[] { "1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989", "1990",
						"1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000" }));
		cbYear.setBounds(503, 406, 61, 20);
		panel.add(cbYear);

		cbContractName = new JComboBox();
		cbContractName.setFont(new Font("Tahoma", Font.BOLD, 13));
		ArrayList<String> listContractName = hopdongBus.getList("loaihopdong");
		Vector<String> nameList = new Vector<>();
		if (!listContractName.isEmpty()) {
			for (String st : listContractName) {
				nameList.add(st);
			}
		}
		cbContractName.setModel(new DefaultComboBoxModel<>(nameList));
		cbContractName.setBounds(743, 363, 215, 20);
		panel.add(cbContractName);

		JLabel lbNgayKT = new JLabel("Ngày KT: ");
		lbNgayKT.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbNgayKT.setBounds(635, 406, 97, 20);
		panel.add(lbNgayKT);

		cbNgayKT = new JComboBox();
		cbNgayKT.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbNgayKT.setModel(new DefaultComboBoxModel(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09",
				"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26",
				"27", "28", "29", "30", "31" }));
		cbNgayKT.setBounds(743, 406, 41, 20);
		panel.add(cbNgayKT);

		JLabel label_1 = new JLabel("/");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label_1.setBounds(785, 406, 23, 20);
		panel.add(label_1);

		cbThangKT = new JComboBox();
		cbThangKT.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbThangKT.setModel(new DefaultComboBoxModel(
				new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));
		cbThangKT.setBounds(809, 406, 46, 20);
		panel.add(cbThangKT);

		cbNamKT = new JComboBox();
		cbNamKT.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cbNamKT.setModel(new DefaultComboBoxModel(new String[] { "2016", "2017", "2018", "2019", "2020", "2021", "2022",
				"2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030" }));
		cbNamKT.setBounds(897, 406, 61, 20);
		panel.add(cbNamKT);

		JLabel label_2 = new JLabel("/");
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		label_2.setBounds(857, 406, 30, 20);
		panel.add(label_2);

		lbTonGiao = new JLabel("Tôn giáo:");
		lbTonGiao.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbTonGiao.setBounds(200, 485, 121, 20);
		panel.add(lbTonGiao);

		txtTonGiao = new JTextField();
		txtTonGiao.setFont(new Font("Tahoma", Font.PLAIN, 14));
		txtTonGiao.setColumns(10);
		txtTonGiao.setBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		txtTonGiao.setBounds(349, 485, 215, 20);
		panel.add(txtTonGiao);

		JLabel lbLuong = new JLabel("Lương cơ bản:");
		lbLuong.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbLuong.setBounds(635, 485, 112, 20);
		panel.add(lbLuong);

		ArrayList<String> listLuong = new ArrayList<>();
		listLuong = luongBus.getOneCol("luongcoban");
		Vector<String> vtLuong = new Vector<>();
		if (listLuong != null) {
			for (String st : listLuong) {
				vtLuong.add(st);
			}
		}
		cbLuong = new JComboBox(vtLuong);
		cbLuong.setFont(new Font("Tahoma", Font.BOLD, 12));
		cbLuong.setBounds(743, 485, 215, 20);
		panel.add(cbLuong);

		JLabel lbHeSo = new JLabel("Hệ số PC:");
		lbHeSo.setFont(new Font("SansSerif", Font.BOLD, 15));
		lbHeSo.setBounds(200, 530, 112, 20);
		panel.add(lbHeSo);

		ArrayList<String> listPhuCap = new ArrayList<>();
		listPhuCap = phucapBus.getOneCol("hesophucap");
		Vector<String> vtPhucap = new Vector<>();
		if (listPhuCap != null) {
			for (String st : listPhuCap) {
				vtPhucap.add(st);
			}
		}
		cbHeSo = new JComboBox(vtPhucap);
		cbHeSo.setFont(new Font("Tahoma", Font.BOLD, 12));
		cbHeSo.setBounds(349, 530, 78, 20);
		panel.add(cbHeSo);

		ArrayList<String> listThuong = new ArrayList<>();
		listThuong = thuongBus.getOneCol("tenloaithuong");
		Vector<String> vtThuong = new Vector<>();
		if (listThuong != null) {
			for (String st : listThuong) {
				vtThuong.add(st);
			}
		}
		loadData();
		clickCancel();
		clickChangeImg();
		clickOK();

	}

	public void loadData() {
		if (nhanvien != null) {
			txtId.setText(nhanvien.getMaNV());
			txtId.setEnabled(false);
			txtAccount.setText(nhanvien.getSoTaiKhoan());
			txtAddress.setText(nhanvien.getDiaChi());
			txtEmail.setText(nhanvien.getEmail());
			txtFirstName.setText(nhanvien.getHo());
			txtLastName.setText(nhanvien.getTen());
			txtNation.setText(nhanvien.getDanToc());
			txtPhoneNumber.setText(nhanvien.getSoDT());
			txtIdCard.setText(nhanvien.getSoCMND());
			txtTonGiao.setText(nhanvien.getTonGiao());
			lbImg.setIcon(new ImageIcon("img\\imgnhansu\\" + nhanvien.getHinhAnh()));
			if (nhanvien.getGioiTinh().equals("Nam")) {
				rbtMale.setSelected(true);
			} else {
				rbtFemale.setSelected(true);
			}
			cbYear.setSelectedItem(nhanvien.getNgaySinh().toString().substring(0, 4));
			cbMonth.setSelectedItem(nhanvien.getNgaySinh().toString().substring(5, 7));
			cbDay.setSelectedItem(nhanvien.getNgaySinh().toString().substring(8));
			cbContractName.setSelectedItem(fQuanLyNhanSu.getHopdongDto().getLoaiHopDong());
			cbDepartmentName.setSelectedItem(fQuanLyNhanSu.getPhongbanDto().getTenPhong());
			cbPositionName.setSelectedItem(fQuanLyNhanSu.getChucvuDto().getTenChucVu());
			cbHeSo.setSelectedItem(phucapBus.getHeSo(nhanvien.getMaPhuCap()));
			cbNamKT.setSelectedItem(fQuanLyNhanSu.getHopdongDto().getNgayKetThuc().toString().substring(0, 4));
			cbThangKT.setSelectedItem(fQuanLyNhanSu.getHopdongDto().getNgayKetThuc().toString().substring(5, 7));
			cbNgayKT.setSelectedItem(fQuanLyNhanSu.getHopdongDto().getNgayKetThuc().toString().substring(8));
			cbLuong.setSelectedItem(luongBus.getLuongCoBan(nhanvien.getMaBacLuong()));
		}

	}

	public void clickOK() {
		lbOK.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {
				lbOK.setForeground(null);

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				lbOK.setForeground(Color.green);

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				int respone = JOptionPane.showConfirmDialog(null, "Bạn có chắc chắn muốn lưu thông tin?", "Confirm",
						JOptionPane.YES_NO_OPTION);
				if (respone == JOptionPane.YES_OPTION) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					StringBuilder ngayKt = new StringBuilder();
					ngayKt.append(cbNamKT.getSelectedItem()).append("-").append(cbThangKT.getSelectedItem()).append("-")
							.append(cbNgayKT.getSelectedItem());

					Date ngayKT = null;
					ngayKT = Date.valueOf(ngayKt.toString());
					// lấy đối tượng cũ
					HopDongLaoDongDTO hopdong = hopdongBus.getHopDongLaoDong(nhanvien.getMahdld());
					// tạo đối tượng mơi update
					HopDongLaoDongDTO contract = new HopDongLaoDongDTO(nhanvien.getMahdld(),
							(String) cbContractName.getSelectedItem(), hopdong.getNgayKy(), hopdong.getNgayBatDau(),
							(java.sql.Date) ngayKT);

					@SuppressWarnings("unused")
					boolean flag_1 = hopdongBus.update(contract);// update
																	// hopdonglaodong
					if(flag_1==false){
						JOptionPane.showMessageDialog(null, "Update hop dong thất bại!");
						return;
					}
					String birthday = (String) cbYear.getSelectedItem() + "-" + (String) cbMonth.getSelectedItem() + "-"
							+ (String) cbDay.getSelectedItem();
					Date ngaySinh = null;
					ngaySinh = Date.valueOf(birthday);
					// lấy mã phòng ban mới
					String idDepart = phongbanBus.getIdDepartment((String) cbDepartmentName.getSelectedItem());
					// lấy mã chức vụ mới
					String idPosition = chucvuBus.getIdPositionByName((String) cbPositionName.getSelectedItem());
					if (idDepart.equals("P0") && idPosition.equals("TP")) {// nếu
																			// là
																			// phòng
																			// giám
																			// đốc
																			// và
																			// là
																			// trưởng
																			// phòng
																			// thì
																			// mã
																			// chức
																			// vụ
																			// là
																			// CEO
						idPosition = "CEO";
					}
					if (idPosition.equals("CEO") || idPosition.equals("TP")) {
						// lấy
						String address = phongbanBus.getValue(idDepart, "diachi");
						String oldIdBoss = phongbanBus.getValue(idDepart, "truongphong");
						// tao đối tượng phòng ban trường hợp đổi trưởng phòng
						PhongBanDTO phongbanDto = new PhongBanDTO(idDepart, (String) cbDepartmentName.getSelectedItem(),
								getTxtId().getText(), address);
						@SuppressWarnings("unused")
						boolean flag_2 = phongbanBus.update(phongbanDto);// update
						if(flag_2==false){
							JOptionPane.showMessageDialog(null, "Update phòng ban thất bại!");
							return;
						}
						// đổi chức vụ hai thằng
						@SuppressWarnings("unused")
						boolean flag_3 = nhanvienBus.updateOneCol("macv", nhanvien.getMacv(), oldIdBoss);
					}
					NhanVienDTO newStaff = new NhanVienDTO(txtId.getText(), txtFirstName.getText(),
							txtLastName.getText(), rbtFemale.isSelected() ? "Nữ" : "Nam",  ngaySinh,
							txtAddress.getText(), txtNation.getText(), txtIdCard.getText(), txtPhoneNumber.getText(),
							txtEmail.getText(), txtAccount.getText(), linkAnh, nhanvien.getMahdld(), idPosition,
							idDepart, nhanvien.getMaTDHV(), txtTonGiao.getText(),
							luongBus.getOneCol(Integer.parseInt((String) cbLuong.getSelectedItem())),
							phucapBus.getId(Double.parseDouble((String) cbHeSo.getSelectedItem())));
					if (nhanvienBus.update(newStaff)) {
						JOptionPane.showMessageDialog(null, "Sửa thành công!!!");
					} else {
						JOptionPane.showMessageDialog(null, "Sửa thất bại!!!");
					}

				}

			}

		});

	}

	public void clickCancel() {
		lbCancel.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				lbCancel.setForeground(null);

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				lbCancel.setForeground(Color.red);

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				setVisible(false);
				new fQuanLyNhanSu().setVisible(true);

			}
		});
	}

	public void clickChangeImg() {
		btTakeImg.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				ArrayList<String> linkList = nhanvienBus.getListOneCol("hinhanh");
				boolean flag;
				do {
					flag = true;
					JFileChooser fileChooser = new JFileChooser();
					int returnValue = fileChooser.showOpenDialog(contentPane);
					if (returnValue == JFileChooser.APPROVE_OPTION) {
						File selectedFile = fileChooser.getSelectedFile();
						linkAnh = selectedFile.getName().toString();
						for (String st : linkList) {
							if (st.equals(linkAnh)) {
								JOptionPane.showMessageDialog(null,
										"Ảnh này đã bị trùng với nhân viên khác!! Vui lòng chọn lại ảnh.");
								flag = false;
								break;
							}
						}
					}
				} while (!flag);
				lbImg.setIcon(new ImageIcon("img\\imgnhansu\\" + linkAnh));

			}
		});
	}

	public JPanel getPanel() {
		return panel;
	}

	public void setPanel(JPanel panel) {
		this.panel = panel;
	}

	public JTextField getTxtFirstName() {
		return txtFirstName;
	}

	public void setTxtFirstName(JTextField txtFirstName) {
		this.txtFirstName = txtFirstName;
	}

	public JTextField getTxtLastName() {
		return txtLastName;
	}

	public void setTxtLastName(JTextField txtLastName) {
		this.txtLastName = txtLastName;
	}

	public JTextField getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}

	public JTextField getTxtId() {
		return txtId;
	}

	public void setTxtId(JTextField txtId) {
		this.txtId = txtId;
	}

	public JTextField getTxtPhoneNumber() {
		return txtPhoneNumber;
	}

	public void setTxtPhoneNumber(JTextField txtPhoneNumber) {
		this.txtPhoneNumber = txtPhoneNumber;
	}

	public JTextField getTxtIdCard() {
		return txtIdCard;
	}

	public void setTxtIdCard(JTextField txtIdCard) {
		this.txtIdCard = txtIdCard;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JTable getTable_1() {
		return table_1;
	}

	public void setTable_1(JTable table_1) {
		this.table_1 = table_1;
	}

	public JTable getTable_2() {
		return table_2;
	}

	public void setTable_2(JTable table_2) {
		this.table_2 = table_2;
	}

	public JLabel getLbCancel() {
		return lbCancel;
	}

	public void setLbCancel(JLabel lbCancel) {
		this.lbCancel = lbCancel;
	}

	public JLabel getLbOK() {
		return lbOK;
	}

	public void setLbOK(JLabel lbOk) {
		this.lbOK = lbOk;
	}

	public JTextField getTxtNation() {
		return txtNation;
	}

	public void setTxtNation(JTextField txtNation) {
		this.txtNation = txtNation;
	}

	public JTextField getTxtAddress() {
		return txtAddress;
	}

	public void setTxtAddress(JTextField txtAddress) {
		this.txtAddress = txtAddress;
	}

	public JTextField getTxtAccount() {
		return txtAccount;
	}

	public void setTxtAccount(JTextField txtAccount) {
		this.txtAccount = txtAccount;
	}

	public JButton getBtTakeImg() {
		return btTakeImg;
	}

	public void setBtTakeImg(JButton btTakeImg) {
		this.btTakeImg = btTakeImg;
	}

	public JLabel getLbImg() {
		return lbImg;
	}

	public void setLbImg(JLabel lbImg) {
		this.lbImg = lbImg;
	}

	public String getLinkAnh() {
		return linkAnh;
	}

	public void setLinkAnh(String linkAnh) {
		this.linkAnh = linkAnh;
	}

	public JRadioButton getRbtMale() {
		return rbtMale;
	}

	public void setRbtMale(JRadioButton rbtMale) {
		this.rbtMale = rbtMale;
	}

	public JRadioButton getRbtFemale() {
		return rbtFemale;
	}

	public void setRbtFemale(JRadioButton rbtFemale) {
		this.rbtFemale = rbtFemale;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getCbDay() {
		return cbDay;
	}

	@SuppressWarnings("rawtypes")
	public void setCbDay(JComboBox cbDay) {
		this.cbDay = cbDay;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getCbMonth() {
		return cbMonth;
	}

	@SuppressWarnings("rawtypes")
	public void setCbMonth(JComboBox cbMonth) {
		this.cbMonth = cbMonth;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getCbYear() {
		return cbYear;
	}

	@SuppressWarnings("rawtypes")
	public void setCbYear(JComboBox cbYear) {
		this.cbYear = cbYear;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getCbPositionName() {
		return cbPositionName;
	}

	public void setCbPositionName(@SuppressWarnings("rawtypes") JComboBox cbPositionName) {
		this.cbPositionName = cbPositionName;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getCbDepartmentName() {
		return cbDepartmentName;
	}

	public void setCbDepartmentName(@SuppressWarnings("rawtypes") JComboBox cbDepartmentName) {
		this.cbDepartmentName = cbDepartmentName;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getCbContractName() {
		return cbContractName;
	}

	@SuppressWarnings("rawtypes")
	public void setCbContractName(JComboBox cbContractName) {
		this.cbContractName = cbContractName;
	}
}
