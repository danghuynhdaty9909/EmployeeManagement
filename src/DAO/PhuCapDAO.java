package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import DTO.PhuCapDTO;

public class PhuCapDAO {
	DataConnection dc = new DataConnection();
	public ArrayList<String> getOneCol(String col){//lấy danh sách một cột
		ArrayList<String> list=new ArrayList<>();
		String query="Select distinct "+ col+" from quanlynhansu.phucap";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			while (result.next()) {
				list.add(result.getString(1));
			}
			dc.Disconnect();
			return list;
		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}
	}
	//lấy id
	public String getId(double heso){
		String query="select maphucap from quanlynhansu.phucap where hesophucap="+heso;
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {
				return result.getString(1);
			}
			dc.Disconnect();
			return "";
		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}
		
	}
	public String getHeSo(String id){
		String query="select hesophucap from quanlynhansu.phucap where maphucap='"+id+"'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {
				return result.getString(1);
			}
			dc.Disconnect();
			return "";
		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}
		
	}
	
	
	// lấy tất cả danh sách phu cap
	public ArrayList<PhuCapDTO> getAllPhuCap()
	{
		ArrayList<PhuCapDTO> arr = new ArrayList<PhuCapDTO>();
		dc.Connect();
		String sql = "select * from phucap";
		try {
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			PhuCapDTO item = new PhuCapDTO();
			while(rs!=null && rs.next())
			{
				item = new PhuCapDTO(rs.getString(1), rs.getDouble(2));
				arr.add(item);
			}
			dc.Disconnect();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return arr;
	}
	
}
