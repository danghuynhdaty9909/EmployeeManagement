package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;

import DTO.ChamCongDTO;

public class ChamCongDAO {
	DataConnection dc = new DataConnection();
	public int[] getSoGioLam(String maChamCong)
	{
		int[] soGioLam =new int[2];
		soGioLam[0]=0;
		soGioLam[1]=0;
		dc.Connect();
		String sql = "select * from chamcong where machamcong='"+maChamCong+"'";
		try {
			
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			String str = "";
			String[] res;
			if(rs!=null && rs.next()){
				for(int i = 2;i<=32;i++)
				{
					str = rs.getString(i);
					res = str.split("-");//cắt chuỗi lấy ra số giờ làm
					soGioLam[0]+= Integer.parseInt(res[0]);//cast để lấy số giờ làm bình thường
					soGioLam[1]+= Integer.parseInt(res[1]);//cast để lấy số giờ làm tăng ca
				}
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return soGioLam;
	}
	
	
	//insert ma cham cong
	public boolean ChamCongNhanVien(ChamCongDTO chamCong)
	{
		dc.Connect();
		boolean flag = false;
		String sql = "INSERT INTO `quanlynhansu`.`chamcong` (`machamcong`, `ngay1`, `ngay2`, `ngay3`, `ngay4`, `ngay5`, `ngay6`, `ngay7`, `ngay8`, `ngay9`, `ngay10`, `ngay11`, `ngay12`, `ngay13`, `ngay14`, `ngay15`, `ngay16`, `ngay17`, `ngay18`, `ngay19`, `ngay20`, `ngay21`, `ngay22`, `ngay23`, `ngay24`, `ngay25`, `ngay26`, `ngay27`, `ngay28`, `ngay29`, `ngay30`, `ngay31`) VALUES ('"+chamCong.getMaChamCong()+"' ";
		for(int i = 0; i< chamCong.getNgay().length;i++)
		{
			sql+=",'"+chamCong.getNgay()[i]+"'";
		}
		sql+=");";
		try {
			int row = dc.con.createStatement().executeUpdate(sql);
			if(row>0)
			{
				flag = true;
			}
			dc.Disconnect();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}
	
	
	
	public static void main(String[] args) {
		for(int i = 3;i<=31;i++)
		{
			int num = i -1;
			String r = "String n"+num+" = ckb"+i+".isSelected() ? \"8-\"+cbb"+i+".getSelectedItem() : \"0-0\"; \n ngay["+num+"] = n"+num+";";
			System.out.println(r);
		}
	}
	
}
