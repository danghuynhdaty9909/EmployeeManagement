package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import DTO.LuongDTO;

public class LuongDAO {
	DataConnection dc = new DataConnection();

	public ArrayList<String> getOneCol(String col) {// lấy danh sách một cột
		ArrayList<String> list = new ArrayList<>();
		String query = "Select distinct " + col + " from quanlynhansu.luong";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			while (result.next()) {
				list.add(result.getString(1));
			}
			dc.Disconnect();
			return list;
		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}
	}

	public ArrayList<LuongDTO> getAllLuong() {
		ArrayList<LuongDTO> arr = new ArrayList<LuongDTO>();

		try {

			dc.Connect();
			String sql = "select * from luong";
			LuongDTO luong;
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			while (rs.next() && rs != null) {
				luong = new LuongDTO(rs.getString(1), rs.getInt(2));
				arr.add(luong);
			}
			dc.Disconnect();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return arr;
	}
	//lay mot mã lương cua doi tuong cu the
	public String getOneCol(int value){
		String query="select mabacluong from quanlynhansu.luong where luongcoban="+value;
		dc.Connect();
		try {
			ResultSet result=dc.con.createStatement().executeQuery(query);
			if(result.next()){
				return result.getString(1);
			}
			dc.Disconnect();
			return "";
		} catch (SQLException e) {
			dc.Disconnect();
			return "";
		}
	}
	public String getLuongCoBan(String id){
		String query="select luongcoban  from quanlynhansu.luong where mabacluong="+id;
		dc.Connect();
		try {
			ResultSet result=dc.con.createStatement().executeQuery(query);
			if(result.next()){
				return result.getString(1);
			}
			dc.Disconnect();
			return "";
		} catch (SQLException e) {
			dc.Disconnect();
			return "";
		}
	}
	
	public static void main(String[] args) {
		LuongDAO dao = new LuongDAO();
		ArrayList<LuongDTO> arr = dao.getAllLuong();
		for(LuongDTO item: arr)
		{
			System.out.println(item.getLuongCoBan());
			System.out.println(item.getMaBacLuong());
		}
	}
	
}
