package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import DTO.ThuongDTO;

public class ThuongDAO {
	DataConnection dc = new DataConnection();
	public ArrayList<String> getOneCol(String col){//lấy danh sách một cột
		ArrayList<String> list=new ArrayList<>();
		String query="Select distinct "+ col+" from quanlynhansu.thuong";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			while (result.next()) {
				list.add(result.getString(1));
			}
			dc.Disconnect();
			return list;
		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}
	}
	
	
	//lấy tất cả danh sách thưởng
	public ArrayList<ThuongDTO> getAllThuong()
	{
		dc.Connect();
		ArrayList<ThuongDTO> arr = new ArrayList<ThuongDTO>();
		try {
			
			String sql = "select * from thuong";
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			ThuongDTO item;
			while(rs!=null && rs.next())
			{
				item = new ThuongDTO(rs.getString(1), rs.getString(2), rs.getInt(3));
				arr.add(item);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return arr;
	}
	
	public static void main(String[] args) {
		ThuongDAO dao = new ThuongDAO();
		 ArrayList<ThuongDTO> arr = dao.getAllThuong();
		 for(ThuongDTO item : arr)
		 {
			 System.out.println(item.getMaThuong()+" "+item.getTenLoaiThuong()+" "+item.getMucThuong());
		 }
	}
	
}
