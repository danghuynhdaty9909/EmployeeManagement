package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import DTO.PhongBanDTO;

public class PhongBanDAO {
	DataConnection dc = new DataConnection();

	// lấy đối tượng phòng ban theo ma phong
	public PhongBanDTO getPhongBan(String id) {
		String query = "select * from quanlynhansu.phongban where maphongban='" + id + "'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			while (result.next()) {
				return new PhongBanDTO(result.getString(1), result.getString(2), result.getString(3),
						result.getString(4));
			}
			return null;
		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}
	}

	public ArrayList<String> getListOneCol(String col, String cond, String value) {// lấy
		// danh
		// sách
		// một
		// cột
		ArrayList<String> list = new ArrayList<>();
		String query = "select " + col + " from quanlynhansu.phongban";
		if (!cond.isEmpty()) {
			query += " where " + cond + "='" + value + "'";
		}
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			while (result.next()) {
				list.add(result.getString(1));
			}
			dc.Disconnect();
			return list;
		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}

	}

	// lấy tất cả phòng ban
	public ArrayList<PhongBanDTO> getAllPhongBan() {
		ArrayList<PhongBanDTO> result = new ArrayList<PhongBanDTO>();
		String sql = "select * from phongban";
		dc.Connect();
		try {
			PhongBanDTO phongBanDTO;
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			while (rs.next() && rs != null) {
				phongBanDTO = new PhongBanDTO(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
				result.add(phongBanDTO);
			}
			dc.Disconnect();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	// lay ma phong bang ten phong
	public String getIdDepartment(String name) {
		String query = "select maphongban from quanlynhansu.phongban where tenphong='" + name + "'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {

				return result.getString(1);
			}
		} catch (SQLException e) {
			return "";
		}
		return "";
	}

	// lấy giá trị một thuộc tính
	public String getValue(String id, String col) {
		String query = "select " + col + " from quanlynhansu.phongban where  maphongban='" + id + "'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {

				return result.getString(1);
			}
		} catch (SQLException e) {
			return "";
		}
		return "";
	}

	public boolean update(PhongBanDTO phongban) {// sửa
		// phòng
		// ban
		// theo
		// id
		// phòng
		// ban
		String query = "update quanlynhansu.phongban set tenphong='" + phongban.getTenPhong() + "',diachi='"
				+ phongban.getDiaChi() + "',truongphong='" + phongban.getTruongPhong() + "' where maphongban='"
				+ phongban.getMaPhongBan() + "'";
		dc.Connect();
		try {
			int k = dc.con.createStatement().executeUpdate(query);
			if (k >= 1) {
				return true;
			}
			return true;
		} catch (SQLException e) {
			return false;
		}
	}
	public boolean check(String tenphong,String diachi){
		String query="select * from quanlynhansu.phongban where tenphong='"+tenphong+"' and diachi='"+diachi+"'";
		dc.Connect();
		try {
			ResultSet result=dc.con.createStatement().executeQuery(query);
			return result.next()?false:true;
		} catch (SQLException e) {
			dc.Disconnect();
			return false;
		}
		
	}
	public boolean insert(PhongBanDTO phongban) {
		StringBuilder query = new StringBuilder();
		query.append("insert into quanlynhansu.phongban values('");
		query.append(phongban.getMaPhongBan() + "','" + phongban.getTenPhong() + "','" + phongban.getTruongPhong() + "','"
				+ phongban.getDiaChi() + "')");
		dc.Connect();
		try {
			int k = dc.con.createStatement().executeUpdate(query.toString());
			return k >= 1 ? true : false;
		} catch (SQLException e) {
			return false;
		}
	}
}
