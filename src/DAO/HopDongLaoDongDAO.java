package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import DTO.HopDongLaoDongDTO;

public class HopDongLaoDongDAO {
	DataConnection dc = new DataConnection();

	public HopDongLaoDongDTO getHopDongLaoDong(String id) {
		String query = "select * from quanlynhansu.hopdonglaodong where mahdld='" + id + "'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			while (result.next()) {
				return new HopDongLaoDongDTO(result.getString(1), result.getString(2), result.getDate(3),
						result.getDate(4), result.getDate(5));
			}
			return null;
		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}
	}

	// xoa hop dong lao dong
	public boolean delete(String idContract) {
		String query = "delete from  `quanlynhansu`.`hopdonglaodong` where `mahdld`='" + idContract + "'";
		dc.Connect();
		try {
			@SuppressWarnings("unused")
			int k = dc.con.createStatement().executeUpdate(query);
			dc.Disconnect();
			return true;
		} catch (SQLException e) {
			dc.Disconnect();
			return false;
		}
	}

	// lấy list điều kiện
	public ArrayList<String> getList(String col) {
		ArrayList<String> list = new ArrayList<>();
		String query = "Select distinct " + col + " from quanlynhansu.hopdonglaodong";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			while (result.next()) {
				list.add(result.getString(1));
			}
			dc.Disconnect();

			return list;
		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}
	}

	public String createNewId() {
		int max = 0;
		ArrayList<String> list = this.getList("mahdld");
		if (!list.isEmpty()) {
			for (String st : list) {
				if (max < Integer.parseInt(st.substring(2))) {
					max = Integer.parseInt(st.substring(2));
				}
			}
		}
		max++;
		String id = "HD";
		if (max < 10) {
			id += "0" + String.valueOf(max);
		} else {
			id += String.valueOf(max);
		}
		return id;
	}

	public boolean insert(HopDongLaoDongDTO hopdongDto) {
		String query = "insert into quanlynhansu.hopdonglaodong values('";
		query += hopdongDto.getMahdld() + "','" + hopdongDto.getLoaiHopDong() + "','" + hopdongDto.getNgayKy() + "','"
				+ hopdongDto.getNgayBatDau() + "','" + hopdongDto.getNgayKetThuc() + "')";
		dc.Connect();
		try {
			int k = dc.con.createStatement().executeUpdate(query);
			dc.Disconnect();
			return true;
		} catch (SQLException e) {
			dc.Disconnect();
			return false;
		}
	}

	@SuppressWarnings("unused")
	public boolean update(HopDongLaoDongDTO hopdong) {
		String query = "update quanlynhansu.hopdonglaodong set loaihopdong='" + hopdong.getLoaiHopDong() + "', ngayky='"
				+ hopdong.getNgayKy() + "',ngaybatdau='" + hopdong.getNgayBatDau() + "',ngayketthuc='"
				+ hopdong.getNgayKetThuc() +"'  where mahdld='" + hopdong.getMahdld() + "'";
		dc.Connect();
		try {
			int k = dc.con.createStatement().executeUpdate(query);
			dc.Disconnect();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}
}
