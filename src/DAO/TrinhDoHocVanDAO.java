package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import DTO.ChucVuDTO;
import DTO.TrinhDoHocVanDTO;

public class TrinhDoHocVanDAO {
	DataConnection dc = new DataConnection();

	public TrinhDoHocVanDTO getTrinhDo(String id) {
		String query = "select * from quanlynhansu.trinhdohocvan where matdhv='" + id + "'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			while (result.next()) {
				return new TrinhDoHocVanDTO(id, result.getString(2), result.getString(3));
			}
			return null;
		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}
	}

	public ArrayList<String> getListName(String col) {// lấy tên trình độ học
		// vấn hoặc chuyên ngành
		String query = "select distinct " + col + " from quanlynhansu.trinhdohocvan";
		ArrayList<String> list = new ArrayList<>();
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			while (result.next()) {
				list.add(result.getString(1));
			}
			dc.Disconnect();
			return list;
		} catch (SQLException e) {
			dc.Disconnect();
			JOptionPane.showMessageDialog(null, "Lỗi lấy danh sách");
			return null;
		}
	}//

	public boolean check(String name, String valuename, String chuyennganh, String valuechuyennganh) {
		String query = "select * from quanlynhansu.trinhdohocvan where " + name + "='" + valuename + "' and " + chuyennganh
				+ "='" + valuechuyennganh + "'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {
				dc.Disconnect();
				return false;
			}
			dc.Disconnect();
			return true;
		} catch (SQLException e) {
			dc.Disconnect();
			return false;
		}
	}

	public String createNewId() {
		int max = 0;
		ArrayList<String> listId = getListName("matdhv");
		if (!listId.isEmpty()) {
			for (String st : listId) {
				if (Integer.parseInt(st.substring(4)) > max) {
					max = Integer.parseInt(st.substring(4));
				}
			}
		}
		max++;
		String newId = "TDHV" + String.valueOf(max);
		return newId;
	}
	public boolean insert(TrinhDoHocVanDTO trinhdo) {
		String query = "insert into quanlynhansu.trinhdohocvan values('" + trinhdo.getMaTDHV() + "','"
				+ trinhdo.getTenTDHV() + "','" + trinhdo.getChuyenNganh() + "')";
		dc.Connect();
		try {
			int k=dc.con.createStatement().executeUpdate(query);
			dc.Disconnect();
			return k>=1?true:false;
		} catch (SQLException e) {
			dc.Disconnect();
			return false;
		}
	}
	public String getIdByCondition(String valueName, String specializeName){//lấy id bằng tên tdhv và chuyên ngành
		String query="select matdhv from quanlynhansu.trinhdohocvan where tentdhv='"+valueName+"' and chuyennganh='"+specializeName+"'";
		dc.Connect();
		ResultSet result=null;
		try {
			result=dc.con.createStatement().executeQuery(query);
			if(result.next()){
				String id=result.getString(1);
				dc.Disconnect();
				return id;
			}
			dc.Disconnect();
			return "";
		} catch (SQLException e) {
			dc.Disconnect();
			return "";
		}	
		
	}
}
