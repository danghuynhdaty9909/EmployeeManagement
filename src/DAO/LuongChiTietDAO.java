package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import BUS.LuongBUS;
import BUS.PhuCapBUS;
import BUS.ThuongBUS;
import DTO.LuongChiTietDTO;
import DTO.LuongDTO;
import DTO.PhuCapDTO;
import DTO.ThuongDTO;

public class LuongChiTietDAO {
	DataConnection dc = new DataConnection();

	// lay doi tuong luong chi tiet
	public LuongChiTietDTO getLuongChiTiet(String idNhanVien) {
		String query = "select * from quanlynhansu.luongchitiet where manv='" + idNhanVien + "'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {
				return new LuongChiTietDTO(result.getString(1), result.getInt(2), result.getString(3),
						result.getString(4), result.getString(5), result.getInt(6));

			}
			return null;
		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}
	}

	public boolean delete(String manv) {
		String query = "delete from quanlynhansu.luongchitiet where manv='" + manv + "'";
		dc.Connect();
		try {
			@SuppressWarnings("unused")
			int k = dc.con.createStatement().executeUpdate(query);
			dc.Disconnect();
			return true;
		} catch (SQLException e) {
			dc.Disconnect();
			return false;
		}
	}

	// code của ý

	// hàm lấy tất cả lương chi tiết

	public ArrayList<LuongChiTietDTO> getAllLuongChiTietDTO() {
		ArrayList<LuongChiTietDTO> arr = new ArrayList<LuongChiTietDTO>();
		try {
			dc.Connect();
			String sql = "select * from luongchitiet";
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			LuongChiTietDTO dto;
			while (rs.next() && rs != null) {
				dto = new LuongChiTietDTO(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getInt(6));
				arr.add(dto);
			}
			dc.Disconnect();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return arr;
	}

	public boolean TinhLuongThang(int thang) {
		/*
		 * a=(luongcoban+phucap /24/8) luong = a*sogiolambinhthuong
		 * +a*1.5*sogiolamtangca+thuong
		 */
		boolean flag = false;
		dc.Connect();
		int[] soGioLam;
		ChamCongDAO chamCong = new ChamCongDAO();
		String sql = "select * from luongchitiet,chamcong,phucap,thuong,luong,nhanvien where  thang=" + thang
				+ " and luong.mabacluong = nhanvien.mabacluong and thuong.mathuong=luongchitiet.mathuong and phucap.maphucap=nhanvien.maphucap  and chamcong.machamcong=luongchitiet.machamcong and nhanvien.manv = luongchitiet.manv;";
		try {
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			while (rs.next() && rs != null) {
				flag = true;
				int luong = (int) (rs.getInt("luongcoban") + (rs.getInt("luongcoban") * rs.getDouble("hesophucap")))
						/ 24 / 8;
				soGioLam = chamCong.getSoGioLam(rs.getString("luongchitiet.machamcong"));
				luong = (int) (luong * soGioLam[0] + luong * 1.5 * soGioLam[1] + rs.getInt("thuong.mucthuong")) / 1000;
				luong *= 1000;
				sql = "update luongchitiet set thuclanh=" + luong + " where maluongchitiet='"
						+ rs.getString("maluongchitiet") + "'";
				dc.con.createStatement().executeUpdate(sql);
			}
			dc.Disconnect();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;

	}

	public boolean TinhLuongTheoNhanVien(int thang, String manv) {
		/*
		 * a=(luongcoban+phucap /24/8) luong = a*sogiolambinhthuong
		 * +a*1.5*sogiolamtangca+thuong
		 */
		boolean flag = false;
		dc.Connect();
		int[] soGioLam;
		ChamCongDAO chamCong = new ChamCongDAO();
		String sql = "select * from luongchitiet,chamcong,phucap,thuong,luong,nhanvien where  thang=" + thang
				+ " and luong.mabacluong = nhanvien.mabacluong and thuong.mathuong=luongchitiet.mathuong and phucap.maphucap=nhanvien.maphucap  and chamcong.machamcong=luongchitiet.machamcong and nhanvien.manv = luongchitiet.manv and luongchitiet.manv='"
				+ manv + "';";
		try {
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			while (rs.next() && rs != null) {
				flag = true;
				int luong = (int) (rs.getInt("luongcoban") + (rs.getInt("luongcoban") * rs.getDouble("hesophucap")))
						/ 24 / 8;
				soGioLam = chamCong.getSoGioLam(rs.getString("luongchitiet.machamcong"));
				luong = (int) (luong * soGioLam[0] + luong * 1.5 * soGioLam[1] + rs.getInt("thuong.mucthuong")) / 1000;
				luong *= 1000;
				sql = "update luongchitiet set thuclanh=" + luong + " where maluongchitiet='"
						+ rs.getString("maluongchitiet") + "'";
				dc.con.createStatement().executeUpdate(sql);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;

	}

	// hàm setModel cho table danh sách nhân viên ở fQuanLyLuong
	public DefaultTableModel setModelTableNhanVien(String phong, int thang) {
		TinhLuongThang(thang);
		DefaultTableModel modelNhanVien = null;

		try {
			ChamCongDAO chamCongDAO = new ChamCongDAO();
			dc.Connect();
			String sql = "SELECT nv.manv, nv.ho, nv.ten, nv.mabacluong, nv.maphucap, lct.mathuong, lct.machamcong, lct.thuclanh   FROM luongchitiet lct,nhanvien nv where    nv.manv=lct.manv and lct.thang="
					+ thang + " and nv.maphongban='" + phong + "'";

			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			Vector<String> row;

			if (rs != null && rs.next()) {
				modelNhanVien = new DefaultTableModel(new Object[][] {}, new String[] { "Mã nhân viên", "Tên nhân viên",
						"Lương cơ bản", "Thưởng", "Phụ cấp", "Giờ làm bình thường", "Giờ làm tăng ca", "Thực lãnh" });
				while (rs.next() && rs != null) {
					row = new Vector<String>();
					row.add(rs.getString(1));
					row.add(rs.getString(2) + " " + rs.getString(3));
					String maBacLuong = rs.getString(4);
					for (LuongDTO item : LuongBUS.getSttListLuongDTO()) {
						if (item.getMaBacLuong().equals(maBacLuong)) {
							row.add(String.valueOf(item.getLuongCoBan()));
							break;
						}
					}
					String phucap = rs.getString(5);
					for (PhuCapDTO item : PhuCapBUS.getSttListPhuCap()) {
						if (item.getMaPhuCap().equals(phucap)) {
							row.add(String.valueOf(item.getHeSoPhuCap()));
							break;
						}
					}
					String thuong = rs.getString(6);
					for (ThuongDTO item : ThuongBUS.getSttListThuongDTO()) {
						if (item.getMaThuong().equals(thuong)) {
							row.add(String.valueOf(item.getMucThuong()));
							break;
						}
					}

					int[] gioLam = chamCongDAO.getSoGioLam(rs.getString(7));
					row.add(String.valueOf(gioLam[0]));
					row.add(String.valueOf(gioLam[1]));
					row.add(rs.getString(8));

					modelNhanVien.addRow(row);
				}

			}
			dc.Disconnect();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return modelNhanVien;
	}

	// hàm kiem tra khi tìm kiếm lương theo nhân vien
	public boolean CheckNhanVien(String manv, String thang) {
		boolean flag = false;
		dc.Connect();
		String sql = "select * from luongchitiet where thang=" + thang + " and manv='" + manv + "' ";

		try {
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			if (rs != null && rs.next()) {
				flag = true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return flag;
	}

	// set model nhan vien khi tìm nhan vien
	public DefaultTableModel TimKiemNhanVien(int thang, String manv) {
		TinhLuongTheoNhanVien(thang, manv);
		dc.Connect();
		DefaultTableModel modelNhanVien = new DefaultTableModel(new Object[][] {},
				new String[] { "Mã nhân viên", "Tên nhân viên", "Lương cơ bản", "Thưởng", "Phụ cấp",
						"Giờ làm bình thường", "Giờ làm tăng ca", "Thực lãnh" });
		;
		ChamCongDAO chamCongDAO = new ChamCongDAO();
		String sql = "SELECT nv.manv, nv.ho, nv.ten, nv.mabacluong, nv.maphucap, lct.mathuong, lct.machamcong, lct.thuclanh   FROM luongchitiet lct,nhanvien nv where  nv.manv=lct.manv and lct.thang="
				+ thang + " ";
		if (!manv.isEmpty()) {
			sql += " and lct.manv='" + manv + "';";
		}
		Vector row = new Vector<>();
		try {
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			if (rs != null && rs.next()) {
				row.add(rs.getString(1));
				row.add(rs.getString(2) + " " + rs.getString(3));
				String maBacLuong = rs.getString(4);
				for (LuongDTO item : LuongBUS.getSttListLuongDTO()) {
					if (item.getMaBacLuong().equals(maBacLuong)) {
						row.add(String.valueOf(item.getLuongCoBan()));
						break;
					}
				}
				String phucap = rs.getString(5);
				for (PhuCapDTO item : PhuCapBUS.getSttListPhuCap()) {
					if (item.getMaPhuCap().equals(phucap)) {
						row.add(String.valueOf(item.getHeSoPhuCap()));
						break;
					}
				}
				String thuong = rs.getString(6);
				for (ThuongDTO item : ThuongBUS.getSttListThuongDTO()) {
					if (item.getMaThuong().equals(thuong)) {
						row.add(String.valueOf(item.getMucThuong()));
						break;
					}
				}
				int[] gioLam = chamCongDAO.getSoGioLam(rs.getString(7));
				row.add(String.valueOf(gioLam[0]));
				row.add(String.valueOf(gioLam[1]));
				row.add(rs.getString(8));
				modelNhanVien.addRow(row);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return modelNhanVien;

	}

	public boolean ThemLuongChiTiet(LuongChiTietDTO luong)
	{
		boolean flag = false;
		String sql ="SELECT maluongchitiet FROM quanlynhansu.luongchitiet order by maluongchitiet desc limit 1;";
		dc.Connect();
		try {
			int maluongchitiet = 0;
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			if(rs.next() && rs!=null)
			{
				maluongchitiet = Integer.parseInt(rs.getString(1).toString())+1;
			}
			sql = "INSERT INTO `quanlynhansu`.`luongchitiet` (`maluongchitiet`, `mathuong`, `manv`, `machamcong`, `thang`) VALUES ('"+maluongchitiet+"', '"+luong.getMaThuong()+"', '"+luong.getMaNhanVien()+"', '"+luong.getMaChamCong()+"', '"+luong.getThang()+"');";
			
			int row = dc.con.createStatement().executeUpdate(sql);
			if(row>0)
			{
				flag = true;
			}
			dc.Disconnect();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return flag;
		
	}
	
	// hàm auto tính lương chi tiết có trong database

	public void TinhLuong() {
		dc.Connect();
		String sql = "SELECT distinct thang FROM quanlynhansu.luongchitiet ;";
		try {
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			while (rs.next() && rs != null) {
				TinhLuongThang(rs.getInt(1));
			}

			dc.Disconnect();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

	}

}
