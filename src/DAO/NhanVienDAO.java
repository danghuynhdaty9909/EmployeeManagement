package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.*;

import DTO.NhanVienDTO;

public class NhanVienDAO {
	DataConnection dc = new DataConnection();

	public ArrayList<NhanVienDTO> loadDataByCondition(String cond, String value) {
		ArrayList<NhanVienDTO> list = new ArrayList<>();
		ResultSet result = null;
		String query = "select * from nhanvien";
		if (!cond.isEmpty()) {
			query += " where " + cond + "='" + value + "'";
		}
		try {
			dc.Connect();
			result = dc.con.createStatement().executeQuery(query);
			if (result == null) {
				return null;
			}
			NhanVienDTO staff;
			while (result.next()) {
				staff = new NhanVienDTO(result.getString(1), result.getString(2), result.getString(3),
						result.getString(4), result.getDate(5), result.getString(6), result.getString(7),
						result.getString(8), result.getString(9), result.getString(10), result.getString(11),
						result.getString(12), result.getString(13), result.getString(14), result.getString(15),
						result.getString(16), result.getString(17), result.getString(18), result.getString(19));
				list.add(staff);
			}
			dc.Disconnect();
			return list;

		} catch (SQLException e) {
			dc.Disconnect();
			return null;
		}
	}

	// xóa nhân viên
	@SuppressWarnings("unused")
	public boolean deleteEmployee(String id) {
		String query = "delete from `quanlynhansu`.`nhanvien` where `manv`='" + id + "'";
		dc.Connect();
		try {
			int k1 = dc.con.createStatement().executeUpdate(query);
			dc.Disconnect();
			return true;

		} catch (SQLException e) {
			dc.Disconnect();
			return false;
		}
	}

	// lấy danh sách 1 cột
	public ArrayList<String> getListOneCol(String col) {
		String query = "select " + col + " from nhanvien";
		ArrayList<String> list = new ArrayList<>();
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			while (result.next()) {
				list.add(result.getString(1));
			}
			dc.Disconnect();
			return list;
		} catch (SQLException e) {
			return null;
		}
	}

	// tạo id mới
	public String createNewId() {
		int max = 0;
		ArrayList<String> listId = getListOneCol("manv");
		if (!listId.isEmpty()) {
			for (String st : listId) {
				if (Integer.parseInt(st) > max) {
					max = Integer.parseInt(st);
				}
			}
		}
		max++;
		String id = String.valueOf(max);
		return id;
	}

	// them nhan vien
	public boolean insert(NhanVienDTO staff) {
		dc.Connect();
		StringBuilder query = new StringBuilder("insert into quanlynhansu.nhanvien values('");
		query.append(staff.getMaNV()).append("','");
		query.append(staff.getHo()).append("','");
		query.append(staff.getTen()).append("','");
		query.append(staff.getGioiTinh()).append("','");
		query.append(staff.getNgaySinh()).append("','");
		query.append(staff.getDiaChi()).append("','");
		query.append(staff.getDanToc()).append("','");
		query.append(staff.getSoCMND()).append("','");
		query.append(staff.getSoDT()).append("','");
		query.append(staff.getEmail()).append("','");
		query.append(staff.getSoTaiKhoan()).append("','");
		query.append(staff.getHinhAnh()).append("','");
		query.append(staff.getMahdld()).append("','");
		query.append(staff.getMacv()).append("','");
		query.append(staff.getMaPhongBan()).append("','");
		query.append(staff.getMaTDHV()).append("','");
		query.append(staff.getTonGiao()).append("','");
		query.append(staff.getMaBacLuong()).append("','");
		query.append(staff.getMaPhuCap()).append("')");
		try {
			int result = dc.con.createStatement().executeUpdate(query.toString());
			if (result >= 1) {
				dc.Disconnect();
				return true;
			}
		} catch (SQLException e) {
			return false;
		}
		dc.Disconnect();
		return false;
	}

	public boolean updateOneCol(String col, String newValue, String valueId) {// update
		// 1
		// cột
		String query = "update quanlynhansu.nhanvien set " + col + "='" + newValue + "' where manv='" + valueId + "'";
		dc.Connect();
		try {
			int k = dc.con.createStatement().executeUpdate(query);
			if (k >= 0) {
				dc.Disconnect();
				return true;
			}
			dc.Disconnect();
			return true;
		} catch (SQLException e) {
			dc.Disconnect();
			return false;
		}
	}

	// update nhan vien
	public boolean update(NhanVienDTO nhanvienDto) {
		StringBuilder query = new StringBuilder();
		query.append("update quanlynhansu.nhanvien set ho='" + nhanvienDto.getHo() + "',");
		query.append(" ten='" + nhanvienDto.getTen() + "', gioitinh='" + nhanvienDto.getGioiTinh() + "', ngaysinh='"
				+ nhanvienDto.getNgaySinh() + "',");
		query.append(" diachi='" + nhanvienDto.getDiaChi() + "', dantoc='" + nhanvienDto.getDanToc() + "', socmnd='"
				+ nhanvienDto.getSoCMND() + "',");
		query.append(" sodt='" + nhanvienDto.getSoDT() + "', email='" + nhanvienDto.getEmail() + "', sotaikhoan='"
				+ nhanvienDto.getSoTaiKhoan() + "',");
		query.append(" hinhanh='" + nhanvienDto.getHinhAnh() + "', mahdld='" + nhanvienDto.getMahdld() + "', macv='"
				+ nhanvienDto.getMacv() + "',");
		query.append(" maphongban='" + nhanvienDto.getMaPhongBan() + "', matdhv='" + nhanvienDto.getMaTDHV() + "', ");
		query.append("tongiao='" + nhanvienDto.getTonGiao() + "'");
		query.append(",mabacluong='" + nhanvienDto.getMaBacLuong() + "'");
		query.append(", maphucap='" + nhanvienDto.getMaPhuCap() + "'");
		query.append(" where manv='" + nhanvienDto.getMaNV() + "'");
		dc.Connect();
		try {
			@SuppressWarnings("unused")
			int k = dc.con.createStatement().executeUpdate(query.toString());
			dc.Disconnect();
			return true;
		} catch (SQLException e) {
			dc.Disconnect();
			return false;
		}
	}

	// dem do nhan vien theo dieu kien
	public int countEmployeeByCondition(String condition, String value) {
		String query = "Select count(manv) from quanlynhansu.nhanvien where " + condition + "='" + value + "'";
		dc.Connect();
		try {
			ResultSet result = dc.con.createStatement().executeQuery(query);
			if (result.next()) {
				int k = result.getInt(1);
				dc.Disconnect();
				return k;
			}
			return 0;
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Đã xảy ra lỗi");
			return -1;
		}
	}


		// public int countStaffByCodition(String condition, String value) {
	//
	// public String getId(String col, String id, String value) {
	// String query = "select " + col + " from nhanvien where " + id + "='" +
	// value + "'";
	// dc.Connect();
	// try {
	// ResultSet result = dc.con.createStatement().executeQuery(query);
	// if (result.next()) {
	// return result.getString(1);
	// }
	// } catch (SQLException e) {
	// return "";
	// }
	// return "";
	// }
	//
	// code của ý
	// hàm lấy thông tin nhân viên bằng id
	public NhanVienDTO getNhanVienByID(String manv) {
		NhanVienDTO result = null;
		dc.Connect();
		String sql = "select * from nhanvien where manv='" + manv + "'";
		try {
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			if (rs != null && rs.next()) {
				result = new NhanVienDTO(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getDate(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9),
						rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13), rs.getString(14),
						rs.getString(15), rs.getString(16), rs.getString(17), rs.getString(18), rs.getString(19));
				return result;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	
	public ArrayList<NhanVienDTO> getAllNhanVienDTO()
	{
		ArrayList<NhanVienDTO> arr = new ArrayList<NhanVienDTO>();
		dc.Connect();
		String sql = "select * from nhanvien";
		NhanVienDTO item;
		try {
			ResultSet rs = dc.con.createStatement().executeQuery(sql);
			while(rs!=null && rs.next())
			{
				item = new NhanVienDTO(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getDate(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9),
						rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13), rs.getString(14),
						rs.getString(15), rs.getString(16), rs.getString(17),rs.getString(18),rs.getString(19));
				
				arr.add(item);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return arr;
	}
	public static void main(String[] args) {
		NhanVienDAO dao = new NhanVienDAO();
		dao.getAllNhanVienDTO();
	}
}
